<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
// require APPPATH . 'core/MY_Controller.php';

class Gate_umkm extends REST_Controller{

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('login_model');
        $this->load->model('pemilik_usaha/pemilik_usaha_model','pemilik_usaha_model');
        $this->load->model('pemilik_usaha/master_wilayah_model','master_wilayah_model');

    }

    public function userauthentication_get(){
        $username = $this->get("username");
        $password = $this->get("password");

        $get_passw = $this->login_model->get(
            array(
                "fields"=>"password",
                "where"=>array(
                    "username"=>$username
                )
            ),"row"
        );

        if($get_passw){
            if(password_verify($password,$get_passw->password)){
                $user = $this->login_model->get(
                    array(
                        "fields"=>"nama_lengkap,CONCAT('http://192.168.100.15/umkm/assets/foto_user/',foto_user) as foto_user",
                        "where"=>array(
                            "username"=>$username
                        )
                    ),"row"
                );
            }else{
                $user = array();
            }
        }else{
            $user = array();
        }


        if($user){
            $this->response([
                'status' => true,
                'data' => array($user)
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'data' => $user
            ], REST_Controller::HTTP_OK);
        }
    }

    public function datapemilikusaha_get(){
        $data_pemilik_usaha = $this->pemilik_usaha_model->get(
            array(
                'order_by'=>array(
                    'nama_pemilik_usaha'=>"ASC"
                )
            )
        );

        if($data_pemilik_usaha){
            $this->response([
                'status' => true,
                'data' => array($data_pemilik_usaha)
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'data' => array()
            ], REST_Controller::HTTP_OK);
        }
    }
    
    public function datamasterwilayah_get(){
        $data_master_wilayah = $this->master_wilayah_model->get(
            array(
                "fields"=>"master_wilayah.*,concat(klasifikasi,' - ',nama_wilayah) as nama_wilayah",
                "where_false"=>"klasifikasi NOT IN ('PROV','KAB','KEC')",
                "order_by_false"=>'field (klasifikasi,"KEC","KEL","DESA"),nama_wilayah'
            )
        );

        if($data_master_wilayah){
            $this->response([
                'status' => true,
                'data' => array($data_master_wilayah)
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'data' => array()
            ], REST_Controller::HTTP_OK);
        }
    }
    
    public function datausaha_get(){
        $data_usaha = $this->data_usaha_model->get(
            array(
                "fields"=>"master_data_usaha.*,nama_pemilik_usaha,nama_jenis_usaha",
                "join"=>array(
                    "master_pemilik_usaha"=>"id_pemilik_usaha=master_pemilik_usaha_id AND master_pemilik_usaha.deleted_at IS NULL and master_pemilik_usaha.is_verified = 1",
                    "master_jenis_usaha"=>"id_jenis_usaha=master_jenis_usaha_id",
                ),
                'order_by'=>array(
                    'nama_toko_perusahaan'=>"ASC"
                )
            )
        );

        if($data_usaha){
            $this->response([
                'status' => true,
                'data' => array($data_usaha)
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'data' => array()
            ], REST_Controller::HTTP_OK);
        }
    }
    
    public function dataasetomset_get(){
        $data_aset_omset = $this->aset_omset_usaha_model->get(
            array(
                "fields"=>"aset_omset_usaha.*,nama_pemilik_usaha,nama_toko_perusahaan,nama_jenis_usaha",
                "join"=>array(
                    "master_data_usaha"=>"id_data_usaha=master_data_usaha_id AND master_data_usaha.deleted_at IS NULL and master_data_usaha.is_verified = 1",
                    "master_pemilik_usaha"=>"id_pemilik_usaha=master_pemilik_usaha_id AND master_pemilik_usaha.deleted_at IS NULL and master_pemilik_usaha.is_verified = 1",
                    "master_jenis_usaha"=>"id_jenis_usaha=master_jenis_usaha_id"
                )
            )
        );

        if($data_aset_omset){
            $this->response([
                'status' => true,
                'data' => array($data_aset_omset)
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'data' => array()
            ], REST_Controller::HTTP_OK);
        }
    }
}

?>