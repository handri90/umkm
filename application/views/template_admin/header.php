<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php echo $title_main; ?></title>

	<script>var base_url = "<?php echo base_url(); ?>";</script>

	<!-- Global stylesheets -->
	<link href="<?php echo base_url(); ?>assets/template_admin/css/family_roboto.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/template_admin/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/template_admin/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/template_admin/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/template_admin/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/template_admin/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/template_admin/css/colors.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/template_admin/css/custom.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/template_admin/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
	
	<!-- /global stylesheets -->

	<link rel="icon" href="<?php echo base_url(); ?>assets/template_admin/favicon.png" type="image/x-icon">

	<!-- Core JS files -->
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/main/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/forms/selects/select2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/demo_pages/components_modals.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/notifications/pnotify.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/notifications/bootbox.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/forms/inputs/touchspin.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/forms/styling/switch.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/loaders/progressbar.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/ui/moment/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/pickers/daterangepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/plugins/visualization/echarts/echarts.min.js"></script>

	
	<script src="<?php echo base_url(); ?>assets/template_admin/js/bootstrap-datepicker.min.js"></script>
	
	<script src="<?php echo base_url(); ?>assets/template_admin/js/app.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/demo_pages/extra_pnotify.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/global_assets/js/demo_pages/components_modals.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/js/custom.js"></script>
	<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>

	<script src="<?php echo base_url(); ?>assets/template_admin/js/cleave.js/dist/cleave.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/js/currency.min.js"></script>

	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-dark">
		<div class="navbar-brand">
			<a href="<?php echo base_url(); ?>" class="d-inline-block">
				<img src="<?php echo base_url(); ?>assets/template_admin/global_assets/images/logo_light.png" alt="">
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-user"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-paragraph-justify3"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>
			</ul>

			<span class="badge ml-md-3 mr-md-auto">&nbsp</span>

			<ul class="navbar-nav">
				

				<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo base_url().$this->config->item('user_path')."/".($this->session->userdata('foto_user')?$this->session->userdata('foto_user'):"default_profile.jpg"); ?>" class="rounded-circle mr-2" height="34" alt="">
						<span><?php echo $this->session->userdata("nama_lengkap"); ?></span>
					</a>

					<div class="dropdown-menu dropdown-menu-right">
						<a href="<?php echo base_url(); ?>user" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
						<a href="<?php echo base_url(); ?>Login/act_logout" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->