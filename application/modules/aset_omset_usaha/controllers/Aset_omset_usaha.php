<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aset_omset_usaha extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('aset_omset_usaha_model');
		$this->load->model('jenis_usaha/jenis_usaha_model','jenis_usaha_model');
		$this->load->model('data_usaha/data_usaha_model','data_usaha_model');
		$this->load->model('pemilik_usaha/pemilik_usaha_model','pemilik_usaha_model');
		$this->load->model('jenis_usaha/jenis_usaha_model','jenis_usaha_model');
	}

	public function index()
	{
        $data['master_pemilik_usaha'] = $this->pemilik_usaha_model->get(
            array(
                "order_by"=>array(
                    "nama_pemilik_usaha"=>"ASC"
                ),
                "where"=>array(
                    "is_verified"=>"1"
                )
            )
        );

        $data['master_jenis_usaha'] = $this->jenis_usaha_model->get(
            array(
                "order_by"=>array(
                    "nama_jenis_usaha"=>"ASC"
                )
            )
        );

        $data['master_tahun'] = $this->aset_omset_usaha_model->get(
            array(
                "fields"=>"tahun_berkenaan AS tahun",
                "order_by"=>array(
                    "nama_jenis_usaha"=>"ASC"
                ),
                "group_by"=>"tahun",
                "order_by"=>array(
                    "tahun"=>"ASC"
                ),
                "where"=>array(
                    "is_verified"=>"1"
                )
            )
        );

        $data['breadcrumb'] = [['link'=>false,'content'=>'Aset & Omset Usaha','is_active'=>true]];
        if($this->session->userdata('level_user_id') == '3'){
            $this->execute('index_admin_biasa',$data);
        }else{
            $this->execute('index',$data);
        }
    }
    
    public function tambah_aset_omset_usaha(){
        if(empty($_POST)){
            $data['master_data_usaha'] = $this->data_usaha_model->get(
                array(
                    "fields"=>"master_data_usaha.*,nama_pemilik_usaha,nama_jenis_usaha,nik",
                    "join"=>array(
                        "master_pemilik_usaha"=>"id_pemilik_usaha=master_pemilik_usaha_id AND master_pemilik_usaha.deleted_at IS NULL AND master_pemilik_usaha.is_verified = 1",
                        "master_jenis_usaha"=>"id_jenis_usaha=master_jenis_usaha_id"
                    ),
                    "order_by"=>array(
                        "nama_toko_perusahaan"=>"ASC"
                    ),
                    "where"=>array(
                        "master_data_usaha.is_verified"=>"1"
                    )
                )
            );

            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'aset_omset_usaha','content'=>'Aset & Omset Usaha','is_active'=>false],['link'=>false,'content'=>'Tambah Aset & Omset Usaha','is_active'=>true]];
            $this->execute('form_aset_omset_usaha',$data);
        }else{

            $data_master_usaha = $this->aset_omset_usaha_model->get(
                array(
                    "where"=>array(
                        "master_data_usaha_id"=>decrypt_data($this->ipost('data_usaha')),
                        "tahun_berkenaan"=>trim($this->ipost('tahun'))
                    )
                )
            );

            if(!$data_master_usaha){
                $data = array(
                    "master_data_usaha_id"=>decrypt_data($this->ipost('data_usaha')),
                    "jumlah_aset"=>replace_dot($this->ipost('jumlah_aset')),
                    "jumlah_omset_per_tahun"=>replace_dot($this->ipost('jumlah_omset_per_tahun')),
                    "modal_sendiri"=>replace_dot($this->ipost('modal_sendiri')),
                    "modal_luar"=>replace_dot($this->ipost('modal_luar')),
                    "jumlah_pekerja"=>$this->ipost('jumlah_pekerja'),
                    "pinjaman_luar"=>replace_dot($this->ipost('pinjaman_luar')),
                    "agunan_modal"=>$this->ipost('agunan_modal'),
                    "tahun_berkenaan"=>trim($this->ipost('tahun')),
                    'is_verified'=>"0",
                    'created_at'=>$this->datetime(),
                    'id_user_created'=>$this->session->userdata("id_user")
                );
    
                $status = $this->aset_omset_usaha_model->save($data);
            }else{
                $status = false;
            }

            if($status){
                $this->session->set_flashdata('message','Aset & Omset baru berhasil ditambahkan');
            }else{
                $this->session->set_flashdata('message','Aset & Omset baru gagal ditambahkan');
            }

            redirect('aset_omset_usaha');
        }
    }

    public function edit_aset_omset_usaha($id_aset_omset_usaha){
        $aset_omset_master = $this->aset_omset_usaha_model->get_by(decrypt_data($id_aset_omset_usaha));

        if(!$aset_omset_master){
            $this->page_error();
        }

        if(empty($_POST)){
            $data['master_data_usaha'] = $this->data_usaha_model->get(
                array(
                    "fields"=>"master_data_usaha.*,nama_pemilik_usaha,nama_jenis_usaha,nik",
                    "join"=>array(
                        "master_pemilik_usaha"=>"id_pemilik_usaha=master_pemilik_usaha_id AND master_pemilik_usaha.deleted_at IS NULL AND master_pemilik_usaha.is_verified = 1",
                        "master_jenis_usaha"=>"id_jenis_usaha=master_jenis_usaha_id"
                    ),
                    "order_by"=>array(
                        "nama_toko_perusahaan"=>"ASC"
                    ),
                    "where"=>array(
                        "master_data_usaha.is_verified"=>"1"
                    )
                )
            );

            $data['content'] = $aset_omset_master;
            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'aset_omset_usaha','content'=>'Aset & Omset Usaha','is_active'=>false],['link'=>false,'content'=>'Tambah Aset & Omset Usaha','is_active'=>true]];
            $this->execute('form_aset_omset_usaha',$data);
        }else{
            $data_master_usaha = $this->aset_omset_usaha_model->get(
                array(
                    "where"=>array(
                        "master_data_usaha_id"=>decrypt_data($this->ipost('data_usaha')),
                        "tahun_berkenaan"=>trim($this->ipost('tahun'))
                    ),
                    "where_false"=>"id_aset_omset_usaha != '".decrypt_data($id_aset_omset_usaha)."'"
                )
            );

            if(!$data_master_usaha){
                $data = array(
                    "master_data_usaha_id"=>decrypt_data($this->ipost('data_usaha')),
                    "jumlah_aset"=>replace_dot($this->ipost('jumlah_aset')),
                    "jumlah_omset_per_tahun"=>replace_dot($this->ipost('jumlah_omset_per_tahun')),
                    "modal_sendiri"=>replace_dot($this->ipost('modal_sendiri')),
                    "modal_luar"=>replace_dot($this->ipost('modal_luar')),
                    "jumlah_pekerja"=>$this->ipost('jumlah_pekerja'),
                    "pinjaman_luar"=>replace_dot($this->ipost('pinjaman_luar')),
                    "agunan_modal"=>$this->ipost('agunan_modal'),
                    "tahun_berkenaan"=>trim($this->ipost('tahun')),
                    'updated_at'=>$this->datetime(),
                    'id_user_updated'=>$this->session->userdata("id_user")
                );
                
                $status = $this->aset_omset_usaha_model->edit(decrypt_data($id_aset_omset_usaha),$data);
            }else{
                $status = false;
            }
            
            if($status){
                $this->session->set_flashdata('message','Aset & Omset berhasil diubah');
            }else{
                $this->session->set_flashdata('message','Aset & Omset gagal diubah');
            }

            redirect('aset_omset_usaha');
        }
    }

    public function delete_aset_omset_usaha(){
        $id_aset_omset_usaha = decrypt_data($this->iget('id_aset_omset_usaha'));
        $aset_omset_master = $this->aset_omset_usaha_model->get_by($id_aset_omset_usaha);

        if(!$aset_omset_master){
            $this->page_error();
        }

        $status = $this->aset_omset_usaha_model->remove($id_aset_omset_usaha);
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }

    public function detail_pemilik_usaha($id_aset_omset_usaha){
        $aset_omset_master = $this->aset_omset_usaha_model->get_by(decrypt_data($id_aset_omset_usaha));

        if(!$aset_omset_master){
            $this->page_error();
        }

        $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'aset_omset_usaha','content'=>'Aset & Omset Usaha','is_active'=>false],['link'=>false,'content'=>'Detail Pemilik Usaha','is_active'=>true]];

        $this->execute('detail_pemilik_usaha',$data);
    }

    public function change_status(){
        $id_aset_omset = decrypt_data($this->ipost("id_aset_omset"));

        $data_master = $this->aset_omset_usaha_model->get_by($id_aset_omset);

        if($data_master->is_verified == "0"){
            $data_update = array(
                "is_verified"=>"1"
            );
        }else{
            $data_update = array(
                "is_verified"=>"0"
            );
        }

        $status = $this->aset_omset_usaha_model->edit($id_aset_omset,$data_update);

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }
}
