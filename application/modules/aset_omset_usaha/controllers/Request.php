<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {
	function __construct(){
		parent::__construct();
        $this->load->model('aset_omset_usaha_model');
        $this->load->model('data_usaha/data_usaha_model','data_usaha_model');
	}

	public function get_aset_omset_usaha()
	{
        $pemilik_usaha = $this->iget("pemilik_usaha");
        $data_usaha = $this->iget("data_usaha");
        $tahun = $this->iget("tahun");

        $con_where = array();

        if($data_usaha){
            $con_where = array(
                "id_data_usaha"=>decrypt_data($data_usaha)
            );
        }else if($pemilik_usaha){
            $con_where = array(
                "id_pemilik_usaha"=>decrypt_data($pemilik_usaha)
            );
        }

        $wh_false = "";
        if($tahun != ""){
            $wh_false = "tahun_berkenaan = ".$tahun;
        }

        $data_usaha = $this->aset_omset_usaha_model->get(
            array(
                "fields"=>"aset_omset_usaha.*,nama_pemilik_usaha,nama_toko_perusahaan,nama_jenis_usaha",
                "join"=>array(
                    "master_data_usaha"=>"id_data_usaha=master_data_usaha_id AND master_data_usaha.deleted_at IS NULL and master_data_usaha.is_verified = 1",
                    "master_pemilik_usaha"=>"id_pemilik_usaha=master_pemilik_usaha_id AND master_pemilik_usaha.deleted_at IS NULL and master_pemilik_usaha.is_verified = 1",
                    "master_jenis_usaha"=>"id_jenis_usaha=master_jenis_usaha_id"
                ),
                "where"=>$con_where,
                "where_false"=>$wh_false
            )
        );

        $templist = array();
        foreach($data_usaha as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_aset_omset_usaha);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function get_data_usaha(){
        $pemilik_usaha = $this->iget("pemilik_usaha");

        $data_usaha = $this->data_usaha_model->get(
            array(
                "fields"=>"master_data_usaha.*,nama_pemilik_usaha,nama_jenis_usaha",
                "join"=>array(
                    "master_pemilik_usaha"=>"id_pemilik_usaha=master_pemilik_usaha_id AND master_pemilik_usaha.deleted_at IS NULL and master_pemilik_usaha.is_verified = 1",
                    "master_jenis_usaha"=>"id_jenis_usaha=master_jenis_usaha_id",
                ),
                "where"=>array(
                    "master_pemilik_usaha_id"=>decrypt_data($pemilik_usaha),
                    "master_data_usaha.is_verified"=>"1"
                ),
                'order_by'=>array(
                    'nama_toko_perusahaan'=>"ASC"
                )
            )
        );

        $templist = array();
        foreach($data_usaha as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_data_usaha);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function cek_aset_omset(){
        $id_asset_omset_usaha = $this->iget("id_asset_omset_usaha");
        $data_usaha = $this->iget("data_usaha");
        $tahun = $this->iget("tahun");

        $con_str = "";

        if($id_asset_omset_usaha){
            $con_str .= "id_aset_omset_usaha != '".decrypt_data($id_asset_omset_usaha)."'";
        }

        $year_now_str_totime = strtotime($this->datetime());
        $year_now = date("Y",$year_now_str_totime);
        
        $data_master_usaha = $this->aset_omset_usaha_model->get(
            array(
                "where"=>array(
                    "master_data_usaha_id"=>decrypt_data($data_usaha),
                    "tahun_berkenaan"=>$tahun
                ),
                "where_false"=>$con_str
            )
        );

        if(!$data_master_usaha){
            $data = true;
        }else{
            $data = false;
        }

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function get_aset_omset_usaha_by_id(){
        $id_aset_omset = decrypt_data($this->iget("id_aset_omset"));

        $data = $this->aset_omset_usaha_model->get(
            array(
                "fields"=>"aset_omset_usaha.*,nama_pemilik_usaha,nama_toko_perusahaan,nama_jenis_usaha",
                "join"=>array(
                    "master_data_usaha"=>"id_data_usaha=master_data_usaha_id AND master_data_usaha.deleted_at IS NULL and master_data_usaha.is_verified = 1",
                    "master_pemilik_usaha"=>"id_pemilik_usaha=master_pemilik_usaha_id AND master_pemilik_usaha.deleted_at IS NULL and master_pemilik_usaha.is_verified = 1",
                    "master_jenis_usaha"=>"id_jenis_usaha=master_jenis_usaha_id"
                ),
                "where"=>array(
                    "id_aset_omset_usaha"=>$id_aset_omset
                )
            ),"row"
        );

        $data->id_encrypt = encrypt_data($data->id_aset_omset_usaha);
        $created_at = strtotime($data->created_at);
        $data->tahun_n = date("Y",$created_at);

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
}
