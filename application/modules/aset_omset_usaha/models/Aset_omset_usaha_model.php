<?php

class Aset_omset_usaha_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="aset_omset_usaha";
        $this->primary_id="id_aset_omset_usaha";
    }
}