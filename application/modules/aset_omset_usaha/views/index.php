<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Pemilik Usaha: </label>
                        <select class="form-control select-search" name="pemilik_usaha" onChange="get_data_usaha()">
                            <option value="">-- SEMUA --</option>
                            <?php
                            foreach($master_pemilik_usaha as $key=>$row){
                                ?>
                                <option value="<?php echo encrypt_data($row->id_pemilik_usaha); ?>"><?php echo $row->nama_pemilik_usaha; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Toko / Perusahaan: </label>
                        <select class="form-control select-search" name="data_usaha" onChange="get_aset_omset_usaha()">
                            <option value="">-- Pilih Toko / Perusahaan --</option>
                            <?php
                            foreach($master_data_usaha as $key=>$row){
                                ?>
                                <option value="<?php echo encrypt_data($row->id_data_usaha); ?>"><?php echo $row->nama_toko_perusahaan; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="tahun-filter row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tahun : </label>
                        <select class="form-control select-search" name="tahun" onChange="get_aset_omset_usaha()">
                            <option value="">-- Pilih Tahun --</option>
                            <?php
                            foreach($master_tahun as $key=>$row){
                                ?>
                                <option value="<?php echo $row->tahun; ?>"><?php echo $row->tahun; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Basic datatable -->
    <div class="card">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url(); ?>aset_omset_usaha/tambah_aset_omset_usaha" class="btn btn-info">Tambah Aset & Omset</a>
            </div>
        </div>
        <table id="datatableAsetOmsetUsaha" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>Pemilik Usaha</th>
                    <th>Toko / Perusahaan</th>
                    <th>Jenis Usaha</th>
                    <th>Jumlah Aset</th>
                    <th>Jumlah Omset</th>
                    <th>Modal Sendiri</th>
                    <th>Modal Luar</th>
                    <th>Pinjaman Luar</th>
                    <th>Tahun</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->
</div>

<div id="modal_detail" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title">Detail Aset Omset</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <table class="table datatable-save-state">
                    <tbody>
                        <tr>
                            <td width="200">Tahun</td>
                            <td width="50">:</td>
                            <td>
                                <span class="tahun_berkenaan"></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">Nama Pemilik Usaha</td>
                            <td width="50">:</td>
                            <td>
                                <span class="nama_pemilik_usaha"></span>
                                <input type="hidden" name="id_aset_omset" />
                            </td>
                        </tr>
                        <tr>
                            <td width="200">Nama Toko / Perusahaan</td>
                            <td width="50">:</td>
                            <td>
                                <span class="nama_toko_perusahaan"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Jumlah Aset</td>
                            <td>:</td>
                            <td><span class="jumlah_aset"></span></td>
                        </tr>
                        <tr>
                            <td>Jumlah Omset Pertahun</td>
                            <td>:</td>
                            <td><span class="jumlah_omset_pertahun"></span></td>
                        </tr>
                        <tr>
                            <td>Modal Sendiri</td>
                            <td>:</td>
                            <td><span class="modal_sendiri"></span></td>
                        </tr>
                        <tr>
                            <td>Modal Luar</td>
                            <td>:</td>
                            <td><span class="modal_luar"></span></td>
                        </tr>
                        <tr>
                            <td>Jumlah Pekerja</td>
                            <td>:</td>
                            <td><span class="jumlah_pekerja"></span></td>
                        </tr>
                        <tr>
                            <td>Nilai Pinjaman Pada Lembaga Keuangan</td>
                            <td>:</td>
                            <td><span class="pinjaman_lembaga_keuangan"></span></td>
                        </tr>
                        <tr>
                            <td>Agunan Yang Digunakan</td>
                            <td>:</td>
                            <td><span class="agunan"></span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn bg-primary" onClick="change_status()"><span class="label_btn_verified"></span></button>
            </div>
        </div>
    </div>
</div>

<script>

$(".tahun-filter").hide();

const IDR = value => currency(
    value, { 
        symbol: "Rp. ",
        precision: 0,
        separator: "." 
});

let datatableAsetOmsetUsaha = $("#datatableAsetOmsetUsaha").DataTable({
    "columns": [
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        {
            "width":"220"
        }
    ]
});
get_aset_omset_usaha();
function get_aset_omset_usaha(){
    $(".tahun-filter").hide();

    let pemilik_usaha = $("select[name=pemilik_usaha]").val();
    let data_usaha = $("select[name=data_usaha]").val();
    let tahun = "";

    if(pemilik_usaha == ""){
        $(".tahun-filter").show();
        tahun = $("select[name=tahun]").val();
    }

    datatableAsetOmsetUsaha.clear().draw();
    $.ajax({
        url: base_url+'aset_omset_usaha/request/get_aset_omset_usaha',
        data:{pemilik_usaha:pemilik_usaha,data_usaha:data_usaha,tahun:tahun},
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $.each(response,function(index,value){
                datatableAsetOmsetUsaha.row.add([
                    value.nama_pemilik_usaha,
                    value.nama_toko_perusahaan,
                    value.nama_jenis_usaha,
                    IDR(value.jumlah_aset).format(true),
                    IDR(value.jumlah_omset_per_tahun).format(true),
                    IDR(value.modal_sendiri).format(true),
                    IDR(value.modal_luar).format(true),
                    IDR(value.pinjaman_luar).format(true),
                    value.tahun_berkenaan,
                    (value.is_verified == "0"?"<span class='badge bg-warning mr-md-auto'>Draft</span>":"<span class='badge bg-success mr-md-auto'>Sudah Diverifikasi</span>"),
                    "<a href='"+base_url+"aset_omset_usaha/edit_aset_omset_usaha/"+value.id_encrypt+"' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('"+value.id_encrypt+"')\" href='#'><i class='icon-trash'></i></a> <a class='btn btn-success btn-icon' onClick=\"show_detail('"+value.id_encrypt+"')\" href='#'><i class='icon-eye8'></i></a>"
                ]).draw(false);
            });
        },
        complete:function(response){
            loading_stop();
        }
    });
}

function get_data_usaha(){
    let pemilik_usaha = $("select[name=pemilik_usaha]").val();

    let html = "";
    $("select[name=data_usaha]").html("<option value=''>-- SEMUA --</option>");
    $.ajax({
        url: base_url+'aset_omset_usaha/request/get_data_usaha',
        data: {pemilik_usaha:pemilik_usaha},
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            html += "<option value=''>-- SEMUA --</option>";
            $.each(response,function(index,value){
                html += "<option value='"+value.id_encrypt+"'>"+value.nama_toko_perusahaan+"</option>";
            });

            $("select[name=data_usaha]").html(html);
            get_aset_omset_usaha();
        },
        complete:function(response){
            loading_stop();
        }
    });
}

function confirm_delete(id_aset_omset_usaha){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    swalInit({
        title: 'Apakah anda yakin menghapus data ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: base_url+'aset_omset_usaha/delete_aset_omset_usaha',
                data : {id_aset_omset_usaha:id_aset_omset_usaha},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    if(response){
                        get_aset_omset_usaha();
                        swalInit(
                            'Berhasil',
                            'Data sudah dihapus',
                            'success'
                        );
                    }else{
                        get_aset_omset_usaha();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa dihapus',
                            'error'
                        );
                    }
                },
                complete:function(response){
                    loading_stop();
                }
            });
        }
        else if(result.dismiss === swal.DismissReason.cancel) {
            swalInit(
                'Batal',
                'Data masih tersimpan!',
                'error'
            ).then(function(results){
                loading_stop();
                if(result.results){
                    get_aset_omset_usaha();
                }
            });
        }
    });
}

function show_detail(id_aset_omset){
    $.ajax({
        url: base_url+'aset_omset_usaha/request/get_aset_omset_usaha_by_id',
        data:{id_aset_omset:id_aset_omset},
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $(".label_btn_verified").html((response.is_verified == "0"?"Setuju":"Batalkan"));
            $("#modal_detail").modal("show");
            $(".nama_pemilik_usaha").html(response.nama_pemilik_usaha);
            $(".nama_toko_perusahaan").html(response.nama_toko_perusahaan);
            $(".jumlah_aset").html(IDR(response.jumlah_aset).format(true));
            $(".jumlah_omset_pertahun").html(IDR(response.jumlah_omset_per_tahun).format(true));
            $(".modal_sendiri").html(IDR(response.modal_sendiri).format(true));
            $(".modal_luar").html(IDR(response.modal_luar).format(true));
            $(".jumlah_pekerja").html(response.jumlah_pekerja);
            $(".pinjaman_lembaga_keuangan").html(IDR(response.pinjaman_luar).format(true));
            $(".agunan").html(response.agunan_modal);
            $(".tahun_berkenaan").html(response.tahun_berkenaan);
            $("input[name=id_aset_omset]").val(id_aset_omset);
        },
        complete:function(){
            loading_stop();
        }
    });
}

function change_status(){
    id_aset_omset = $("input[name=id_aset_omset]").val();

    $.ajax({
        url: base_url+'aset_omset_usaha/change_status',
        data:{id_aset_omset:id_aset_omset},
        type: 'POST',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $("#modal_detail").modal("toggle");
            $(".nama_pemilik_usaha").html("");
            $(".nama_toko_perusahaan").html("");
            $(".jumlah_aset").html("");
            $(".jumlah_omset_pertahun").html("");
            $(".modal_sendiri").html("");
            $(".modal_luar").html("");
            $(".jumlah_pekerja").html("");
            $(".pinjaman_lembaga_keuangan").html("");
            $(".agunan").html("");
            $("input[name=id_aset_omset]").val("");
            get_aset_omset_usaha();
        },
        complete:function(){
            loading_stop();
        }
    });
}
</script>