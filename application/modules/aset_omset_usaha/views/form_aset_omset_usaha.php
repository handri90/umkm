<style>
    .calendars{
        width:350px;
    }
</style>
<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <?php echo form_open(); ?>
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Aset & Omset Usaha</legend>

                    <input type="hidden" name="id_asset_omset_usaha" value="<?php echo !empty($content)?encrypt_data($content->id_aset_omset_usaha):""; ?>" />

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Tahun <span class="text-danger">*</span></label>
                        <div class="col-lg-1">
                            <div class="input-group input-daterange" id="tahun-range">
                                <input readonly required type="text" class="form-control" name="tahun" value="<?php echo !empty($content)?$content->tahun_berkenaan:""; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Nama Toko / Perusahaan <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <select class="form-control select-search" name="data_usaha" required>
								<option value="">-- Pilih Perusahaan --</option>
								<?php
								foreach($master_data_usaha as $key=>$row){
									$selected = "";
									if(!empty($content)){
										if($row->id_data_usaha == $content->master_data_usaha_id){
											$selected = 'selected="selected"';
										}
									}
									?>
									<option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_data_usaha); ?>"><?php echo $row->nik." - ".$row->nama_pemilik_usaha." - ".$row->nama_toko_perusahaan." - ".$row->nama_jenis_usaha; ?></option>
									<?php
								}
								?>
							</select>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Jumlah Aset <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control input-currency-aset" value="<?php echo !empty($content)?$content->jumlah_aset:""; ?>" name="jumlah_aset" required placeholder="Jumlah Aset">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Jumlah Omset Pertahun <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control input-currency-omset" value="<?php echo !empty($content)?$content->jumlah_omset_per_tahun:""; ?>" name="jumlah_omset_per_tahun" required placeholder="Jumlah Omset Pertahun">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Modal Sendiri <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control input-currency-modal-sendiri" value="<?php echo !empty($content)?$content->modal_sendiri:""; ?>" name="modal_sendiri" required placeholder="Modal Sendiri">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Modal Luar <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control input-currency-modal-luar" value="<?php echo !empty($content)?$content->modal_luar:""; ?>" name="modal_luar" required placeholder="Modal Luar">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Jumlah Pekerja <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="number" class="form-control" value="<?php echo !empty($content)?$content->jumlah_pekerja:""; ?>" name="jumlah_pekerja" required placeholder="Jumlah Pekerja">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Nilai Pinjaman dari Bank / Lembaga Keuangan <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control input-currency-nilai-pinjaman" value="<?php echo !empty($content)?$content->pinjaman_luar:""; ?>" name="pinjaman_luar" required placeholder="Nilai Pinjaman dari Bank / Lembaga Keuangan">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Uraian Agunan <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <textarea class="form-control" name="agunan_modal" required placeholder="Uraian Agunan"><?php echo !empty($content)?$content->agunan_modal:""; ?></textarea>
                        </div>
                    </div>
                </fieldset>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /form inputs -->
</div>

<script>
$('#tahun-range input').each(function () {
    $(this).datepicker({
        autoclose: true,
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years"
    });
});

var cleave = new Cleave('.input-currency-aset', {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    numeralDecimalMark: ',',
    delimiter: '.',
});

var cleave = new Cleave('.input-currency-omset', {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    numeralDecimalMark: ',',
    delimiter: '.',
});

var cleave = new Cleave('.input-currency-modal-sendiri', {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    numeralDecimalMark: ',',
    delimiter: '.',
});

var cleave = new Cleave('.input-currency-modal-luar', {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    numeralDecimalMark: ',',
    delimiter: '.',
});

var cleave = new Cleave('.input-currency-nilai-pinjaman', {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    numeralDecimalMark: ',',
    delimiter: '.',
});

$("form").submit(function(){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    let id_asset_omset_usaha = $("input[name=id_asset_omset_usaha]").val();
    let data_usaha = $("select[name=data_usaha]").val();
    let tahun = $("input[name=tahun]").val().trim();
    let status = true;

    $.ajax({
        url: base_url+'aset_omset_usaha/request/cek_aset_omset',
        async:false,
        data:{data_usaha:data_usaha,id_asset_omset_usaha:id_asset_omset_usaha,tahun:tahun},
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            // alert(response);
            status = response;
        },
        complete:function(response){
            loading_stop();
        }
    });

    if(!status){
        swalInit(
            'Gagal',
            'Data Aset dan Omset sudah ada di tahun berkenaan',
            'error'
        );
        return false;
    }
})

</script>