<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="tahun-filter row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tipe Laporan : </label>
                        <select class="form-control select-search" name="tipe_laporan" onChange="get_tipe_laporan()">
                            <option value="">-- Pilih Tipe Laporan --</option>
                            <?php
                            foreach($tipe_laporan as $key=>$row){
                                ?>
                                <option value="<?php echo $key; ?>"><?php echo $row; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Jenis Usaha : </label>
                        <select multiple="multiple" class="form-control select-search" name="jenis_usaha" required onchange="get_laporan()">
                            <option value="all">-- Semua Jenis Usaha --</option>
                            <?php
                            foreach($master_jenis_usaha as $key=>$row){
                                $selected = "";
                                if(!empty($content)){
                                    if($row->id_jenis_usaha == $content->master_jenis_usaha_id){
                                        $selected = 'selected="selected"';
                                    }
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_jenis_usaha); ?>"><?php echo $row->nama_jenis_usaha; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="kecamatan-filter row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Kecamatan : </label>
                        <select class="form-control select-search" name="kecamatan" onChange="get_laporan()">
                            <option value="">-- Semua Kecamatan --</option>
                            <?php
                            foreach($list_kecamatan as $key=>$row){
                                ?>
                                <option value="<?php echo encrypt_data($row->kode_wilayah); ?>"><?php echo ucwords(strtolower($row->nama_wilayah)); ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="range-year row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tahun : </label>
                        <div class="input-group input-daterange" id="education-range">
                            <!-- <input readonly type="text" class="form-control" name="tahun" onchange="get_laporan()"> -->
                            <input readonly type="text" class="form-control" name="year_start" onchange="get_laporan()">
                            <span class="input-group-prepend">
                                <span class="input-group-text">to</span>
                            </span>
                            <input readonly type="text" class="form-control" name="year_end" onchange="get_laporan()">
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-year row">
                <div class="col-md-1">
                    <div class="form-group">
                        <label>Tahun : </label>
                        <div class="input-group input-daterange" id="education-range">
                            <!-- <input readonly type="text" class="form-control" name="tahun" onchange="get_laporan()"> -->
                            <input readonly type="text" class="form-control" name="tahun" onchange="get_laporan()">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Basic datatable -->
    <div class="card">
        <div class="card-body">
            <div class="box-top text-right">
                <!-- <a href="#cetakLaporanFormat12" id="cetakLaporanFormat12" class="btn btn-info">Cetak Laporan (Format 1 & 2)</a>
                <a href="#cetakLaporanFormat3" id="cetakLaporanFormat3" class="btn btn-info">Cetak Laporan (Format 3)</a>
                <a href="#cetakLaporanFormat4" id="cetakLaporanFormat4" class="btn btn-info">Cetak Laporan (Format 4)</a> -->
                <a href="#cetakLaporan" id="cetakLaporan" class="btn btn-info">Cetak Laporan</a>
            </div>
        </div>
        <table id="datatableLaporan" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>Pemilik Usaha</th>
                    <th>Toko / Perusahaan</th>
                    <th>Jenis Usaha</th>
                    <th>Jumlah Aset</th>
                    <th>Jumlah Omset</th>
                    <th>Modal Sendiri</th>
                    <th>Modal Luar</th>
                    <th>Pinjaman Luar</th>
                    <th>Tahun</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->
</div>

<script>
$(".box-top").hide();
$(".range-year").hide();
$(".single-year").hide();
$(".kecamatan-filter").hide();

const IDR = value => currency(
    value, { 
        symbol: "Rp. ",
        precision: 0,
        separator: "." 
});

let datatableLaporan = $("#datatableLaporan").DataTable({
    "columns": [
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    ]
});

function get_tipe_laporan(){
    datatableLaporan.clear().draw();
    $(".box-top").hide();
    $(".range-year").hide();
    $(".single-year").hide();
    $(".kecamatan-filter").hide();
    
    let tipe_laporan = $("select[name=tipe_laporan]").val();

    if(tipe_laporan == "laporan1" || tipe_laporan == "laporan2"){
        $("input[name=year_start]").val("");
        $("input[name=year_end]").val("");
        if(tipe_laporan == "laporan1"){
            $(".kecamatan-filter").show();
            get_laporan();
        }else{
            $("select[name=kecamatan]").val("");
            $("select[name=kecamatan]").trigger("change");
            $(".kecamatan-filter").hide();
        }

        $(".single-year").show();
    }else if(tipe_laporan == "laporan3" || tipe_laporan == "laporan4"){
        $("input[name=tahun]").val("");
        if(tipe_laporan == "laporan3"){
            $(".kecamatan-filter").show();
            get_laporan();
        }else{
            $("select[name=kecamatan]").val("");
            $("select[name=kecamatan]").trigger("change");
            $(".kecamatan-filter").hide();
        }

        $(".range-year").show();
    }

    let tahun = $("input[name=tahun]").val().trim();
    let year_start = $("input[name=year_start]").val().trim();
    let year_end = $("input[name=year_end]").val().trim();
    let kecamatan = $("select[name=kecamatan]").val();

    if((year_start && year_end) || tahun){
        $(".box-top").show();
    }else{
        $(".box-top").hide();
    }
}

function get_laporan(){
    $(".box-top").hide();

    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    let jenis_usaha = $("select[name=jenis_usaha]").val();
    let tahun = $("input[name=tahun]").val().trim();
    let year_start = $("input[name=year_start]").val().trim();
    let year_end = $("input[name=year_end]").val().trim();
    let tipe_laporan = $("select[name=tipe_laporan]").val();
    let kecamatan = $("select[name=kecamatan]").val();


    datatableLaporan.clear().draw();

    if((year_start && year_end) || tahun){
        $(".box-top").hide();

        if((year_start && year_end) && (year_start > year_end)){
            $(".box-top").hide();

            swalInit(
                'Gagal',
                'Tahun awal lebih dari tahun akhir',
                'error'
            );
        }else{
            $(".box-top").show();
            // $("#cetakLaporanFormat12").attr("href",base_url+"laporan_per_jenis_usaha/cetak_laporan_12/"+tahun);
            // $("#cetakLaporanFormat3").attr("href",base_url+"laporan_per_jenis_usaha/cetak_laporan_3/"+tahun);
            // $("#cetakLaporanFormat4").attr("href",base_url+"laporan_per_jenis_usaha/cetak_laporan_4/"+tahun);
            // $("#cetakLaporanFormat5").attr("href",base_url+"laporan_per_jenis_usaha/cetak_laporan_5/"+tahun);
            // $("#cetakLaporanFormat12").attr("href",base_url+"laporan_per_jenis_usaha/cetak_laporan_12/"+year_start+"/"+year_end);
            // $("#cetakLaporanFormat3").attr("href",base_url+"laporan_per_jenis_usaha/cetak_laporan_3/"+year_start+"/"+year_end);
            // $("#cetakLaporanFormat4").attr("href",base_url+"laporan_per_jenis_usaha/cetak_laporan_4/"+year_start+"/"+year_end);
            // $("#cetakLaporanFormat5").attr("href",base_url+"laporan_per_jenis_usaha/cetak_laporan_5/"+year_start+"/"+year_end);
            $("#cetakLaporan").attr("href",base_url+"laporan_per_jenis_usaha/cetak_laporan/"+tipe_laporan+"/"+((year_start)?year_start:"no")+"/"+((year_end)?year_end:"no")+"/"+((tahun)?tahun:"no")+"/"+((kecamatan)?kecamatan:"no"));
            // $("#cetakLaporan").attr("href",base_url+"laporan_per_jenis_usaha/cetak_laporan/"+jenis_usaha+"/"+year_start+"/"+year_end);

            $.ajax({
                url: base_url+'laporan_per_jenis_usaha/request/get_laporan',
                data:{jenis_usaha:jenis_usaha,year_start:year_start,year_end:year_end,tahun:tahun,kecamatan:kecamatan},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    $.each(response,function(index,value){
                        datatableLaporan.row.add([
                            value.nama_pemilik_usaha,
                            value.nama_toko_perusahaan,
                            value.nama_jenis_usaha,
                            IDR(value.jumlah_aset).format(true),
                            IDR(value.jumlah_omset_per_tahun).format(true),
                            IDR(value.modal_sendiri).format(true),
                            IDR(value.modal_luar).format(true),
                            IDR(value.pinjaman_luar).format(true),
                            value.tahun_berkenaan
                        ]).draw(false);
                    });
                },
                complete:function(response){
                    loading_stop();
                }
            });
        }
    }
}

$('#education-range input').each(function () {
    $(this).datepicker({
        autoclose: true,
        format: " yyyy",
        viewMode: "years",
        minViewMode: "years"
    });
    $(this).datepicker('clearDates');
});
</script>