<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_per_jenis_usaha extends MY_Controller {
	function __construct(){
		parent::__construct();
        $this->load->model('jenis_usaha/jenis_usaha_model','jenis_usaha_model');
        $this->load->model('pemilik_usaha/pemilik_usaha_model','pemilik_usaha_model');
        $this->load->model('aset_omset_usaha/aset_omset_usaha_model','aset_omset_usaha_model');
		$this->load->model('data_usaha/master_wilayah_model','master_wilayah_model');
	}

	public function index()
	{
        $data['master_jenis_usaha'] = $this->jenis_usaha_model->get(
            array(
                "order_by"=>array(
                    "nama_jenis_usaha"=>"ASC"
                )
            )
        );

        $data['tipe_laporan'] = array(
            "laporan1"=>"Laporan 1",
            "laporan2"=>"Laporan 2",
            "laporan3"=>"Laporan 3",
            "laporan4"=>"Laporan 4",
        );

        $data['master_tahun'] = $this->aset_omset_usaha_model->get(
            array(
                "fields"=>"DATE_FORMAT(created_at,'%Y') AS tahun",
                "order_by"=>array(
                    "nama_jenis_usaha"=>"ASC"
                ),
                "group_by"=>"tahun",
                "order_by"=>array(
                    "tahun"=>"ASC"
                ),
                "where"=>array(
                    "is_verified"=>"1"
                )
            )
        );

        $data['list_kecamatan'] = $this->master_wilayah_model->get(
            array(
                "fields"=>"master_wilayah.*,concat(klasifikasi,' - ',nama_wilayah) as nama_wilayah",
                "where"=>array(
                    "klasifikasi"=>"KEC"
                ),
                "order_by_false"=>'field (klasifikasi,"KEC","KEL","DESA"),nama_wilayah'
            )
        );
        
        $data['breadcrumb'] = [['link'=>false,'content'=>'Laporan','is_active'=>true]];
        $this->execute('index',$data);
    }

    public function cetak_laporan($format_laporan,$year_start,$year_end,$single_year,$kecamatan){
        if($kecamatan == "no"){
            $kecamatan = "all";
        }else{
            $kecamatan = decrypt_data($kecamatan);
        }

        if($format_laporan == "laporan1"){
            $this->cetak_laporan_1($single_year,$kecamatan);
        }else if($format_laporan == "laporan2"){
            $this->cetak_laporan_2($single_year);
        }else if($format_laporan == "laporan3"){
            $this->cetak_laporan_3($year_start,$year_end,$kecamatan);
        }else if($format_laporan == "laporan4"){
            $this->cetak_laporan_4($year_start,$year_end);
        }
    }

    public function cetak_laporan_1($single_year,$kecamatan){
        $jenis_usaha = $this->session->userdata("jenis_usaha");

        require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
		require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

        $filename = "Laporan UMKM (Data Detail UMKM).xlsx";

        $style_header_table = array(
            'font' => array(
				'size' => 11,
				'bold' => true
			),'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
        
        $style_header = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
			'font' => array(
				'size' => 11,
				'bold' => true
			),'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'dae1f3')
            )
        );

        $style_header_child = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
			'font' => array(
				'size' => 9,
			),'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'e7e5e6')
            )
        );

        $style_header_jenis_usaha = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
			'font' => array(
				'size' => 11,
				'bold' => true
			),
        );
        
        $style_data = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $style_data_footer = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
            ),
            'font' => array(
				'size' => 11,
				'bold' => true
			)
        );

        $style_data_footer_tengah = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'font' => array(
				'size' => 11,
				'bold' => true
			)
        );
        
        $style_data_kiri = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT
            )
        );
        
        $style_data_tengah = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        
        $style_data_kanan = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
            )
		);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getActiveSheet()->getStyle("A4:AA4")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("A5:AA5")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("A6:AA6")->applyFromArray($style_header_child);

        $objPHPExcel->getActiveSheet()->mergeCells("A4:A5");
        $objPHPExcel->getActiveSheet()->mergeCells("B4:B5");
        $objPHPExcel->getActiveSheet()->mergeCells("C4:C5");
        $objPHPExcel->getActiveSheet()->mergeCells("D4:D5");
        $objPHPExcel->getActiveSheet()->mergeCells("E4:E5");
        $objPHPExcel->getActiveSheet()->mergeCells("F4:F5");
        $objPHPExcel->getActiveSheet()->mergeCells("G4:G5");
        $objPHPExcel->getActiveSheet()->mergeCells("H4:H5");
        $objPHPExcel->getActiveSheet()->mergeCells("I4:I5");
        $objPHPExcel->getActiveSheet()->mergeCells("J4:J5");
        
        $objPHPExcel->getActiveSheet()->mergeCells("K4:M4");
        
        $objPHPExcel->getActiveSheet()->mergeCells("N4:P4");
        
        $objPHPExcel->getActiveSheet()->mergeCells("Q4:Q5");
        $objPHPExcel->getActiveSheet()->mergeCells("R4:R5");
        $objPHPExcel->getActiveSheet()->mergeCells("S4:S5");
        $objPHPExcel->getActiveSheet()->mergeCells("T4:T5");
        $objPHPExcel->getActiveSheet()->mergeCells("U4:U5");
        $objPHPExcel->getActiveSheet()->mergeCells("V4:V5");
        
        $objPHPExcel->getActiveSheet()->mergeCells("W4:X4");
        
        $objPHPExcel->getActiveSheet()->mergeCells("Y4:Y5");
        
        $objPHPExcel->getActiveSheet()->mergeCells("Z4:AA4");

        $objPHPExcel->getActiveSheet()->SetCellValue("A1","DATA USAHA MIKRO, KECIL DAN MENENGAH");
        $objPHPExcel->getActiveSheet()->SetCellValue("A2","KABUPATEN KOTAWARINGIN BARAT TAHUN ".$single_year);
        $objPHPExcel->getActiveSheet()->mergeCells("A1:AA1");
        $objPHPExcel->getActiveSheet()->mergeCells("A2:AA2");

        $objPHPExcel->getActiveSheet()->getStyle("A1:AA2")->applyFromArray($style_header_table);
        
		$objPHPExcel->getActiveSheet()->SetCellValue("A4","No");
		$objPHPExcel->getActiveSheet()->SetCellValue("B4","Nama Pemilik Usaha");
		$objPHPExcel->getActiveSheet()->SetCellValue("C4","Nama Toko/Perusahaan");
		$objPHPExcel->getActiveSheet()->SetCellValue("D4","NIK");
		$objPHPExcel->getActiveSheet()->SetCellValue("E4","Nomor KK");
		$objPHPExcel->getActiveSheet()->SetCellValue("F4","Tempat/Tanggal Lahir");
		$objPHPExcel->getActiveSheet()->SetCellValue("G4","Jenis Kelamin");
		$objPHPExcel->getActiveSheet()->SetCellValue("H4","Status Perkawinan");
		$objPHPExcel->getActiveSheet()->SetCellValue("I4","Pendidikan Terakhir");
		$objPHPExcel->getActiveSheet()->SetCellValue("J4","Pekerjaan Inti");
        
        $objPHPExcel->getActiveSheet()->SetCellValue("K4","Alamat Pemilik");
        $objPHPExcel->getActiveSheet()->SetCellValue("K5","Desa/Kelurahan");
        $objPHPExcel->getActiveSheet()->SetCellValue("L5","Kecamatan");
        $objPHPExcel->getActiveSheet()->SetCellValue("M5","Nama Jalan");

        $objPHPExcel->getActiveSheet()->SetCellValue("N4","Alamat Usaha/Perusahaan");
        $objPHPExcel->getActiveSheet()->SetCellValue("N5","Desa/Kelurahan");
        $objPHPExcel->getActiveSheet()->SetCellValue("O5","Kecamatan");
        $objPHPExcel->getActiveSheet()->SetCellValue("P5","Nama Jalan");
        
        $objPHPExcel->getActiveSheet()->SetCellValue("Q4","Nomor Telepon");
        $objPHPExcel->getActiveSheet()->SetCellValue("R4","NPWP");
        $objPHPExcel->getActiveSheet()->SetCellValue("S4","Jenis Usaha");
        $objPHPExcel->getActiveSheet()->SetCellValue("T4","Nomor Izin Usaha");
        $objPHPExcel->getActiveSheet()->SetCellValue("U4","Jumlah Aset/Tahun (Rp.)");
        $objPHPExcel->getActiveSheet()->SetCellValue("V4","Jumlah Omset/Tahun (Rp.)");
        
        $objPHPExcel->getActiveSheet()->SetCellValue("W4","Modal Usaha");
        $objPHPExcel->getActiveSheet()->SetCellValue("W5","Modal Sendiri (Rp.)");
        $objPHPExcel->getActiveSheet()->SetCellValue("X5","Modal Luar (Rp.)");
        
        $objPHPExcel->getActiveSheet()->SetCellValue("Y4","Jumlah Pekerja");
        
        $objPHPExcel->getActiveSheet()->SetCellValue("Z4","Pinjaman Pada Lembaga Keuangan");
        $objPHPExcel->getActiveSheet()->SetCellValue("Z5","Nilai Pinjaman (Rp.)");
        $objPHPExcel->getActiveSheet()->SetCellValue("AA5","Agunan Yang Digunakan");

		$objPHPExcel->getActiveSheet()->SetCellValue("A6","1");
		$objPHPExcel->getActiveSheet()->SetCellValue("B6","2");
		$objPHPExcel->getActiveSheet()->SetCellValue("C6","3");
		$objPHPExcel->getActiveSheet()->SetCellValue("D6","4");
		$objPHPExcel->getActiveSheet()->SetCellValue("E6","5");
		$objPHPExcel->getActiveSheet()->SetCellValue("F6","6");
		$objPHPExcel->getActiveSheet()->SetCellValue("G6","7");
		$objPHPExcel->getActiveSheet()->SetCellValue("H6","8");
		$objPHPExcel->getActiveSheet()->SetCellValue("I6","9");
		$objPHPExcel->getActiveSheet()->SetCellValue("J6","10");
		$objPHPExcel->getActiveSheet()->SetCellValue("K6","11");
		$objPHPExcel->getActiveSheet()->SetCellValue("L6","12");
		$objPHPExcel->getActiveSheet()->SetCellValue("M6","13");
		$objPHPExcel->getActiveSheet()->SetCellValue("N6","14");
		$objPHPExcel->getActiveSheet()->SetCellValue("O6","15");
		$objPHPExcel->getActiveSheet()->SetCellValue("P6","16");
		$objPHPExcel->getActiveSheet()->SetCellValue("Q6","17");
		$objPHPExcel->getActiveSheet()->SetCellValue("R6","18");
		$objPHPExcel->getActiveSheet()->SetCellValue("S6","19");
		$objPHPExcel->getActiveSheet()->SetCellValue("T6","20");
		$objPHPExcel->getActiveSheet()->SetCellValue("U6","21");
		$objPHPExcel->getActiveSheet()->SetCellValue("V6","22");
        $objPHPExcel->getActiveSheet()->SetCellValue("W6","23");
        $objPHPExcel->getActiveSheet()->SetCellValue("X6","24");
        $objPHPExcel->getActiveSheet()->SetCellValue("Y6","25");
        $objPHPExcel->getActiveSheet()->SetCellValue("Z6","26");
        $objPHPExcel->getActiveSheet()->SetCellValue("AA6","27");

        $con_where = array();
        if($kecamatan == "all"){
            $con_where = array(
                "klasifikasi"=>"KEC"
            );
        }else{
            $con_where = array(
                "klasifikasi"=>"KEC",
                "kode_wilayah"=>$kecamatan
            );
        }

        $data_master_wilayah_kecamatan = $this->master_wilayah_model->get(
            array(
                "where"=>$con_where,
                "order_by"=>array(
                    "nama_wilayah"=>"ASC"
                )
            )
        );

        $number_column = 7;
        $jumlah_aset_pertahun = 0;
        $jumlah_omset_pertahun = 0;
        $modal_sendiri = 0;
        $modal_luar = 0;
        $jumlah_pekerja = 0;
        $nilai_pinjaman = 0;
        
        foreach($data_master_wilayah_kecamatan as $key_kecamatan=>$row_kecamatan){
            $objPHPExcel->getActiveSheet()->mergeCells("A".$number_column.":AA".$number_column);
            $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,ucwords(strtolower("Kecamatan ".$row_kecamatan->nama_wilayah)));
            $objPHPExcel->getActiveSheet()->getStyle("A".$number_column.":AA".$number_column)->applyFromArray($style_header_jenis_usaha);
            
            
            $number_column++;
            
            $wh_str = "";
            if($jenis_usaha != "all"){
                $wh_str = "AND id_jenis_usaha IN (".$jenis_usaha.")";
            }
            
            $data_jenis_usaha = $this->jenis_usaha_model->get(
                array(
                    "fields"=>"master_jenis_usaha.*",
                    "join"=>array(
                        "master_data_usaha"=>"master_jenis_usaha_id=id_jenis_usaha AND master_data_usaha.deleted_at IS NULL AND master_data_usaha.is_verified = 1",
                        "master_wilayah"=>"id_master_wilayah=kelurahan_master_wilayah_id AND kode_induk=".$row_kecamatan->kode_wilayah,
                        "aset_omset_usaha"=>"master_data_usaha_id=id_data_usaha AND aset_omset_usaha.deleted_at IS NULL and aset_omset_usaha.is_verified=1"
                    ),
                    "where_false"=>"tahun_berkenaan = ".$single_year." ".$wh_str,
                    "order_by"=>array(
                        "nama_jenis_usaha"=>"ASC"
                    ),
                    "group_by"=>"id_jenis_usaha"
                )
            );
            
            $jumlah_aset_per_kecamatan = 0;
            $jumlah_omset_per_kecamatan = 0;
            $jumlah_modal_sendiri_per_kecamatan = 0;
            $jumlah_modal_luar_per_kecamatan = 0;
            $jumlah_pekerja_per_kecamatan = 0;
            $jumlah_nilai_pinjaman_per_kecamatan = 0;
            foreach($data_jenis_usaha as $key=>$row){
                $objPHPExcel->getActiveSheet()->mergeCells("A".$number_column.":AA".$number_column);
                $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,ucwords(strtolower($row->nama_jenis_usaha)));
                $objPHPExcel->getActiveSheet()->getStyle("A".$number_column.":AA".$number_column)->applyFromArray($style_header_jenis_usaha);
                
                $data_usaha = $this->aset_omset_usaha_model->get(
                    array(
                        "fields"=>"aset_omset_usaha.*,nama_pemilik_usaha,nik,nomor_kk,tempat_lahir,tanggal_lahir,jenis_kelamin,status_perkawinan,pendidikan_terakhir,pekerjaan_inti,master_pemilik_usaha.alamat AS alamat_pemilik,master_pemilik_usaha.rt_rw as rt_rw_pemilik,nomor_telepon,npwp,alamat_usaha,master_data_usaha.rt_rw as rt_rw_usaha,no_izin_usaha,nama_toko_perusahaan,nama_jenis_usaha,jumlah_aset,jumlah_omset_per_tahun,modal_sendiri,modal_luar,jumlah_pekerja,pinjaman_luar,agunan_modal,pendidikan_terakhir_lainnya,pekerjaan_inti_lainnya,master_pemilik_usaha.kelurahan_master_wilayah_id as pemilik_kel_desa, master_data_usaha.kelurahan_master_wilayah_id as usaha_kel_desa,DATE_FORMAT(aset_omset_usaha.created_at,'%Y') AS tahun",
                        "join"=>array(
                            "master_data_usaha"=>"id_data_usaha=master_data_usaha_id AND master_data_usaha.deleted_at IS NULL AND master_data_usaha.is_verified = 1",
                            "master_pemilik_usaha"=>"id_pemilik_usaha=master_pemilik_usaha_id AND master_pemilik_usaha.deleted_at IS NULL AND master_pemilik_usaha.is_verified = 1",
                            "master_jenis_usaha"=>"id_jenis_usaha=master_jenis_usaha_id",
                            "master_wilayah"=>"id_master_wilayah=master_data_usaha.kelurahan_master_wilayah_id AND kode_induk=".$row_kecamatan->kode_wilayah
                        ),
                        "where"=> array(
                            "aset_omset_usaha.is_verified"=>"1",
                            "master_jenis_usaha_id"=>$row->id_jenis_usaha
                        ),
                        "where_false"=>"tahun_berkenaan = ".$single_year
                        )
                    );
                    
                    $no_data = 1;
                    $number_column++;
                    foreach($data_usaha as $key_data_usaha=>$row_data_usaha){
                    $created_at = strtotime($row_data_usaha->created_at);
                    $tahun_n = date("Y",$created_at);

                    $data_master_pemilik = $this->master_wilayah_model->get_by($row_data_usaha->pemilik_kel_desa);

                    $data_kecamatan_pemilik = $this->master_wilayah_model->get(
                        array(
                            "fields"=>"nama_wilayah,kode_induk",
                            "where"=>array(
                                "kode_wilayah"=>$data_master_pemilik->kode_induk
                            )
                        ),"row"
                    );
                    
                    $data_kabupaten_pemilik = $this->master_wilayah_model->get(
                        array(
                            "fields"=>"nama_wilayah,kode_induk",
                            "where"=>array(
                                "kode_wilayah"=>$data_kecamatan_pemilik->kode_induk
                            )
                        ),"row"
                    );

                    $data_master_usaha = $this->master_wilayah_model->get_by($row_data_usaha->usaha_kel_desa);

                    $data_kecamatan_usaha = $this->master_wilayah_model->get(
                        array(
                            "fields"=>"nama_wilayah,kode_induk",
                            "where"=>array(
                                "kode_wilayah"=>$data_master_usaha->kode_induk
                            )
                        ),"row"
                    );
                    
                    $data_kabupaten_usaha = $this->master_wilayah_model->get(
                        array(
                            "fields"=>"nama_wilayah,kode_induk",
                            "where"=>array(
                                "kode_wilayah"=>$data_kecamatan_usaha->kode_induk
                            )
                        ),"row"
                    );

                    $data_provinsi_usaha = $this->master_wilayah_model->get(
                        array(
                            "fields"=>"nama_wilayah,kode_induk",
                            "where"=>array(
                                "kode_wilayah"=>$data_kabupaten_usaha->kode_induk
                            )
                        ),"row"
                    );

                    $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,$no_data);
                    $objPHPExcel->getActiveSheet()->getStyle("A".$number_column)->applyFromArray($style_data_tengah);

                    $objPHPExcel->getActiveSheet()->SetCellValue("B".$number_column,$row_data_usaha->nama_pemilik_usaha);
                    $objPHPExcel->getActiveSheet()->getStyle("B".$number_column)->applyFromArray($style_data);

                    $objPHPExcel->getActiveSheet()->SetCellValue("C".$number_column,$row_data_usaha->nama_toko_perusahaan);
                    $objPHPExcel->getActiveSheet()->getStyle("C".$number_column)->applyFromArray($style_data);

                    $objPHPExcel->getActiveSheet()->SetCellValue("D".$number_column,$row_data_usaha->nik);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                        "D".$number_column,
                        $row_data_usaha->nik,
                        PHPExcel_Cell_DataType::TYPE_STRING
                    );
                    $objPHPExcel->getActiveSheet()->getStyle("D".$number_column)->applyFromArray($style_data_kiri);

                    $objPHPExcel->getActiveSheet()->SetCellValue("E".$number_column,$row_data_usaha->nomor_kk);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                        "E".$number_column,
                        $row_data_usaha->nomor_kk,
                        PHPExcel_Cell_DataType::TYPE_STRING
                    );
                    $objPHPExcel->getActiveSheet()->getStyle("E".$number_column)->applyFromArray($style_data_kiri);

                    $objPHPExcel->getActiveSheet()->SetCellValue("F".$number_column,$row_data_usaha->tempat_lahir.", ".date("d-m-Y",strtotime($row_data_usaha->tanggal_lahir)));
                    $objPHPExcel->getActiveSheet()->getStyle("F".$number_column)->applyFromArray($style_data);

                    $objPHPExcel->getActiveSheet()->SetCellValue("G".$number_column,$row_data_usaha->jenis_kelamin);
                    $objPHPExcel->getActiveSheet()->getStyle("G".$number_column)->applyFromArray($style_data_tengah);

                    $objPHPExcel->getActiveSheet()->SetCellValue("H".$number_column,$row_data_usaha->status_perkawinan);
                    $objPHPExcel->getActiveSheet()->getStyle("H".$number_column)->applyFromArray($style_data);

                    $objPHPExcel->getActiveSheet()->SetCellValue("I".$number_column,($row_data_usaha->pendidikan_terakhir != "Lainnya"?$row_data_usaha->pendidikan_terakhir:$row_data_usaha->pendidikan_terakhir_lainnya));
                    $objPHPExcel->getActiveSheet()->getStyle("I".$number_column)->applyFromArray($style_data);

                    $objPHPExcel->getActiveSheet()->SetCellValue("J".$number_column,($row_data_usaha->pekerjaan_inti != "Lainnya"?$row_data_usaha->pekerjaan_inti:$row_data_usaha->pekerjaan_inti_lainnya));
                    $objPHPExcel->getActiveSheet()->getStyle("J".$number_column)->applyFromArray($style_data);

                    $objPHPExcel->getActiveSheet()->SetCellValue("K".$number_column,$data_master_pemilik->nama_wilayah);
                    $objPHPExcel->getActiveSheet()->getStyle("K".$number_column)->applyFromArray($style_data);
                    
                    $objPHPExcel->getActiveSheet()->SetCellValue("L".$number_column,ucwords(strtolower($data_kecamatan_pemilik->nama_wilayah)));
                    $objPHPExcel->getActiveSheet()->getStyle("L".$number_column)->applyFromArray($style_data);
                    
                    $objPHPExcel->getActiveSheet()->SetCellValue("M".$number_column,$row_data_usaha->alamat_pemilik." RT/RW ".$row_data_usaha->rt_rw_pemilik);
                    $objPHPExcel->getActiveSheet()->getStyle("M".$number_column)->applyFromArray($style_data);

                    $objPHPExcel->getActiveSheet()->SetCellValue("N".$number_column,$data_master_usaha->nama_wilayah);
                    $objPHPExcel->getActiveSheet()->getStyle("N".$number_column)->applyFromArray($style_data);
                    
                    $objPHPExcel->getActiveSheet()->SetCellValue("O".$number_column,ucwords(strtolower($data_kecamatan_usaha->nama_wilayah)));
                    $objPHPExcel->getActiveSheet()->getStyle("O".$number_column)->applyFromArray($style_data);
                    
                    $objPHPExcel->getActiveSheet()->SetCellValue("P".$number_column,$row_data_usaha->alamat_usaha." RT/RW ".$row_data_usaha->rt_rw_usaha);
                    $objPHPExcel->getActiveSheet()->getStyle("P".$number_column)->applyFromArray($style_data);

                    $objPHPExcel->getActiveSheet()->SetCellValue("Q".$number_column,$row_data_usaha->nomor_telepon);
                    $objPHPExcel->getActiveSheet()->getStyle("Q".$number_column)->applyFromArray($style_data);

                    $objPHPExcel->getActiveSheet()->SetCellValue("R".$number_column,$row_data_usaha->npwp);
                    $objPHPExcel->getActiveSheet()->getStyle("R".$number_column)->applyFromArray($style_data_kiri);

                    $objPHPExcel->getActiveSheet()->SetCellValue("S".$number_column,$row_data_usaha->nama_jenis_usaha);
                    $objPHPExcel->getActiveSheet()->getStyle("S".$number_column)->applyFromArray($style_data);

                    $objPHPExcel->getActiveSheet()->SetCellValue("T".$number_column,$row_data_usaha->no_izin_usaha);
                    $objPHPExcel->getActiveSheet()->getStyle("T".$number_column)->applyFromArray($style_data);

                    $objPHPExcel->getActiveSheet()->SetCellValue("U".$number_column,nominal($row_data_usaha->jumlah_aset));
                    $objPHPExcel->getActiveSheet()->getStyle("U".$number_column)->applyFromArray($style_data_kanan);
                    $jumlah_aset_pertahun += $row_data_usaha->jumlah_aset;
                    $jumlah_aset_per_kecamatan += $row_data_usaha->jumlah_aset;

                    $objPHPExcel->getActiveSheet()->SetCellValue("V".$number_column,nominal($row_data_usaha->jumlah_omset_per_tahun));
                    $objPHPExcel->getActiveSheet()->getStyle("V".$number_column)->applyFromArray($style_data_kanan);
                    $jumlah_omset_pertahun += $row_data_usaha->jumlah_omset_per_tahun;
                    $jumlah_omset_per_kecamatan += $row_data_usaha->jumlah_omset_per_tahun;

                    $objPHPExcel->getActiveSheet()->SetCellValue("W".$number_column,nominal($row_data_usaha->modal_sendiri));
                    $objPHPExcel->getActiveSheet()->getStyle("W".$number_column)->applyFromArray($style_data_kanan);
                    $modal_sendiri += $row_data_usaha->modal_sendiri;
                    $jumlah_modal_sendiri_per_kecamatan += $row_data_usaha->modal_sendiri;

                    $objPHPExcel->getActiveSheet()->SetCellValue("X".$number_column,nominal($row_data_usaha->modal_luar));
                    $objPHPExcel->getActiveSheet()->getStyle("X".$number_column)->applyFromArray($style_data_kanan);
                    $modal_luar += $row_data_usaha->modal_luar;
                    $jumlah_modal_luar_per_kecamatan += $row_data_usaha->modal_luar;

                    $objPHPExcel->getActiveSheet()->SetCellValue("Y".$number_column,$row_data_usaha->jumlah_pekerja);
                    $objPHPExcel->getActiveSheet()->getStyle("Y".$number_column)->applyFromArray($style_data_tengah);
                    $jumlah_pekerja += $row_data_usaha->jumlah_pekerja;
                    $jumlah_pekerja_per_kecamatan += $row_data_usaha->jumlah_pekerja;

                    $objPHPExcel->getActiveSheet()->SetCellValue("Z".$number_column,nominal($row_data_usaha->pinjaman_luar));
                    $objPHPExcel->getActiveSheet()->getStyle("Z".$number_column)->applyFromArray($style_data_kanan);
                    $nilai_pinjaman += $row_data_usaha->pinjaman_luar;
                    $jumlah_nilai_pinjaman_per_kecamatan += $row_data_usaha->pinjaman_luar;

                    $objPHPExcel->getActiveSheet()->SetCellValue("AA".$number_column,$row_data_usaha->agunan_modal);
                    $objPHPExcel->getActiveSheet()->getStyle("AA".$number_column)->applyFromArray($style_data);

                    $no_data++;
                    $number_column++;
                }
            }

            $objPHPExcel->getActiveSheet()->mergeCells("A".$number_column.":T".$number_column);
            $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,"JUMLAH");
            $objPHPExcel->getActiveSheet()->getStyle("A".$number_column.":T".$number_column)->applyFromArray($style_data_footer);
            
            $objPHPExcel->getActiveSheet()->SetCellValue("U".$number_column,nominal($jumlah_aset_per_kecamatan));
            $objPHPExcel->getActiveSheet()->getStyle("U".$number_column)->applyFromArray($style_data_footer);
            
            $objPHPExcel->getActiveSheet()->SetCellValue("V".$number_column,nominal($jumlah_omset_per_kecamatan));
            $objPHPExcel->getActiveSheet()->getStyle("V".$number_column)->applyFromArray($style_data_footer);
            
            $objPHPExcel->getActiveSheet()->SetCellValue("W".$number_column,nominal($jumlah_modal_sendiri_per_kecamatan));
            $objPHPExcel->getActiveSheet()->getStyle("W".$number_column)->applyFromArray($style_data_footer);
            
            $objPHPExcel->getActiveSheet()->SetCellValue("X".$number_column,nominal($jumlah_modal_luar_per_kecamatan));
            $objPHPExcel->getActiveSheet()->getStyle("X".$number_column)->applyFromArray($style_data_footer);
            
            $objPHPExcel->getActiveSheet()->SetCellValue("Y".$number_column,$jumlah_pekerja_per_kecamatan);
            $objPHPExcel->getActiveSheet()->getStyle("Y".$number_column)->applyFromArray($style_data_footer_tengah);
            
            $objPHPExcel->getActiveSheet()->SetCellValue("Z".$number_column,nominal($jumlah_nilai_pinjaman_per_kecamatan));
            $objPHPExcel->getActiveSheet()->getStyle("Z".$number_column)->applyFromArray($style_data_footer);

            $objPHPExcel->getActiveSheet()->getStyle("AA".$number_column)->applyFromArray($style_data_footer);
            $number_column++;

        }

        $objPHPExcel->getActiveSheet()->mergeCells("A".$number_column.":T".$number_column);
        $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,"TOTAL");
        $objPHPExcel->getActiveSheet()->getStyle("A".$number_column.":T".$number_column)->applyFromArray($style_data_footer);

        $objPHPExcel->getActiveSheet()->SetCellValue("U".$number_column,nominal($jumlah_aset_pertahun));
        $objPHPExcel->getActiveSheet()->getStyle("U".$number_column)->applyFromArray($style_data_footer);

        $objPHPExcel->getActiveSheet()->SetCellValue("V".$number_column,nominal($jumlah_omset_pertahun));
        $objPHPExcel->getActiveSheet()->getStyle("V".$number_column)->applyFromArray($style_data_footer);

        $objPHPExcel->getActiveSheet()->SetCellValue("W".$number_column,nominal($modal_sendiri));
        $objPHPExcel->getActiveSheet()->getStyle("W".$number_column)->applyFromArray($style_data_footer);

        $objPHPExcel->getActiveSheet()->SetCellValue("X".$number_column,nominal($modal_luar));
        $objPHPExcel->getActiveSheet()->getStyle("X".$number_column)->applyFromArray($style_data_footer);

        $objPHPExcel->getActiveSheet()->SetCellValue("Y".$number_column,$jumlah_pekerja);
        $objPHPExcel->getActiveSheet()->getStyle("Y".$number_column)->applyFromArray($style_data_footer_tengah);

        $objPHPExcel->getActiveSheet()->SetCellValue("Z".$number_column,nominal($nilai_pinjaman));
        $objPHPExcel->getActiveSheet()->getStyle("Z".$number_column)->applyFromArray($style_data_footer);
        $objPHPExcel->getActiveSheet()->getStyle("AA".$number_column)->applyFromArray($style_data_footer);

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
		$jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

		$col = 'A';
		while(true){
			$tempCol = $col++;
			$objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
			if($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()){
				break;
			}
		}

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('15');
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth('15');
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth('20');
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(false);
        
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
		
		$writer->save('php://output');
		exit;
    }

    public function cetak_laporan_2($single_year){
        $jenis_usaha = $this->session->userdata("jenis_usaha");

        require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
		require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

        $filename = "Laporan UMKM (Data Rekap UMKM).xlsx";

        $style_header_table = array(
            'font' => array(
				'size' => 11,
				'bold' => true
			),'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $style_data = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $style_data_footer = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
            ),
            'font' => array(
				'size' => 11,
				'bold' => true
			)
        );
        
        $style_data_footer_tengah = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'font' => array(
				'size' => 11,
				'bold' => true
			)
        );

        $style_header = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
			'font' => array(
				'size' => 11,
				'bold' => true
			),'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'dae1f3')
            )
        );

        $style_header_child = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
			'font' => array(
				'size' => 9,
			),'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'e7e5e6')
            )
        );

        $style_data_kiri = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT
            )
        );
        
        $style_data_tengah = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        
        $style_data_kanan = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
            )
		);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getActiveSheet()->getStyle("A4:H4")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("A5:H5")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("A6:H6")->applyFromArray($style_header_child);
        
        $objPHPExcel->getActiveSheet()->SetCellValue("A1","DATA USAHA MIKRO, KECIL DAN MENENGAH");
        $objPHPExcel->getActiveSheet()->SetCellValue("A2","KABUPATEN KOTAWARINGIN BARAT TAHUN ".$single_year);
        $objPHPExcel->getActiveSheet()->mergeCells("A1:H1");
        $objPHPExcel->getActiveSheet()->mergeCells("A2:H2");

        $objPHPExcel->getActiveSheet()->getStyle("A1:H2")->applyFromArray($style_header_table);

        $objPHPExcel->getActiveSheet()->mergeCells("A4:A5");
        $objPHPExcel->getActiveSheet()->mergeCells("B4:B5");
        $objPHPExcel->getActiveSheet()->mergeCells("C4:C5");
        $objPHPExcel->getActiveSheet()->mergeCells("D4:D5");

        $objPHPExcel->getActiveSheet()->mergeCells("E4:F4");
        
        $objPHPExcel->getActiveSheet()->mergeCells("G4:G5");
        $objPHPExcel->getActiveSheet()->mergeCells("H4:H5");

		$objPHPExcel->getActiveSheet()->SetCellValue("A4","NO");
		$objPHPExcel->getActiveSheet()->SetCellValue("B4","Jenis Usaha");
		$objPHPExcel->getActiveSheet()->SetCellValue("C4","Jumlah Aset/Tahun (Rp.)");
        $objPHPExcel->getActiveSheet()->SetCellValue("D4","Jumlah Omset/Tahun (Rp.)");
        
        $objPHPExcel->getActiveSheet()->SetCellValue("E4","Modal Usaha");
        $objPHPExcel->getActiveSheet()->SetCellValue("E5","Modal Sendiri (Rp.)");
        $objPHPExcel->getActiveSheet()->SetCellValue("F5","Modal Luar (Rp.)");
        
        $objPHPExcel->getActiveSheet()->SetCellValue("G4","Jumlah Pekerja");
        $objPHPExcel->getActiveSheet()->SetCellValue("H4","Nilai Pinjaman (Rp.)");

		$objPHPExcel->getActiveSheet()->SetCellValue("A6","1");
		$objPHPExcel->getActiveSheet()->SetCellValue("B6","2");
		$objPHPExcel->getActiveSheet()->SetCellValue("C6","3");
		$objPHPExcel->getActiveSheet()->SetCellValue("D6","4");
		$objPHPExcel->getActiveSheet()->SetCellValue("E6","5");
		$objPHPExcel->getActiveSheet()->SetCellValue("F6","6");
		$objPHPExcel->getActiveSheet()->SetCellValue("G6","7");
		$objPHPExcel->getActiveSheet()->SetCellValue("H6","8");
        
        $wh_str = "";
        if($jenis_usaha != "all"){
            $wh_str = " AND master_jenis_usaha_id IN (".$jenis_usaha.")";
        }

        $data_jenis_usaha = $this->jenis_usaha_model->query("
        SELECT *
        FROM master_jenis_usaha
        JOIN (
            SELECT SUM(jumlah_aset) AS jumlah_aset, SUM(jumlah_omset_per_tahun) AS jumlah_omset_per_tahun, SUM(modal_sendiri) AS modal_sendiri, SUM(modal_luar) AS modal_luar, SUM(jumlah_pekerja) AS jumlah_pekerja, SUM(pinjaman_luar) AS pinjaman_luar,master_jenis_usaha_id
            FROM master_data_usaha
            INNER JOIN aset_omset_usaha ON id_data_usaha=master_data_usaha_id AND aset_omset_usaha.`deleted_at` IS NULL AND aset_omset_usaha.`is_verified` = '1'
            WHERE tahun_berkenaan = ".$single_year." AND master_data_usaha.`deleted_at` IS NULL AND master_data_usaha.`is_verified` = '1' ".$wh_str."
            GROUP BY master_jenis_usaha_id
            ) AS a ON a.master_jenis_usaha_id=id_jenis_usaha
        WHERE master_jenis_usaha.deleted_at IS NULL
        ")->result();        

        $number_column = 7;
        $no_data = 1;
        $jumlah_aset_pertahun = 0;
        $jumlah_omset_pertahun = 0;
        $modal_sendiri = 0;
        $modal_luar = 0;
        $jumlah_pekerja = 0;
        $nilai_pinjaman = 0;

        foreach($data_jenis_usaha as $key=>$row){
            $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,$no_data);
            $objPHPExcel->getActiveSheet()->getStyle("A".$number_column)->applyFromArray($style_data_tengah);
            
            $objPHPExcel->getActiveSheet()->SetCellValue("B".$number_column,$row->nama_jenis_usaha);
            $objPHPExcel->getActiveSheet()->getStyle("B".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("C".$number_column,nominal($row->jumlah_aset));
            $objPHPExcel->getActiveSheet()->getStyle("C".$number_column)->applyFromArray($style_data_kanan);
            $jumlah_aset_pertahun += $row->jumlah_aset;

            $objPHPExcel->getActiveSheet()->SetCellValue("D".$number_column,nominal($row->jumlah_omset_per_tahun));
            $objPHPExcel->getActiveSheet()->getStyle("D".$number_column)->applyFromArray($style_data_kanan);
            $jumlah_omset_pertahun += $row->jumlah_omset_per_tahun;
            
            $objPHPExcel->getActiveSheet()->SetCellValue("E".$number_column,nominal($row->modal_sendiri));
            $objPHPExcel->getActiveSheet()->getStyle("E".$number_column)->applyFromArray($style_data_kanan);
            $modal_sendiri += $row->modal_sendiri;
            
            $objPHPExcel->getActiveSheet()->SetCellValue("F".$number_column,nominal($row->modal_luar));
            $objPHPExcel->getActiveSheet()->getStyle("F".$number_column)->applyFromArray($style_data_kanan);
            $modal_luar += $row->modal_luar;
            
            $objPHPExcel->getActiveSheet()->SetCellValue("G".$number_column,$row->jumlah_pekerja);
            $objPHPExcel->getActiveSheet()->getStyle("G".$number_column)->applyFromArray($style_data_tengah);
            $jumlah_pekerja += $row->jumlah_pekerja;
            
            $objPHPExcel->getActiveSheet()->SetCellValue("H".$number_column,nominal($row->pinjaman_luar));
            $objPHPExcel->getActiveSheet()->getStyle("H".$number_column)->applyFromArray($style_data_kanan);
            $nilai_pinjaman += $row->pinjaman_luar;
            $no_data++;
            $number_column++;            
        }

        $objPHPExcel->getActiveSheet()->mergeCells("A".$number_column.":B".$number_column);
        $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,"JUMLAH");
        $objPHPExcel->getActiveSheet()->getStyle("A".$number_column.":B".$number_column)->applyFromArray($style_data_footer);

        $objPHPExcel->getActiveSheet()->SetCellValue("C".$number_column,nominal($jumlah_aset_pertahun));
        $objPHPExcel->getActiveSheet()->getStyle("C".$number_column)->applyFromArray($style_data_footer);

        $objPHPExcel->getActiveSheet()->SetCellValue("D".$number_column,nominal($jumlah_omset_pertahun));
        $objPHPExcel->getActiveSheet()->getStyle("D".$number_column)->applyFromArray($style_data_footer);

        $objPHPExcel->getActiveSheet()->SetCellValue("E".$number_column,nominal($modal_sendiri));
        $objPHPExcel->getActiveSheet()->getStyle("E".$number_column)->applyFromArray($style_data_footer);

        $objPHPExcel->getActiveSheet()->SetCellValue("F".$number_column,nominal($modal_luar));
        $objPHPExcel->getActiveSheet()->getStyle("F".$number_column)->applyFromArray($style_data_footer);

        $objPHPExcel->getActiveSheet()->SetCellValue("G".$number_column,$jumlah_pekerja);
        $objPHPExcel->getActiveSheet()->getStyle("G".$number_column)->applyFromArray($style_data_footer_tengah);

        $objPHPExcel->getActiveSheet()->SetCellValue("H".$number_column,nominal($nilai_pinjaman));
        $objPHPExcel->getActiveSheet()->getStyle("H".$number_column)->applyFromArray($style_data_footer);

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
		$jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

		$col = 'A';
		while(true){
			$tempCol = $col++;
			$objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
			if($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()){
				break;
			}
		}

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);

        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('15');
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(false);
        
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
		
		$writer->save('php://output');
		exit;
    }

    public function cetak_laporan_3($year_start,$year_end,$kecamatan){
        $jenis_usaha = $this->session->userdata("jenis_usaha");

        require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
		require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

        $filename = "Laporan UMKM (Data Detail Perkembangan Aset & Omset UMKM).xlsx";

        $style_header_table = array(
            'font' => array(
				'size' => 11,
				'bold' => true
			),'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $style_data = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $style_data_footer = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
            ),
            'font' => array(
				'size' => 11,
				'bold' => true
			)
        );

        $style_header = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
			'font' => array(
				'size' => 11,
				'bold' => true
			),'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'dae1f3')
            )
        );

        $style_header_child = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
			'font' => array(
				'size' => 9,
			),'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'e7e5e6')
            )
        );

        $style_header_kecamatan = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
			'font' => array(
				'size' => 11,
				'bold' => true
			),
        );

        $style_data_kiri = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT
            )
        );
        
        $style_data_tengah = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        
        $style_data_kanan = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
            )
		);

        $objPHPExcel = new PHPExcel();
        
        $objPHPExcel->getActiveSheet()->SetCellValue("A1","DATA USAHA MIKRO, KECIL DAN MENENGAH");
        $objPHPExcel->getActiveSheet()->SetCellValue("A2","KABUPATEN KOTAWARINGIN BARAT TAHUN ".$year_start." - ".$year_end);

        $letter_master_header = "E";
        for($i=$year_start;$i<=$year_end;$i++){
            $letter_master_header++;
            $letter_master_header++;
        }

        $objPHPExcel->getActiveSheet()->mergeCells("A1:".$letter_master_header."1");
        $objPHPExcel->getActiveSheet()->mergeCells("A2:".$letter_master_header."2");
        $objPHPExcel->getActiveSheet()->mergeCells("A3:".$letter_master_header."3");

        $objPHPExcel->getActiveSheet()->getStyle("A1:".$letter_master_header."3")->applyFromArray($style_header_table);

        $objPHPExcel->getActiveSheet()->mergeCells("A4:A5");
        $objPHPExcel->getActiveSheet()->mergeCells("B4:B5");
        $objPHPExcel->getActiveSheet()->mergeCells("C4:C5");
        $objPHPExcel->getActiveSheet()->mergeCells("D4:D5");
        $objPHPExcel->getActiveSheet()->mergeCells("E4:E5");

        $objPHPExcel->getActiveSheet()->getStyle("A4:".$letter_master_header."4")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("A5:".$letter_master_header."5")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("A6:".$letter_master_header."6")->applyFromArray($style_header_child);
        
		$objPHPExcel->getActiveSheet()->SetCellValue("A4","No");
        $objPHPExcel->getActiveSheet()->SetCellValue("B4","Nama Pemilik Usaha");
        $objPHPExcel->getActiveSheet()->SetCellValue("C4","Nama Toko/Perusahaan");
        $objPHPExcel->getActiveSheet()->SetCellValue("D4","NIK");
        $objPHPExcel->getActiveSheet()->SetCellValue("E4","Nomor Izin Usaha");
        $objPHPExcel->getActiveSheet()->SetCellValue("A6","1");
        $objPHPExcel->getActiveSheet()->SetCellValue("B6","2");
        $objPHPExcel->getActiveSheet()->SetCellValue("C6","3");
        $objPHPExcel->getActiveSheet()->SetCellValue("D6","4");
        $objPHPExcel->getActiveSheet()->SetCellValue("E6","5");
        
        $letter_header_year = "F";
        $letter_header_column_start = "F";
        $no_header_cell = 6;
        for($i=$year_start;$i<=$year_end;$i++){
            $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."4","Tahun ".$i);
            $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."5","Nilai Aset/Tahun (Rp.)");
            $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."6",$no_header_cell);
            $objPHPExcel->getActiveSheet()->getColumnDimension($letter_header_year)->setWidth('25');
            $objPHPExcel->getActiveSheet()->getColumnDimension($letter_header_year)->setAutoSize(false);
            $letter_header_year++;
            $no_header_cell++;
            $objPHPExcel->getActiveSheet()->mergeCells($letter_header_column_start."4:".$letter_header_year."4");
            $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."5","Nilai Omset/Tahun (Rp.)");
            $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."6",$no_header_cell);
            $objPHPExcel->getActiveSheet()->getColumnDimension($letter_header_year)->setWidth('25');
            $objPHPExcel->getActiveSheet()->getColumnDimension($letter_header_year)->setAutoSize(false);
            $letter_header_year++;
            $no_header_cell++;
            $letter_header_column_start = $letter_header_year;
        }
        
        $con_where = array();
        if($kecamatan == "all"){
            $con_where = array(
                "klasifikasi"=>"KEC"
            );
        }else{
            $con_where = array(
                "klasifikasi"=>"KEC",
                "kode_wilayah"=>$kecamatan
            );
        }

        $data_master_wilayah_kecamatan = $this->master_wilayah_model->get(
            array(
                "where"=>$con_where,
                "order_by"=>array(
                    "nama_wilayah"=>"ASC"
                )
            )
        );        

        $number_column = 7;
        $number_colum_aset_omset = 7;

        foreach($data_master_wilayah_kecamatan as $key=>$row){

            $objPHPExcel->getActiveSheet()->mergeCells("A".$number_column.":".$letter_master_header.$number_column);
            $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,"Kecamatan ".ucwords(strtolower("Kecamatan ".$row->nama_wilayah)));
            $objPHPExcel->getActiveSheet()->getStyle("A".$number_column.":".$letter_master_header.$number_column)->applyFromArray($style_header_kecamatan);

            $wh_str = "";
            if($jenis_usaha != "all"){
                $wh_str = "AND id_jenis_usaha IN (".$jenis_usaha.")";
            }
            
            $data_jenis_usaha = $this->jenis_usaha_model->get(
                array(
                    "fields"=>"master_jenis_usaha.*",
                    "join"=>array(
                        "master_data_usaha"=>"master_jenis_usaha_id=id_jenis_usaha AND master_data_usaha.deleted_at IS NULL AND master_data_usaha.is_verified = 1",
                        "master_wilayah"=>"id_master_wilayah=kelurahan_master_wilayah_id AND kode_induk=".$row->kode_wilayah,
                        "aset_omset_usaha"=>"master_data_usaha_id=id_data_usaha AND aset_omset_usaha.deleted_at IS NULL and aset_omset_usaha.is_verified=1"
                    ),
                    "where_false"=>"tahun_berkenaan BETWEEN ".$year_start." AND ".$year_end." ".$wh_str,
                    "order_by"=>array(
                        "nama_jenis_usaha"=>"ASC"
                    ),
                    "group_by"=>"id_jenis_usaha"
                )
            );

            $number_column++;
            $number_colum_aset_omset++;
            foreach($data_jenis_usaha as $key_jenis_usaha=>$row_jenis_usaha){
                
                $objPHPExcel->getActiveSheet()->mergeCells("A".$number_column.":".$letter_master_header.$number_column);
                $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,$row_jenis_usaha->nama_jenis_usaha);
                $objPHPExcel->getActiveSheet()->getStyle("A".$number_column.":".$letter_master_header.$number_column)->applyFromArray($style_header_kecamatan);

                $data_pemilik_usaha = $this->pemilik_usaha_model->get(
                    array(
                        "fields"=>"master_pemilik_usaha.*,nama_toko_perusahaan,no_izin_usaha",
                        "join"=>array(
                            "master_data_usaha"=>"id_pemilik_usaha=master_pemilik_usaha_id AND master_data_usaha.deleted_at IS NULL AND master_data_usaha.is_verified = 1",
                            "master_jenis_usaha"=>"id_jenis_usaha=master_jenis_usaha_id",
                            "master_wilayah"=>"id_master_wilayah=master_data_usaha.kelurahan_master_wilayah_id AND kode_induk=".$row->kode_wilayah
                        ),
                        "where"=> array(
                            "master_pemilik_usaha.is_verified"=>"1",
                            "master_jenis_usaha_id"=>$row_jenis_usaha->id_jenis_usaha
                        )
                    )
                );

                $no_data = 1;
                $number_column++;
                $number_colum_aset_omset++;
                foreach($data_pemilik_usaha as $key_data_pemilik_usaha=>$row_data_pemilik_usaha){

                    $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,$no_data);
                    $objPHPExcel->getActiveSheet()->getStyle("A".$number_column)->applyFromArray($style_data_tengah);

                    $objPHPExcel->getActiveSheet()->SetCellValue("B".$number_column,$row_data_pemilik_usaha->nama_pemilik_usaha);
                    $objPHPExcel->getActiveSheet()->getStyle("B".$number_column)->applyFromArray($style_data);

                    $objPHPExcel->getActiveSheet()->SetCellValue("C".$number_column,$row_data_pemilik_usaha->nama_toko_perusahaan);
                    $objPHPExcel->getActiveSheet()->getStyle("C".$number_column)->applyFromArray($style_data);

                    $objPHPExcel->getActiveSheet()->SetCellValue("D".$number_column,$row_data_pemilik_usaha->nik);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                        "D".$number_column,
                        $row_data_pemilik_usaha->nik,
                        PHPExcel_Cell_DataType::TYPE_STRING
                    );
                    $objPHPExcel->getActiveSheet()->getStyle("D".$number_column)->applyFromArray($style_data_kiri);

                    $objPHPExcel->getActiveSheet()->SetCellValue("E".$number_column,$row_data_pemilik_usaha->no_izin_usaha);
                    $objPHPExcel->getActiveSheet()->getStyle("E".$number_column)->applyFromArray($style_data);

                    $no_data++;
                    $number_column++;
                    
                    $letter_colum_aset_omset = "F";
                    for($i=$year_start;$i<=$year_end;$i++){
                        $sum_aset_omset = $this->aset_omset_usaha_model->get(
                            array(
                                "fields"=>"aset_omset_usaha.*",
                                "join"=>array(
                                    "master_data_usaha"=>"id_data_usaha=master_data_usaha_id AND master_data_usaha.deleted_at IS NULL AND master_data_usaha.is_verified = 1",
                                    "master_pemilik_usaha"=>"id_pemilik_usaha=master_pemilik_usaha_id AND master_pemilik_usaha.deleted_at IS NULL AND master_pemilik_usaha.is_verified = 1",
                                    "master_jenis_usaha"=>"id_jenis_usaha=master_jenis_usaha_id",
                                    "master_wilayah"=>"id_master_wilayah=master_data_usaha.kelurahan_master_wilayah_id AND kode_induk=".$row->kode_wilayah
                                ),
                                "where"=> array(
                                    "aset_omset_usaha.is_verified"=>"1",
                                    "master_jenis_usaha_id"=>$row_jenis_usaha->id_jenis_usaha,
                                    "id_pemilik_usaha"=>$row_data_pemilik_usaha->id_pemilik_usaha,
                                    "tahun_berkenaan"=>$i
                                )
                            ),"row"
                        );

                        $objPHPExcel->getActiveSheet()->SetCellValue($letter_colum_aset_omset.$number_colum_aset_omset,nominal(($sum_aset_omset)?$sum_aset_omset->jumlah_aset:"0"));
                        $objPHPExcel->getActiveSheet()->getStyle($letter_colum_aset_omset.$number_colum_aset_omset)->applyFromArray($style_data_kanan);
                        // $jumlah_aset_pertahun_0 += ($sum_aset_omset)?$sum_aset_omset->jumlah_aset:0;
                        // $jumlah_aset_pertahun_kecamatan_0 += ($sum_aset_omset)?$sum_aset_omset->jumlah_aset:0;
                        $letter_colum_aset_omset++;
                        

                        $objPHPExcel->getActiveSheet()->SetCellValue($letter_colum_aset_omset.$number_colum_aset_omset,nominal(($sum_aset_omset)?$sum_aset_omset->jumlah_omset_per_tahun:"0"));
                        $objPHPExcel->getActiveSheet()->getStyle($letter_colum_aset_omset.$number_colum_aset_omset)->applyFromArray($style_data_kanan);
                        // $jumlah_omset_pertahun_0 += ($sum_aset_omset)?$sum_aset_omset->jumlah_omset_per_tahun:0;
                        // $jumlah_omset_pertahun_kecamatan_0 += ($sum_aset_omset)?$sum_aset_omset->jumlah_omset_per_tahun:0;
    
                        // $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."5","Tahun ".$i);
                        // $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."6","Nilai Aset/Tahun (Rp.)");
                        // $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."7",$no_header_cell);
                        $letter_colum_aset_omset++;
                    }
                }
                $number_colum_aset_omset++;
            }          

            $objPHPExcel->getActiveSheet()->mergeCells("A".$number_column.":E".$number_column);
            $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,"Jumlah");
            $objPHPExcel->getActiveSheet()->getStyle("A".$number_column.":E".$number_column)->applyFromArray($style_data_footer);
            
            $letter_colum_aset_omset = "F";
            
            for($i=$year_start;$i<=$year_end;$i++){
                $jumlah_aset_pertahun = 0;
                $jumlah_omset_pertahun = 0;
                
                foreach($data_jenis_usaha as $key_jenis_usaha=>$row_jenis_usaha){
                    $sum_aset_omset = $this->jenis_usaha_model->query("
                    SELECT SUM(jumlah_aset) AS jumlah_aset, SUM(jumlah_omset_per_tahun) AS jumlah_omset_per_tahun
                    FROM master_data_usaha
                    INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id AND master_wilayah.`deleted_at` IS NULL AND kode_induk = ".$row->kode_wilayah."
                    INNER JOIN aset_omset_usaha ON id_data_usaha=master_data_usaha_id AND aset_omset_usaha.`deleted_at` IS NULL AND aset_omset_usaha.`is_verified` = '1'
                    WHERE master_jenis_usaha_id=".$row_jenis_usaha->id_jenis_usaha." AND tahun_berkenaan = ".$i." AND master_data_usaha.`deleted_at` IS NULL AND master_data_usaha.`is_verified` = '1'
                    GROUP BY master_jenis_usaha_id
                    ")->row();

                    $jumlah_aset_pertahun += ($sum_aset_omset)?$sum_aset_omset->jumlah_aset:0;
                    
                    $jumlah_omset_pertahun += ($sum_aset_omset)?$sum_aset_omset->jumlah_omset_per_tahun:0;
                }
                $objPHPExcel->getActiveSheet()->SetCellValue($letter_colum_aset_omset.$number_column,nominal($jumlah_aset_pertahun));
                $objPHPExcel->getActiveSheet()->getStyle($letter_colum_aset_omset.$number_column)->applyFromArray($style_data_footer);
                $letter_colum_aset_omset++;
                $objPHPExcel->getActiveSheet()->SetCellValue($letter_colum_aset_omset.$number_column,nominal($jumlah_omset_pertahun));
                $objPHPExcel->getActiveSheet()->getStyle($letter_colum_aset_omset.$number_column)->applyFromArray($style_data_footer);
                $letter_colum_aset_omset++;
            }
            
            // $objPHPExcel->getActiveSheet()->SetCellValue("C".$number_column,"Rp. ".nominal($jumlah_aset_pertahun_kecamatan_0));
            // $objPHPExcel->getActiveSheet()->getStyle("C".$number_column)->applyFromArray($style_data_footer);
            
            // $objPHPExcel->getActiveSheet()->SetCellValue("D".$number_column,"Rp. ".nominal($jumlah_omset_pertahun_kecamatan_0));
            // $objPHPExcel->getActiveSheet()->getStyle("D".$number_column)->applyFromArray($style_data_footer);
            
            // $objPHPExcel->getActiveSheet()->SetCellValue("E".$number_column,"Rp. ".nominal($jumlah_aset_pertahun_kecamatan_1));
            // $objPHPExcel->getActiveSheet()->getStyle("E".$number_column)->applyFromArray($style_data_footer);
            
            // $objPHPExcel->getActiveSheet()->SetCellValue("F".$number_column,"Rp. ".nominal($jumlah_omset_pertahun_kecamatan_1));
            // $objPHPExcel->getActiveSheet()->getStyle("F".$number_column)->applyFromArray($style_data_footer);

            // $objPHPExcel->getActiveSheet()->SetCellValue("G".$number_column,"Rp. ".nominal($jumlah_aset_pertahun_kecamatan_2));
            // $objPHPExcel->getActiveSheet()->getStyle("G".$number_column)->applyFromArray($style_data_footer);
            
            // $objPHPExcel->getActiveSheet()->SetCellValue("H".$number_column,"Rp. ".nominal($jumlah_omset_pertahun_kecamatan_2));
            // $objPHPExcel->getActiveSheet()->getStyle("H".$number_column)->applyFromArray($style_data_footer);

            // $objPHPExcel->getActiveSheet()->SetCellValue("I".$number_column,"Rp. ".nominal($jumlah_aset_pertahun_kecamatan_3));
            // $objPHPExcel->getActiveSheet()->getStyle("I".$number_column)->applyFromArray($style_data_footer);
            
            // $objPHPExcel->getActiveSheet()->SetCellValue("J".$number_column,"Rp. ".nominal($jumlah_omset_pertahun_kecamatan_3));
            // $objPHPExcel->getActiveSheet()->getStyle("J".$number_column)->applyFromArray($style_data_footer);

            // $objPHPExcel->getActiveSheet()->SetCellValue("K".$number_column,"Rp. ".nominal($jumlah_aset_pertahun_kecamatan_4));
            // $objPHPExcel->getActiveSheet()->getStyle("K".$number_column)->applyFromArray($style_data_footer);
            
            // $objPHPExcel->getActiveSheet()->SetCellValue("L".$number_column,"Rp. ".nominal($jumlah_omset_pertahun_kecamatan_4));
            // $objPHPExcel->getActiveSheet()->getStyle("L".$number_column)->applyFromArray($style_data_footer);
            $number_column++;
            $number_colum_aset_omset++;      
        }
        
        $objPHPExcel->getActiveSheet()->mergeCells("A".$number_column.":E".$number_column);
        $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,"TOTAL");
        $objPHPExcel->getActiveSheet()->getStyle("A".$number_column.":E".$number_column)->applyFromArray($style_data_footer);

        
        $letter_colum_aset_omset = "F";
        for($i=$year_start;$i<=$year_end;$i++){
            $jumlah_aset_pertahun_kecamatan = 0;
            $jumlah_omset_pertahun_kecamatan = 0;
            foreach($data_master_wilayah_kecamatan as $key=>$row){
                $wh_str = "";
                if($jenis_usaha != "all"){
                    $wh_str = "AND id_jenis_usaha IN (".$jenis_usaha.")";
                }
                
                $data_jenis_usaha = $this->jenis_usaha_model->get(
                    array(
                        "fields"=>"master_jenis_usaha.*",
                        "join"=>array(
                            "master_data_usaha"=>"master_jenis_usaha_id=id_jenis_usaha AND master_data_usaha.deleted_at IS NULL AND master_data_usaha.is_verified = 1",
                            "master_wilayah"=>"id_master_wilayah=kelurahan_master_wilayah_id AND kode_induk=".$row->kode_wilayah,
                            "aset_omset_usaha"=>"master_data_usaha_id=id_data_usaha AND aset_omset_usaha.deleted_at IS NULL and aset_omset_usaha.is_verified=1"
                        ),
                        "where_false"=>"tahun_berkenaan BETWEEN ".$year_start." AND ".$year_end." ".$wh_str,
                        "order_by"=>array(
                            "nama_jenis_usaha"=>"ASC"
                        ),
                        "group_by"=>"id_jenis_usaha"
                    )
                );
                
                foreach($data_jenis_usaha as $key_jenis_usaha=>$row_jenis_usaha){
                    $sum_aset_omset = $this->jenis_usaha_model->query("
                    SELECT SUM(jumlah_aset) AS jumlah_aset, SUM(jumlah_omset_per_tahun) AS jumlah_omset_per_tahun
                    FROM master_data_usaha
                    INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id AND master_wilayah.`deleted_at` IS NULL AND kode_induk = ".$row->kode_wilayah."
                    INNER JOIN aset_omset_usaha ON id_data_usaha=master_data_usaha_id AND aset_omset_usaha.`deleted_at` IS NULL AND aset_omset_usaha.`is_verified` = '1'
                    WHERE master_jenis_usaha_id=".$row_jenis_usaha->id_jenis_usaha." AND tahun_berkenaan = ".$i." AND master_data_usaha.`deleted_at` IS NULL AND master_data_usaha.`is_verified` = '1'
                    GROUP BY master_jenis_usaha_id
                    ")->row();
    
                    $jumlah_aset_pertahun_kecamatan += ($sum_aset_omset)?$sum_aset_omset->jumlah_aset:0;
                    
                    $jumlah_omset_pertahun_kecamatan += ($sum_aset_omset)?$sum_aset_omset->jumlah_omset_per_tahun:0;
                }
            }
            $objPHPExcel->getActiveSheet()->SetCellValue($letter_colum_aset_omset.$number_column,nominal($jumlah_aset_pertahun_kecamatan));
            $objPHPExcel->getActiveSheet()->getStyle($letter_colum_aset_omset.$number_column)->applyFromArray($style_data_footer);
            $letter_colum_aset_omset++;
            $objPHPExcel->getActiveSheet()->SetCellValue($letter_colum_aset_omset.$number_column,nominal($jumlah_omset_pertahun_kecamatan));
            $objPHPExcel->getActiveSheet()->getStyle($letter_colum_aset_omset.$number_column)->applyFromArray($style_data_footer);
            $letter_colum_aset_omset++;
        }

        // $objPHPExcel->getActiveSheet()->SetCellValue("C".$number_column,"Rp. ".nominal($jumlah_aset_pertahun_0));
        // $objPHPExcel->getActiveSheet()->getStyle("C".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("D".$number_column,"Rp. ".nominal($jumlah_omset_pertahun_0));
        // $objPHPExcel->getActiveSheet()->getStyle("D".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("E".$number_column,"Rp. ".nominal($jumlah_aset_pertahun_1));
        // $objPHPExcel->getActiveSheet()->getStyle("E".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("F".$number_column,"Rp. ".nominal($jumlah_omset_pertahun_1));
        // $objPHPExcel->getActiveSheet()->getStyle("F".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("G".$number_column,"Rp. ".nominal($jumlah_aset_pertahun_2));
        // $objPHPExcel->getActiveSheet()->getStyle("G".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("H".$number_column,"Rp. ".nominal($jumlah_omset_pertahun_2));
        // $objPHPExcel->getActiveSheet()->getStyle("H".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("I".$number_column,"Rp. ".nominal($jumlah_aset_pertahun_3));
        // $objPHPExcel->getActiveSheet()->getStyle("I".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("J".$number_column,"Rp. ".nominal($jumlah_omset_pertahun_3));
        // $objPHPExcel->getActiveSheet()->getStyle("J".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("K".$number_column,"Rp. ".nominal($jumlah_aset_pertahun_4));
        // $objPHPExcel->getActiveSheet()->getStyle("K".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("L".$number_column,"Rp. ".nominal($jumlah_omset_pertahun_4));
        // $objPHPExcel->getActiveSheet()->getStyle("L".$number_column)->applyFromArray($style_data_footer);

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
		$jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

		$col = 'A';
		while(true){
			$tempCol = $col++;
			$objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
			if($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()){
				break;
			}
		}

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
        
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
		
		$writer->save('php://output');
		exit;
    }

    public function cetak_laporan_4($year_start,$year_end){
        $jenis_usaha = $this->session->userdata("jenis_usaha");

        require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
		require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

        $filename = "Laporan UMKM (Data Rekap Perkembangan Aset & Omset UMKM).xlsx";

        $style_header_table = array(
            'font' => array(
				'size' => 11,
				'bold' => true
			),'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $style_data = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $style_data_footer = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
            ),
            'font' => array(
				'size' => 11,
				'bold' => true
			)
        );

        $style_header = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
			'font' => array(
				'size' => 11,
				'bold' => true
			),'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'dae1f3')
            )
        );

        $style_header_child = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
			'font' => array(
				'size' => 9,
			),'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'e7e5e6')
            )
        );

        $style_header_kecamatan = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
			'font' => array(
				'size' => 11,
				'bold' => true
			),
        );

        $style_data_kiri = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT
            )
        );
        
        $style_data_tengah = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        
        $style_data_kanan = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
            )
		);

        $objPHPExcel = new PHPExcel();
        
        $objPHPExcel->getActiveSheet()->SetCellValue("A1","DATA USAHA MIKRO, KECIL DAN MENENGAH");
        $objPHPExcel->getActiveSheet()->SetCellValue("A2","KABUPATEN KOTAWARINGIN BARAT TAHUN ".$year_start." - ".$year_end);

        $letter_master_header = "B";
        for($i=$year_start;$i<=$year_end;$i++){
            $letter_master_header++;
            $letter_master_header++;
        }

        $objPHPExcel->getActiveSheet()->mergeCells("A1:".$letter_master_header."1");
        $objPHPExcel->getActiveSheet()->mergeCells("A2:".$letter_master_header."2");

        $objPHPExcel->getActiveSheet()->getStyle("A1:".$letter_master_header."2")->applyFromArray($style_header_table);

        $objPHPExcel->getActiveSheet()->mergeCells("A4:A5");
        $objPHPExcel->getActiveSheet()->mergeCells("B4:B5");

        $objPHPExcel->getActiveSheet()->getStyle("A4:".$letter_master_header."4")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("A5:".$letter_master_header."5")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("A6:".$letter_master_header."6")->applyFromArray($style_header_child);
        
		$objPHPExcel->getActiveSheet()->SetCellValue("A4","NO");
        $objPHPExcel->getActiveSheet()->SetCellValue("B4","Jenis Usaha");
        $objPHPExcel->getActiveSheet()->SetCellValue("A6","1");
        $objPHPExcel->getActiveSheet()->SetCellValue("B6","2");
        
        $letter_header_year = "C";
        $letter_header_column_start = "C";
        $no_header_cell = 3;
        for($i=$year_start;$i<=$year_end;$i++){
            $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."4","Tahun ".$i);
            $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."5","Nilai Aset/Tahun (Rp.)");
            $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."6",$no_header_cell);
            $letter_header_year++;
            $no_header_cell++;
            $objPHPExcel->getActiveSheet()->mergeCells($letter_header_column_start."4:".$letter_header_year."4");
            $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."5","Nilai Omset/Tahun (Rp.)");
            $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."6",$no_header_cell);
            $letter_header_year++;
            $no_header_cell++;
            $letter_header_column_start = $letter_header_year;
        }   

        $number_column = 7;
        $no_data = 1;
        $number_colum_aset_omset = 7;

        $wh_str = "";
        if($jenis_usaha != "all"){
            $wh_str = "AND id_jenis_usaha IN (".$jenis_usaha.")";
        }
        
        $data_jenis_usaha = $this->jenis_usaha_model->get(
            array(
                "fields"=>"master_jenis_usaha.*",
                "join"=>array(
                    "master_data_usaha"=>"master_jenis_usaha_id=id_jenis_usaha AND master_data_usaha.deleted_at IS NULL AND master_data_usaha.is_verified = 1",
                    "aset_omset_usaha"=>"master_data_usaha_id=id_data_usaha AND aset_omset_usaha.deleted_at IS NULL and aset_omset_usaha.is_verified=1"
                ),
                "where_false"=>"tahun_berkenaan BETWEEN ".$year_start." AND ".$year_end." ".$wh_str,
                "order_by"=>array(
                    "nama_jenis_usaha"=>"ASC"
                ),
                "group_by"=>"id_jenis_usaha"
            )
        );
        

        foreach($data_jenis_usaha as $key_jenis_usaha=>$row_jenis_usaha){
            $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,$no_data);
            $objPHPExcel->getActiveSheet()->getStyle("A".$number_column)->applyFromArray($style_data_tengah);
            
            $objPHPExcel->getActiveSheet()->SetCellValue("B".$number_column,$row_jenis_usaha->nama_jenis_usaha);
            $objPHPExcel->getActiveSheet()->getStyle("B".$number_column)->applyFromArray($style_data);

            $letter_colum_aset_omset = "C";
            for($i=$year_start;$i<=$year_end;$i++){
                $sum_aset_omset = $this->jenis_usaha_model->query("
                SELECT SUM(jumlah_aset) AS jumlah_aset, SUM(jumlah_omset_per_tahun) AS jumlah_omset_per_tahun
                FROM master_data_usaha
                INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id AND master_wilayah.`deleted_at` IS NULL
                INNER JOIN aset_omset_usaha ON id_data_usaha=master_data_usaha_id AND aset_omset_usaha.`deleted_at` IS NULL AND aset_omset_usaha.`is_verified` = '1'
                WHERE master_jenis_usaha_id=".$row_jenis_usaha->id_jenis_usaha." AND tahun_berkenaan = ".$i." AND master_data_usaha.`deleted_at` IS NULL AND master_data_usaha.`is_verified` = '1'
                GROUP BY master_jenis_usaha_id
                ")->row();
                
                $objPHPExcel->getActiveSheet()->SetCellValue($letter_colum_aset_omset.$number_colum_aset_omset,nominal(($sum_aset_omset)?$sum_aset_omset->jumlah_aset:"0"));
                $objPHPExcel->getActiveSheet()->getStyle($letter_colum_aset_omset.$number_colum_aset_omset)->applyFromArray($style_data_kanan);
                // $jumlah_aset_pertahun_0 += ($sum_aset_omset)?$sum_aset_omset->jumlah_aset:0;
                // $jumlah_aset_pertahun_kecamatan_0 += ($sum_aset_omset)?$sum_aset_omset->jumlah_aset:0;
                $letter_colum_aset_omset++;

                $objPHPExcel->getActiveSheet()->SetCellValue($letter_colum_aset_omset.$number_colum_aset_omset,nominal(($sum_aset_omset)?$sum_aset_omset->jumlah_omset_per_tahun:"0"));
                $objPHPExcel->getActiveSheet()->getStyle($letter_colum_aset_omset.$number_colum_aset_omset)->applyFromArray($style_data_kanan);
                // $jumlah_omset_pertahun_0 += ($sum_aset_omset)?$sum_aset_omset->jumlah_omset_per_tahun:0;
                // $jumlah_omset_pertahun_kecamatan_0 += ($sum_aset_omset)?$sum_aset_omset->jumlah_omset_per_tahun:0;

                // $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."5","Tahun ".$i);
                // $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."6","Nilai Aset/Tahun (Rp.)");
                // $objPHPExcel->getActiveSheet()->SetCellValue($letter_header_year."7",$no_header_cell);
                $letter_colum_aset_omset++;
            }
            
            
            $no_data++;
            $number_column++;            
            $number_colum_aset_omset++;
        }          

        // $objPHPExcel->getActiveSheet()->mergeCells("A".$number_column.":B".$number_column);
        // $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,"Jumlah");
        // $objPHPExcel->getActiveSheet()->getStyle("A".$number_column.":B".$number_column)->applyFromArray($style_data_footer);
        
        // $letter_colum_aset_omset = "C";
        // for($i=$year_start;$i<=$year_end;$i++){
        //     $jumlah_aset_pertahun = 0;
        //     $jumlah_omset_pertahun = 0;
        //     foreach($data_jenis_usaha as $key_jenis_usaha=>$row_jenis_usaha){
        //         $sum_aset_omset = $this->jenis_usaha_model->query("
        //         SELECT SUM(jumlah_aset) AS jumlah_aset, SUM(jumlah_omset_per_tahun) AS jumlah_omset_per_tahun
        //         FROM master_data_usaha
        //         INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id AND master_wilayah.`deleted_at` IS NULL
        //         INNER JOIN aset_omset_usaha ON id_data_usaha=master_data_usaha_id AND aset_omset_usaha.`deleted_at` IS NULL AND aset_omset_usaha.`is_verified` = '1'
        //         WHERE master_jenis_usaha_id=".$row_jenis_usaha->id_jenis_usaha." AND tahun_berkenaan = ".$i." AND master_data_usaha.`deleted_at` IS NULL AND master_data_usaha.`is_verified` = '1'
        //         GROUP BY master_jenis_usaha_id
        //         ")->row();

        //         $jumlah_aset_pertahun += ($sum_aset_omset)?$sum_aset_omset->jumlah_aset:0;
                
        //         $jumlah_omset_pertahun += ($sum_aset_omset)?$sum_aset_omset->jumlah_omset_per_tahun:0;
        //     }
        //     $objPHPExcel->getActiveSheet()->SetCellValue($letter_colum_aset_omset.$number_column,nominal($jumlah_aset_pertahun));
        //     $objPHPExcel->getActiveSheet()->getStyle($letter_colum_aset_omset.$number_column)->applyFromArray($style_data_footer);
        //     $letter_colum_aset_omset++;
        //     $objPHPExcel->getActiveSheet()->SetCellValue($letter_colum_aset_omset.$number_column,nominal($jumlah_omset_pertahun));
        //     $objPHPExcel->getActiveSheet()->getStyle($letter_colum_aset_omset.$number_column)->applyFromArray($style_data_footer);
        //     $letter_colum_aset_omset++;
        // }

        $objPHPExcel->getActiveSheet()->mergeCells("A".$number_column.":B".$number_column);
        $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,"Jumlah");
        $objPHPExcel->getActiveSheet()->getStyle("A".$number_column.":B".$number_column)->applyFromArray($style_data_footer);

        

        $wh_str = "";
        if($jenis_usaha != "all"){
            $wh_str = "AND id_jenis_usaha IN (".$jenis_usaha.")";
        }
        
        $data_jenis_usaha = $this->jenis_usaha_model->get(
            array(
                "fields"=>"master_jenis_usaha.*",
                "join"=>array(
                    "master_data_usaha"=>"master_jenis_usaha_id=id_jenis_usaha AND master_data_usaha.deleted_at IS NULL AND master_data_usaha.is_verified = 1",
                    "aset_omset_usaha"=>"master_data_usaha_id=id_data_usaha AND aset_omset_usaha.deleted_at IS NULL and aset_omset_usaha.is_verified=1"
                ),
                "where_false"=>"tahun_berkenaan BETWEEN ".$year_start." AND ".$year_end." ".$wh_str,
                "order_by"=>array(
                    "nama_jenis_usaha"=>"ASC"
                ),
                "group_by"=>"id_jenis_usaha"
            )
        );
        
        $letter_colum_aset_omset = "C";
        for($i=$year_start;$i<=$year_end;$i++){
            $jumlah_aset_pertahun_kecamatan = 0;
            $jumlah_omset_pertahun_kecamatan = 0;
            foreach($data_jenis_usaha as $key_jenis_usaha=>$row_jenis_usaha){
                $sum_aset_omset = $this->jenis_usaha_model->query("
                SELECT SUM(jumlah_aset) AS jumlah_aset, SUM(jumlah_omset_per_tahun) AS jumlah_omset_per_tahun
                FROM master_data_usaha
                INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id AND master_wilayah.`deleted_at` IS NULL
                INNER JOIN aset_omset_usaha ON id_data_usaha=master_data_usaha_id AND aset_omset_usaha.`deleted_at` IS NULL AND aset_omset_usaha.`is_verified` = '1'
                WHERE master_jenis_usaha_id=".$row_jenis_usaha->id_jenis_usaha." AND tahun_berkenaan = ".$i." AND master_data_usaha.`deleted_at` IS NULL AND master_data_usaha.`is_verified` = '1'
                GROUP BY master_jenis_usaha_id
                ")->row();

                $jumlah_aset_pertahun_kecamatan += ($sum_aset_omset)?$sum_aset_omset->jumlah_aset:0;
                
                $jumlah_omset_pertahun_kecamatan += ($sum_aset_omset)?$sum_aset_omset->jumlah_omset_per_tahun:0;
            }
            $objPHPExcel->getActiveSheet()->SetCellValue($letter_colum_aset_omset.$number_column,nominal($jumlah_aset_pertahun_kecamatan));
            $objPHPExcel->getActiveSheet()->getStyle($letter_colum_aset_omset.$number_column)->applyFromArray($style_data_footer);
            $letter_colum_aset_omset++;
            $objPHPExcel->getActiveSheet()->SetCellValue($letter_colum_aset_omset.$number_column,nominal($jumlah_omset_pertahun_kecamatan));
            $objPHPExcel->getActiveSheet()->getStyle($letter_colum_aset_omset.$number_column)->applyFromArray($style_data_footer);
            $letter_colum_aset_omset++;
        }

        // $objPHPExcel->getActiveSheet()->SetCellValue("C".$number_column,"Rp. ".nominal($jumlah_aset_pertahun_0));
        // $objPHPExcel->getActiveSheet()->getStyle("C".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("D".$number_column,"Rp. ".nominal($jumlah_omset_pertahun_0));
        // $objPHPExcel->getActiveSheet()->getStyle("D".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("E".$number_column,"Rp. ".nominal($jumlah_aset_pertahun_1));
        // $objPHPExcel->getActiveSheet()->getStyle("E".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("F".$number_column,"Rp. ".nominal($jumlah_omset_pertahun_1));
        // $objPHPExcel->getActiveSheet()->getStyle("F".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("G".$number_column,"Rp. ".nominal($jumlah_aset_pertahun_2));
        // $objPHPExcel->getActiveSheet()->getStyle("G".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("H".$number_column,"Rp. ".nominal($jumlah_omset_pertahun_2));
        // $objPHPExcel->getActiveSheet()->getStyle("H".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("I".$number_column,"Rp. ".nominal($jumlah_aset_pertahun_3));
        // $objPHPExcel->getActiveSheet()->getStyle("I".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("J".$number_column,"Rp. ".nominal($jumlah_omset_pertahun_3));
        // $objPHPExcel->getActiveSheet()->getStyle("J".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("K".$number_column,"Rp. ".nominal($jumlah_aset_pertahun_4));
        // $objPHPExcel->getActiveSheet()->getStyle("K".$number_column)->applyFromArray($style_data_footer);

        // $objPHPExcel->getActiveSheet()->SetCellValue("L".$number_column,"Rp. ".nominal($jumlah_omset_pertahun_4));
        // $objPHPExcel->getActiveSheet()->getStyle("L".$number_column)->applyFromArray($style_data_footer);

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
		$jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

		$col = 'A';
		while(true){
			$tempCol = $col++;
			$objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
			if($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()){
				break;
			}
		}

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(false);

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('15');
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(false);

        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(false);
        
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
		
		$writer->save('php://output');
		exit;
    }
}
