<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {
	function __construct(){
		parent::__construct();
        $this->load->model('aset_omset_usaha/aset_omset_usaha_model','aset_omset_usaha_model');
	}

	public function get_laporan()
	{
        $jenis_usaha = $this->input->get("jenis_usaha");
        
        $year_start = $this->iget("year_start");
        $year_end = $this->iget("year_end");
        $tahun = $this->iget("tahun");

        $kecamatan = decrypt_data($this->iget("kecamatan"));

        if(($year_start && $year_end) || $tahun || $kecamatan){
            $wh_str = "";
            if(!empty($jenis_usaha)){
                if(in_array("all",$jenis_usaha)){
                    $this->session->set_userdata("jenis_usaha","all");
                }else{
                    $arr_jenis_usaha = array();
                    for($i = 0;$i<count($jenis_usaha);$i++){
                        array_push($arr_jenis_usaha,decrypt_data($jenis_usaha[$i]));
                    }
                    
                    $wh_str = "AND master_jenis_usaha_id IN (".implode(',',$arr_jenis_usaha).")";
                    $this->session->set_userdata("jenis_usaha",implode(',',$arr_jenis_usaha));
                }
            }else{
                $this->session->set_userdata("jenis_usaha","all");
            }

            $wh_false = "";
            if($tahun){
                $wh_false = "tahun_berkenaan = ".$tahun;
            }else{
                $wh_false = "tahun_berkenaan BETWEEN ".$year_start." AND ".$year_end;
            }

            $wh_join = "";

            if($kecamatan){
                $wh_join = "AND kode_induk=".$kecamatan;
            }

            $data_usaha = $this->aset_omset_usaha_model->get(
                array(
                    "fields"=>"aset_omset_usaha.*,nama_pemilik_usaha,nama_toko_perusahaan,nama_jenis_usaha",
                    "join"=>array(
                        "master_data_usaha"=>"id_data_usaha=master_data_usaha_id AND master_data_usaha.deleted_at IS NULL AND master_data_usaha.is_verified = 1",
                        "master_pemilik_usaha"=>"id_pemilik_usaha=master_pemilik_usaha_id AND master_pemilik_usaha.deleted_at IS NULL AND master_pemilik_usaha.is_verified = 1",
                        "master_jenis_usaha"=>"id_jenis_usaha=master_jenis_usaha_id",
                        "master_wilayah"=>"master_data_usaha.kelurahan_master_wilayah_id=id_master_wilayah AND master_wilayah.deleted_at IS NULL ".$wh_join
                    ),
                    "where"=> array(
                        "aset_omset_usaha.is_verified"=>"1"
                    ),
                    // "where_false"=>"tahun_berkenaan BETWEEN ".$year_start." AND ".$year_end." AND master_jenis_usaha_id IN (".implode(',',$arr_jenis_usaha).")"
                    "where_false"=>$wh_false." ".$wh_str
                )
            );
    
            $templist = array();
            foreach($data_usaha as $key=>$row){
                foreach($row as $keys=>$rows){
                    $templist[$key][$keys] = $rows;
                }
                $templist[$key]['id_encrypt'] = encrypt_data($row->id_aset_omset_usaha);
            }
    
            $data = $templist;
        }else{
            $data = array();
        }
            
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
