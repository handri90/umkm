<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tahun : </label>
                        <select class="form-control select-search" name="tahun" onChange="get_laporan()">
                            <option value="">-- Pilih Tahun --</option>
                            <?php
                            foreach($master_tahun as $key=>$row){
                                ?>
                                <option value="<?php echo $row->tahun; ?>"><?php echo $row->tahun; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Basic datatable -->
    <div class="card">
        <div class="card-body">
            <div class="box-top text-right">
                <a href="#cetakLaporan" id="cetak_laporan" class="btn btn-info">Cetak Laporan</a>
            </div>
        </div>
        <table id="datatableLaporan" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>Pemilik Usaha</th>
                    <th>Toko / Perusahaan</th>
                    <th>Jenis Usaha</th>
                    <th>Jumlah Aset</th>
                    <th>Jumlah Omset</th>
                    <th>Modal Sendiri</th>
                    <th>Modal Luar</th>
                    <th>Pinjaman Luar</th>
                    <th>Tahun</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->
</div>

<script>

$(".box-top").hide();

const IDR = value => currency(
    value, { 
        symbol: "Rp. ",
        precision: 0,
        separator: "." 
});

let datatableLaporan = $("#datatableLaporan").DataTable({
    "columns": [
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    ]
});
get_laporan();
function get_laporan(){
    let tahun = $("select[name=tahun]").val();

    datatableLaporan.clear().draw();
    if(tahun){
        $(".box-top").show();

        $("#cetak_laporan").attr("href",base_url+"laporan_per_tahun/cetak_laporan/"+tahun);

        $.ajax({
            url: base_url+'laporan_per_tahun/request/get_laporan',
            data:{tahun:tahun},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                $.each(response,function(index,value){
                    datatableLaporan.row.add([
                        value.nama_pemilik_usaha,
                        value.nama_toko_perusahaan,
                        value.nama_jenis_usaha,
                        IDR(value.jumlah_aset).format(true),
                        IDR(value.jumlah_omset_per_tahun).format(true),
                        IDR(value.modal_sendiri).format(true),
                        IDR(value.modal_luar).format(true),
                        IDR(value.pinjaman_luar).format(true),
                        value.tahun_n
                    ]).draw(false);
                });
            },
            complete:function(response){
                loading_stop();
            }
        });
    }
}
</script>