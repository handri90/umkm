<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {
	function __construct(){
		parent::__construct();
        $this->load->model('aset_omset_usaha/aset_omset_usaha_model','aset_omset_usaha_model');
	}

	public function get_laporan()
	{
        $tahun = $this->iget("tahun");

        $data_usaha = $this->aset_omset_usaha_model->get(
            array(
                "fields"=>"aset_omset_usaha.*,nama_pemilik_usaha,nama_toko_perusahaan,nama_jenis_usaha",
                "join"=>array(
                    "master_data_usaha"=>"id_data_usaha=master_data_usaha_id AND master_data_usaha.deleted_at IS NULL AND master_data_usaha.is_verified = 1",
                    "master_pemilik_usaha"=>"id_pemilik_usaha=master_pemilik_usaha_id AND master_pemilik_usaha.deleted_at IS NULL AND master_pemilik_usaha.is_verified = 1",
                    "master_jenis_usaha"=>"id_jenis_usaha=master_jenis_usaha_id AND master_jenis_usaha.deleted_at IS NULL"
                ),
                "where"=> array(
                    "DATE_FORMAT(aset_omset_usaha.created_at,'%Y')"=>$tahun,
                    "aset_omset_usaha.is_verified"=>"1"
                )
            )
        );

        $templist = array();
        foreach($data_usaha as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_aset_omset_usaha);
            $created_at = strtotime($row->created_at);
            $templist[$key]['tahun_n'] = date("Y",$created_at);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
}
