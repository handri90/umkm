<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_per_tahun extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('aset_omset_usaha/aset_omset_usaha_model','aset_omset_usaha_model');
		$this->load->model('data_usaha/master_wilayah_model','master_wilayah_model');
	}

	public function index()
	{
        $data['master_tahun'] = $this->aset_omset_usaha_model->get(
            array(
                "fields"=>"DATE_FORMAT(created_at,'%Y') AS tahun",
                "order_by"=>array(
                    "nama_jenis_usaha"=>"ASC"
                ),
                "group_by"=>"tahun",
                "order_by"=>array(
                    "tahun"=>"ASC"
                ),
                "where"=>array(
                    "is_verified"=>"1"
                )
            )
        );

        $data['breadcrumb'] = [['link'=>false,'content'=>'Laporan','is_active'=>true]];
        $this->execute('index',$data);
    }

    public function cetak_laporan($tahun){
        require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
		require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

        $filename = "Laporan Per Tahun.xlsx";

        $style_header_table = array(
            'font' => array(
				'size' => 11,
				'bold' => true
			),'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
        
        $style_header = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
			'font' => array(
				'size' => 11,
				'bold' => true
			),'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'dae1f3')
            )
        );

        $style_header_child = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
			'font' => array(
				'size' => 9,
			),'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'e7e5e6')
            )
        );
        
        $style_data = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        
        $style_data_kiri = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT
            )
        );
        
        $style_data_tengah = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        
        $style_data_kanan = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment'=> array(
                'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
            )
		);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getActiveSheet()->getStyle("A4:W4")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("A5:W5")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("A6:W6")->applyFromArray($style_header_child);

        $objPHPExcel->getActiveSheet()->mergeCells("A4:A5");
        $objPHPExcel->getActiveSheet()->mergeCells("B4:B5");
        $objPHPExcel->getActiveSheet()->mergeCells("C4:C5");
        $objPHPExcel->getActiveSheet()->mergeCells("D4:D5");
        $objPHPExcel->getActiveSheet()->mergeCells("E4:E5");
        $objPHPExcel->getActiveSheet()->mergeCells("F4:F5");
        $objPHPExcel->getActiveSheet()->mergeCells("G4:G5");
        $objPHPExcel->getActiveSheet()->mergeCells("H4:H5");
        $objPHPExcel->getActiveSheet()->mergeCells("I4:I5");
        $objPHPExcel->getActiveSheet()->mergeCells("J4:J5");
        
        $objPHPExcel->getActiveSheet()->mergeCells("K4:L4");
        
        $objPHPExcel->getActiveSheet()->mergeCells("M4:M5");
        $objPHPExcel->getActiveSheet()->mergeCells("N4:N5");
        $objPHPExcel->getActiveSheet()->mergeCells("O4:O5");
        $objPHPExcel->getActiveSheet()->mergeCells("P4:P5");
        $objPHPExcel->getActiveSheet()->mergeCells("Q4:Q5");
        $objPHPExcel->getActiveSheet()->mergeCells("R4:R5");
        
        $objPHPExcel->getActiveSheet()->mergeCells("S4:T4");
        
        $objPHPExcel->getActiveSheet()->mergeCells("U4:U5");
    
        $objPHPExcel->getActiveSheet()->mergeCells("V4:W4");
        
        $objPHPExcel->getActiveSheet()->SetCellValue("A1","DATA USAHA MIKRO, KECIL DAN MENENGAH");
        $objPHPExcel->getActiveSheet()->SetCellValue("A2","KABUPATEN KOTAWARINGIN BARAT TAHUN ".$tahun);
        $objPHPExcel->getActiveSheet()->mergeCells("A1:W1");
        $objPHPExcel->getActiveSheet()->mergeCells("A2:W2");

        $objPHPExcel->getActiveSheet()->getStyle("A1:W2")->applyFromArray($style_header_table);
        
		$objPHPExcel->getActiveSheet()->SetCellValue("A4","NO");
		$objPHPExcel->getActiveSheet()->SetCellValue("B4","Nama Pemilik Usaha");
		$objPHPExcel->getActiveSheet()->SetCellValue("C4","Nama Toko/Perusahaan");
		$objPHPExcel->getActiveSheet()->SetCellValue("D4","NIK");
		$objPHPExcel->getActiveSheet()->SetCellValue("E4","Nomor KK");
		$objPHPExcel->getActiveSheet()->SetCellValue("F4","Tempat/Tanggal Lahir");
		$objPHPExcel->getActiveSheet()->SetCellValue("G4","Jenis Kelamin");
		$objPHPExcel->getActiveSheet()->SetCellValue("H4","Status Perkawinan");
		$objPHPExcel->getActiveSheet()->SetCellValue("I4","Pendidikan Terakhir");
		$objPHPExcel->getActiveSheet()->SetCellValue("J4","Pekerjaan Inti");
        
        $objPHPExcel->getActiveSheet()->SetCellValue("K4","Alamat");
        $objPHPExcel->getActiveSheet()->SetCellValue("K5","Alamat Pemilik Usaha");
        $objPHPExcel->getActiveSheet()->SetCellValue("L5","Alamat Toko/Perusahaan");
        
        $objPHPExcel->getActiveSheet()->SetCellValue("M4","Nomor Telepon");
        $objPHPExcel->getActiveSheet()->SetCellValue("N4","NPWP");
        $objPHPExcel->getActiveSheet()->SetCellValue("O4","Jenis Usaha");
        $objPHPExcel->getActiveSheet()->SetCellValue("P4","Nomor Izin Usaha");
        $objPHPExcel->getActiveSheet()->SetCellValue("Q4","Jumlah Aset");
        $objPHPExcel->getActiveSheet()->SetCellValue("R4","Jumlah Omset/Tahun");
        
        $objPHPExcel->getActiveSheet()->SetCellValue("S4","Modal Usaha");
        $objPHPExcel->getActiveSheet()->SetCellValue("S5","Modal Sendiri");
        $objPHPExcel->getActiveSheet()->SetCellValue("T5","Modal Luar");
        
        $objPHPExcel->getActiveSheet()->SetCellValue("U4","Jumlah Pekerja");
        
        $objPHPExcel->getActiveSheet()->SetCellValue("V4","Pinjaman Pada Lembaga Keuangan");
        $objPHPExcel->getActiveSheet()->SetCellValue("V5","Nilai Pinjaman");
        $objPHPExcel->getActiveSheet()->SetCellValue("W5","Agunan Yang Digunakan");

		$objPHPExcel->getActiveSheet()->SetCellValue("A6","1");
		$objPHPExcel->getActiveSheet()->SetCellValue("B6","2");
		$objPHPExcel->getActiveSheet()->SetCellValue("C6","3");
		$objPHPExcel->getActiveSheet()->SetCellValue("D6","4");
		$objPHPExcel->getActiveSheet()->SetCellValue("E6","5");
		$objPHPExcel->getActiveSheet()->SetCellValue("F6","6");
		$objPHPExcel->getActiveSheet()->SetCellValue("G6","7");
		$objPHPExcel->getActiveSheet()->SetCellValue("H6","8");
		$objPHPExcel->getActiveSheet()->SetCellValue("I6","9");
		$objPHPExcel->getActiveSheet()->SetCellValue("J6","10");
		$objPHPExcel->getActiveSheet()->SetCellValue("K6","11");
		$objPHPExcel->getActiveSheet()->SetCellValue("L6","12");
		$objPHPExcel->getActiveSheet()->SetCellValue("M6","13");
		$objPHPExcel->getActiveSheet()->SetCellValue("N6","14");
		$objPHPExcel->getActiveSheet()->SetCellValue("O6","15");
		$objPHPExcel->getActiveSheet()->SetCellValue("P6","16");
		$objPHPExcel->getActiveSheet()->SetCellValue("Q6","17");
		$objPHPExcel->getActiveSheet()->SetCellValue("R6","18");
		$objPHPExcel->getActiveSheet()->SetCellValue("S6","19");
		$objPHPExcel->getActiveSheet()->SetCellValue("T6","20");
		$objPHPExcel->getActiveSheet()->SetCellValue("U6","21");
		$objPHPExcel->getActiveSheet()->SetCellValue("V6","22");
		$objPHPExcel->getActiveSheet()->SetCellValue("W6","23");

        $data_usaha = $this->aset_omset_usaha_model->get(
            array(
                "fields"=>"aset_omset_usaha.*,nama_pemilik_usaha,nik,nomor_kk,tempat_lahir,tanggal_lahir,jenis_kelamin,status_perkawinan,pendidikan_terakhir,pekerjaan_inti,master_pemilik_usaha.alamat AS alamat_pemilik,master_pemilik_usaha.rt_rw as rt_rw_pemilik,nomor_telepon,npwp,alamat_usaha,master_data_usaha.rt_rw as rt_rw_usaha,no_izin_usaha,nama_toko_perusahaan,nama_jenis_usaha,jumlah_aset,jumlah_omset_per_tahun,modal_sendiri,modal_luar,jumlah_pekerja,pinjaman_luar,agunan_modal,pendidikan_terakhir_lainnya,pekerjaan_inti_lainnya,master_pemilik_usaha.kelurahan_master_wilayah_id as pemilik_kel_desa, master_data_usaha.kelurahan_master_wilayah_id as usaha_kel_desa",
                "join"=>array(
                    "master_data_usaha"=>"id_data_usaha=master_data_usaha_id AND master_data_usaha.deleted_at IS NULL AND master_data_usaha.is_verified = 1",
                    "master_pemilik_usaha"=>"id_pemilik_usaha=master_pemilik_usaha_id AND master_pemilik_usaha.deleted_at IS NULL AND master_pemilik_usaha.is_verified = 1",
                    "master_jenis_usaha"=>"id_jenis_usaha=master_jenis_usaha_id AND master_jenis_usaha.deleted_at IS NULL"
                ),
                "where"=> array(
                    "DATE_FORMAT(aset_omset_usaha.created_at,'%Y')"=>$tahun,
                    "aset_omset_usaha.is_verified"=>"1"
                ),
                "order_by"=>array(
                    "nama_pemilik_usaha"=>"ASC"
                )
            )
        );

        $number_column = 7;
		$no_data = 1;
        foreach($data_usaha as $key=>$row){
            $created_at = strtotime($row->created_at);
            $tahun_n = date("Y",$created_at);

            $data_master_pemilik = $this->master_wilayah_model->get_by($row->pemilik_kel_desa);

            $data_kecamatan_pemilik = $this->master_wilayah_model->get(
                array(
                    "fields"=>"nama_wilayah,kode_induk",
                    "where"=>array(
                        "kode_wilayah"=>$data_master_pemilik->kode_induk
                    )
                ),"row"
            );
            
            $data_kabupaten_pemilik = $this->master_wilayah_model->get(
                array(
                    "fields"=>"nama_wilayah,kode_induk",
                    "where"=>array(
                        "kode_wilayah"=>$data_kecamatan_pemilik->kode_induk
                    )
                ),"row"
            );

            $data_master_usaha = $this->master_wilayah_model->get_by($row->usaha_kel_desa);

            $data_kecamatan_usaha = $this->master_wilayah_model->get(
                array(
                    "fields"=>"nama_wilayah,kode_induk",
                    "where"=>array(
                        "kode_wilayah"=>$data_master_usaha->kode_induk
                    )
                ),"row"
            );
            
            $data_kabupaten_usaha = $this->master_wilayah_model->get(
                array(
                    "fields"=>"nama_wilayah,kode_induk",
                    "where"=>array(
                        "kode_wilayah"=>$data_kecamatan_usaha->kode_induk
                    )
                ),"row"
            );

            $data_provinsi_usaha = $this->master_wilayah_model->get(
                array(
                    "fields"=>"nama_wilayah,kode_induk",
                    "where"=>array(
                        "kode_wilayah"=>$data_kabupaten_usaha->kode_induk
                    )
                ),"row"
            );

            $objPHPExcel->getActiveSheet()->SetCellValue("A".$number_column,$no_data);
            $objPHPExcel->getActiveSheet()->getStyle("A".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("B".$number_column,$row->nama_pemilik_usaha);
            $objPHPExcel->getActiveSheet()->getStyle("B".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("C".$number_column,$row->nama_toko_perusahaan);
            $objPHPExcel->getActiveSheet()->getStyle("C".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("D".$number_column,$row->nik);
            $objPHPExcel->getActiveSheet()->getStyle("D".$number_column)->applyFromArray($style_data_kiri);

            $objPHPExcel->getActiveSheet()->SetCellValue("E".$number_column,$row->nomor_kk);
            $objPHPExcel->getActiveSheet()->getStyle("E".$number_column)->applyFromArray($style_data_kiri);

            $objPHPExcel->getActiveSheet()->SetCellValue("F".$number_column,$row->tempat_lahir.", ".date("d-m-Y",strtotime($row->tanggal_lahir)));
            $objPHPExcel->getActiveSheet()->getStyle("F".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("G".$number_column,$row->jenis_kelamin);
            $objPHPExcel->getActiveSheet()->getStyle("G".$number_column)->applyFromArray($style_data_tengah);

            $objPHPExcel->getActiveSheet()->SetCellValue("H".$number_column,$row->status_perkawinan);
            $objPHPExcel->getActiveSheet()->getStyle("H".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("I".$number_column,($row->pendidikan_terakhir != "Lainnya"?$row->pendidikan_terakhir:$row->pendidikan_terakhir_lainnya));
            $objPHPExcel->getActiveSheet()->getStyle("I".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("J".$number_column,($row->pekerjaan_inti != "Lainnya"?$row->pekerjaan_inti:$row->pekerjaan_inti_lainnya));
            $objPHPExcel->getActiveSheet()->getStyle("J".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("K".$number_column,$row->alamat_pemilik." RT/RW ".$row->rt_rw_pemilik.", Kecamatan ".ucwords(strtolower($data_kecamatan_pemilik->nama_wilayah)).", Kabupaten ".ucwords(strtolower($data_kabupaten_pemilik->nama_wilayah)));
            $objPHPExcel->getActiveSheet()->getStyle("K".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("L".$number_column,$row->alamat_usaha." RT/RW ".$row->rt_rw_usaha.", Kecamatan ".ucwords(strtolower($data_kecamatan_usaha->nama_wilayah)).", Kabupaten ".ucwords(strtolower($data_kabupaten_usaha->nama_wilayah)));
            $objPHPExcel->getActiveSheet()->getStyle("L".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("M".$number_column,$row->nomor_telepon);
            $objPHPExcel->getActiveSheet()->getStyle("M".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("N".$number_column,$row->npwp);
            $objPHPExcel->getActiveSheet()->getStyle("N".$number_column)->applyFromArray($style_data_kiri);

            $objPHPExcel->getActiveSheet()->SetCellValue("O".$number_column,$row->nama_jenis_usaha);
            $objPHPExcel->getActiveSheet()->getStyle("O".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("P".$number_column,$row->no_izin_usaha);
            $objPHPExcel->getActiveSheet()->getStyle("P".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("Q".$number_column,"Rp. ".nominal($row->jumlah_aset));
            $objPHPExcel->getActiveSheet()->getStyle("Q".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("R".$number_column,"Rp. ".nominal($row->jumlah_omset_per_tahun));
            $objPHPExcel->getActiveSheet()->getStyle("R".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("S".$number_column,"Rp. ".nominal($row->modal_sendiri));
            $objPHPExcel->getActiveSheet()->getStyle("S".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("T".$number_column,"Rp. ".nominal($row->modal_luar));
            $objPHPExcel->getActiveSheet()->getStyle("T".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("U".$number_column,$row->jumlah_pekerja);
            $objPHPExcel->getActiveSheet()->getStyle("U".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("V".$number_column,"Rp. ".nominal($row->pinjaman_luar));
            $objPHPExcel->getActiveSheet()->getStyle("V".$number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("W".$number_column,$row->agunan_modal);
            $objPHPExcel->getActiveSheet()->getStyle("W".$number_column)->applyFromArray($style_data);

            $no_data++;
            $number_column++;
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
		$jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

		$col = 'A';
		while(true){
			$tempCol = $col++;
			$objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
			if($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()){
				break;
			}
		}

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('15');
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth('15');
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(false);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth('15');
		$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(false);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
		
		$writer->save('php://output');
		exit;
    }
}
