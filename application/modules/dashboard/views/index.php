<!-- Content area -->
<div class="content">
    <div class="row">
        <div class="col-sm-6 col-xl-4">
            <div class="card card-body bg-blue-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0 data_pemilik_usaha_masuk"></h3>
                        <span class="text-uppercase font-size-xs">data pemilik usaha masuk</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-user-tie icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-4">
            <div class="card card-body bg-blue-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0 data_pemilik_usaha_belum_terverifikasi"></h3>
                        <span class="text-uppercase font-size-xs">data pemilik usaha belum terverifikasi</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-user-tie icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-4">
            <div class="card card-body bg-blue-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0 data_pemilik_usaha_sudah_terverifikasi"></h3>
                        <span class="text-uppercase font-size-xs">data pemilik usaha sudah terverifikasi</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-user-tie icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-xl-4">
            <div class="card card-body bg-success-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0 data_usaha_masuk"></h3>
                        <span class="text-uppercase font-size-xs">data usaha masuk</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-clipboard3 icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-4">
            <div class="card card-body bg-success-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0 data_usaha_belum_terverifikasi"></h3>
                        <span class="text-uppercase font-size-xs">data usaha belum terverifikasi</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-clipboard3 icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-4">
            <div class="card card-body bg-success-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0 data_usaha_sudah_terverifikasi"></h3>
                        <span class="text-uppercase font-size-xs">data usaha sudah terverifikasi</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-clipboard3 icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-xl-4">
            <div class="card card-body bg-indigo-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0 data_aset_omset_masuk"></h3>
                        <span class="text-uppercase font-size-xs">data aset & omset masuk</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-coin-dollar icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-4">
            <div class="card card-body bg-indigo-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0 data_aset_omset_belum_terverifikasi"></h3>
                        <span class="text-uppercase font-size-xs">data aset & omset belum terverifikasi</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-coin-dollar icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-4">
            <div class="card card-body bg-indigo-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0 data_aset_omset_sudah_terverifikasi"></h3>
                        <span class="text-uppercase font-size-xs">data aset & omset sudah terverifikasi</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-coin-dollar icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-6 col-xl-4">
            <div class="card card-body bg-info-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0 jumlah_unit_usaha"></h3>
                        <span class="text-uppercase font-size-xs">jumlah unit usaha umkm</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-office icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-4">
            <div class="card card-body bg-teal-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0 jumlah_aset_umkm"></h3>
                        <span class="text-uppercase font-size-xs">jumlah aset umkm</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-cash icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-4">
            <div class="card card-body bg-orange-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0 jumlah_omset_umkm"></h3>
                        <span class="text-uppercase font-size-xs">jumlah omset umkm</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-cash3 icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
<!-- /content area -->

<script>
const IDR = value => currency(
    value, { 
        symbol: "Rp. ",
        precision: 0,
        separator: "." 
});

data_pemilik_usaha_masuk();
function data_pemilik_usaha_masuk(){
    $.ajax({
        url: base_url+'dashboard/request/get_jumlah_data_pemilik_usaha_masuk',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $(".data_pemilik_usaha_masuk").html((response.jumlah_pemilik_usaha_masuk)?response.jumlah_pemilik_usaha_masuk:"0");
        },
        complete:function(){
            loading_stop();
        }
    });
}

data_usaha_masuk();
function data_usaha_masuk(){
    $.ajax({
        url: base_url+'dashboard/request/get_jumlah_data_usaha_masuk',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $(".data_usaha_masuk").html((response.jumlah_data_usaha_masuk)?response.jumlah_data_usaha_masuk:"0");
        },
        complete:function(){
            loading_stop();
        }
    });
}

data_aset_omset_masuk();
function data_aset_omset_masuk(){
    $.ajax({
        url: base_url+'dashboard/request/get_jumlah_data_aset_omset_masuk',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $(".data_aset_omset_masuk").html((response.jumlah_data_aset_omset_masuk)?response.jumlah_data_aset_omset_masuk:"0");
        },
        complete:function(){
            loading_stop();
        }
    });
}

data_pemilik_usaha_belum_terverifikasi();
function data_pemilik_usaha_belum_terverifikasi(){
    $.ajax({
        url: base_url+'dashboard/request/get_jumlah_data_pemilik_usaha_belum_terverifikasi',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $(".data_pemilik_usaha_belum_terverifikasi").html((response.jumlah_pemilik_usaha_belum_terverifikasi)?response.jumlah_pemilik_usaha_belum_terverifikasi:"0");
        },
        complete:function(){
            loading_stop();
        }
    });
}

data_usaha_belum_terverifikasi();
function data_usaha_belum_terverifikasi(){
    $.ajax({
        url: base_url+'dashboard/request/get_jumlah_data_usaha_belum_terverifikasi',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $(".data_usaha_belum_terverifikasi").html((response.jumlah_data_usaha_belum_terverifikasi)?response.jumlah_data_usaha_belum_terverifikasi:"0");
        },
        complete:function(){
            loading_stop();
        }
    });
}

data_aset_omset_belum_terverifikasi();
function data_aset_omset_belum_terverifikasi(){
    $.ajax({
        url: base_url+'dashboard/request/get_jumlah_data_aset_omset_belum_terverifikasi',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $(".data_aset_omset_belum_terverifikasi").html((response.jumlah_data_aset_omset_belum_terverifikasi)?response.jumlah_data_aset_omset_belum_terverifikasi:"0");
        },
        complete:function(){
            loading_stop();
        }
    });
}

data_pemilik_usaha_sudah_terverifikasi();
function data_pemilik_usaha_sudah_terverifikasi(){
    $.ajax({
        url: base_url+'dashboard/request/get_jumlah_data_pemilik_usaha_sudah_terverifikasi',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $(".data_pemilik_usaha_sudah_terverifikasi").html((response.jumlah_pemilik_usaha_sudah_terverifikasi)?response.jumlah_pemilik_usaha_sudah_terverifikasi:"0");
        },
        complete:function(){
            loading_stop();
        }
    });
}

data_usaha_sudah_terverifikasi();
function data_usaha_sudah_terverifikasi(){
    $.ajax({
        url: base_url+'dashboard/request/get_jumlah_data_usaha_sudah_terverifikasi',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $(".data_usaha_sudah_terverifikasi").html((response.jumlah_data_usaha_sudah_terverifikasi)?response.jumlah_data_usaha_sudah_terverifikasi:"0");
        },
        complete:function(){
            loading_stop();
        }
    });
}

data_aset_omset_sudah_terverifikasi();
function data_aset_omset_sudah_terverifikasi(){
    $.ajax({
        url: base_url+'dashboard/request/get_jumlah_data_aset_omset_sudah_terverifikasi',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $(".data_aset_omset_sudah_terverifikasi").html((response.jumlah_data_aset_omset_sudah_terverifikasi)?response.jumlah_data_aset_omset_sudah_terverifikasi:"0");
        },
        complete:function(){
            loading_stop();
        }
    });
}

jumlah_unit_usaha();
function jumlah_unit_usaha(){
    $.ajax({
        url: base_url+'dashboard/request/get_jumlah_unit_usaha',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $(".jumlah_unit_usaha").html((response.jumlah_unit_usaha)?response.jumlah_unit_usaha:"0");
        },
        complete:function(){
            loading_stop();
        }
    });
}

jumlah_aset_umkm();
function jumlah_aset_umkm(){
    $.ajax({
        url: base_url+'dashboard/request/get_jumlah_aset_umkm',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $(".jumlah_aset_umkm").html(IDR(response.jumlah_aset_umkm).format(true));
        },
        complete:function(){
            loading_stop();
        }
    });
}

jumlah_omset_umkm();
function jumlah_omset_umkm(){
    $.ajax({
        url: base_url+'dashboard/request/get_jumlah_omset_umkm',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $(".jumlah_omset_umkm").html(IDR(response.jumlah_omset_umkm).format(true));
        },
        complete:function(){
            loading_stop();
        }
    });
}
    
</script>