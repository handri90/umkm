<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    function __construct(){
        parent::__construct();
		$this->load->model('pemilik_usaha/pemilik_usaha_model','pemilik_usaha_model');

    }

	public function index()
	{
        $data['master_pemilik_usaha'] = $this->pemilik_usaha_model->get(
            array(
                "order_by"=>array(
                    "nama_pemilik_usaha"=>"ASC"
                )
            )
        );

        $data['breadcrumb'] = [];
		$this->execute('index',$data);
    }
}
