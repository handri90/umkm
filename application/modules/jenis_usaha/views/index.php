<div class="content">
    <!-- Basic datatable -->
    <div class="card">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url(); ?>jenis_usaha/tambah_jenis_usaha" class="btn btn-info">Tambah Jenis Usaha</a>
            </div>
        </div>
        <table id="datatableJenisUsaha" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>Nama Jenis Usaha</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->
</div>

<script>
let datatableJenisUsaha = $("#datatableJenisUsaha").DataTable();
get_data_jenis_usaha();
function get_data_jenis_usaha(){
    datatableJenisUsaha.clear().draw();
    $.ajax({
        url: base_url+'jenis_usaha/request/get_data_jenis_usaha',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $.each(response,function(index,value){
                datatableJenisUsaha.row.add([
                    value.nama_jenis_usaha,
                    "<a href='"+base_url+"jenis_usaha/edit_jenis_usaha/"+value.id_encrypt+"' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('"+value.id_encrypt+"')\" href='#'><i class='icon-trash'></i></a>"
                ]).draw(false);
            });
        },
        complete:function(response){
            loading_stop();
        }
    });
}

function confirm_delete(id_jenis_usaha){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    swalInit({
        title: 'Apakah anda yakin menghapus data ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: base_url+'jenis_usaha/delete_jenis_usaha',
                data : {id_jenis_usaha:id_jenis_usaha},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    if(response){
                        get_data_jenis_usaha();
                        swalInit(
                            'Berhasil',
                            'Data sudah dihapus',
                            'success'
                        );
                    }else{
                        get_data_jenis_usaha();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa dihapus',
                            'error'
                        );
                    }
                },
                complete:function(response){
                    loading_stop();
                }
            });
        }
        else if(result.dismiss === swal.DismissReason.cancel) {
            swalInit(
                'Batal',
                'Data masih tersimpan!',
                'error'
            ).then(function(results){
                loading_stop();
                if(result.results){
                    get_data_jenis_usaha();
                }
            });
        }
    });
}
</script>