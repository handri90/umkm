<?php

class Jenis_usaha_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="master_jenis_usaha";
        $this->primary_id="id_jenis_usaha";
    }
}