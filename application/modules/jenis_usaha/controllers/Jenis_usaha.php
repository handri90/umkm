<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_usaha extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('jenis_usaha_model');
	}

	public function index()
	{
        $data['breadcrumb'] = [['link'=>false,'content'=>'Jenis Usaha','is_active'=>true]];
        $this->execute('index',$data);
    }
    
    public function tambah_jenis_usaha(){
        if(empty($_POST)){
            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'jenis_usaha','content'=>'Jenis Usaha','is_active'=>false],['link'=>false,'content'=>'Tambah Jenis Usaha','is_active'=>true]];
            $this->execute('form_jenis_usaha',$data);
        }else{

            $data = array(
                "nama_jenis_usaha"=>$this->ipost('jenis_usaha'),
                'created_at'=>$this->datetime()
            );

            $status = $this->jenis_usaha_model->save($data);
            if($status){
                $this->session->set_flashdata('message','Data baru berhasil ditambahkan');
            }else{
                $this->session->set_flashdata('message','Data baru gagal ditambahkan');
            }

            redirect('jenis_usaha');
        }
    }

    public function edit_jenis_usaha($id_jenis_usaha){
        $data_master = $this->jenis_usaha_model->get_by(decrypt_data($id_jenis_usaha));

        if(!$data_master){
            $this->page_error();
        }

        if(empty($_POST)){
            $data['content'] = $data_master;
            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'jenis_usaha','content'=>'Jenis Usaha','is_active'=>false],['link'=>false,'content'=>'Tambah Jenis Usaha','is_active'=>true]];
            $this->execute('form_jenis_usaha',$data);
        }else{
            $data = array(
                "nama_jenis_usaha"=>$this->ipost('jenis_usaha'),
                'updated_at'=>$this->datetime()
            );

            $status = $this->jenis_usaha_model->edit(decrypt_data($id_jenis_usaha),$data);
            if($status){
                $this->session->set_flashdata('message','Data berhasil diubah');
            }else{
                $this->session->set_flashdata('message','Data gagal diubah');
            }

            redirect('jenis_usaha');
        }
    }

    public function delete_jenis_usaha(){
        $id_jenis_usaha = decrypt_data($this->iget('id_jenis_usaha'));
        $data_master = $this->jenis_usaha_model->get_by($id_jenis_usaha);

        if(!$data_master){
            $this->page_error();
        }

        $status = $this->jenis_usaha_model->remove($id_jenis_usaha);
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }
}
