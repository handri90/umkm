<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('jenis_usaha_model');
	}

	public function get_data_jenis_usaha()
	{
        $data_jenis_usaha = $this->jenis_usaha_model->get(
            array(
                'order_by'=>array(
                    'nama_jenis_usaha'=>"ASC"
                )
            )
        );

        $templist = array();
        foreach($data_jenis_usaha as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_jenis_usaha);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
}
