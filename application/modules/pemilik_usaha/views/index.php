<style>
    .v_align_top{
        vertical-align:top !important;
    }
</style>
<div class="content">
    <!-- Basic datatable -->
    <div class="card">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url(); ?>pemilik_usaha/tambah_pemilik_usaha" class="btn btn-info">Tambah Pemilik Usaha</a>
            </div>
        </div>
        <table id="datatablePemilikUsaha" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>Nama Pemilik Usaha</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->
</div>

<div id="modal_detail" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title">Detail Pemilik Usaha</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <table class="table datatable-save-state">
                    <tbody>
                        <tr>
                            <td width="200">Nama</td>
                            <td width="50">:</td>
                            <td>
                                <span class="nama_pemilik_usaha"></span>
                                <input type="hidden" name="id_pemilik_usaha" />
                            </td>
                        </tr>
                        <tr>
                            <td class="v_align_top">NIK</td>
                            <td class="v_align_top">:</td>
                            <td><span class="nik"></span> 
                                <a href="#collapse-ktp" class="text-default" data-toggle="collapse">
									<i class="icon-image2"></i>
                                </a>
                                <div class="collapse" id="collapse-ktp">
                                    <div class="mt-3">
                                        <image class="foto_ktp" width="250" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="v_align_top">No. KK</td>
                            <td class="v_align_top">:</td>
                            <td><span class="nomor_kk"></span>
                                <a href="#collapse-kk" class="text-default" data-toggle="collapse">
									<i class="icon-image2"></i>
                                </a>
                                <div class="collapse" id="collapse-kk">
                                    <div class="mt-3">
                                        <image class="foto_kk" width="250" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Tempat, Tanggal Lahir</td>
                            <td>:</td>
                            <td><span class="tempat_tanggal_lahir"></span></td>
                        </tr>
                        <tr>
                            <td>Jenis Kelamin</td>
                            <td>:</td>
                            <td><span class="jenis_kelamin"></span></td>
                        </tr>
                        <tr>
                            <td>Status Perkawinan</td>
                            <td>:</td>
                            <td><span class="status_perkawinan"></span></td>
                        </tr>
                        <tr>
                            <td>Pendidikan Terakhir</td>
                            <td>:</td>
                            <td><span class="pendidikan_terakhir"></span></td>
                        </tr>
                        <tr>
                            <td>Pekerjaan Inti</td>
                            <td>:</td>
                            <td><span class="pekerjaan_inti"></span></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td><span class="alamat"></span></td>
                        </tr>
                        <tr>
                            <td>No. Telepon</td>
                            <td>:</td>
                            <td><span class="no_telepon"></span></td>
                        </tr>
                        <tr>
                            <td>NPWP</td>
                            <td>:</td>
                            <td><span class="npwp"></span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn bg-primary" onClick="change_status()"><span class="label_btn_verified"></span></button>
            </div>
        </div>
    </div>
</div>

<div id="foto_detail" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title">Foto</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <image class="foto_user" width="400" />
            </div>
        </div>
    </div>
</div>

<script>
let datatablePemilikUsaha = $("#datatablePemilikUsaha").DataTable();
get_data_pemilik_usaha();
function get_data_pemilik_usaha(){
    datatablePemilikUsaha.clear().draw();
    $.ajax({
        url: base_url+'pemilik_usaha/request/get_data_pemilik_usaha',
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $.each(response,function(index,value){
                datatablePemilikUsaha.row.add([
                    value.nama_pemilik_usaha,
                    (value.is_verified == "0"?"<span class='badge bg-warning ml-md-3 mr-md-auto'>Draft</span>":"<span class='badge bg-success ml-md-3 mr-md-auto'>Sudah Diverifikasi</span>"),
                    "<a href='"+base_url+"pemilik_usaha/edit_pemilik_usaha/"+value.id_encrypt+"' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('"+value.id_encrypt+"')\" href='#'><i class='icon-trash'></i></a> <a class='btn btn-success btn-icon' onClick=\"show_detail('"+value.id_encrypt+"')\" href='#'><i class='icon-eye8'></i></a>"
                ]).draw(false);
            });
        },
        complete:function(response){
            loading_stop();
        }
    });
}

function confirm_delete(id_pemilik_usaha){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    swalInit({
        title: 'Apakah anda yakin menghapus data ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: base_url+'pemilik_usaha/delete_pemilik_usaha',
                data : {id_pemilik_usaha:id_pemilik_usaha},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    if(response){
                        get_data_pemilik_usaha();
                        swalInit(
                            'Berhasil',
                            'Data sudah dihapus',
                            'success'
                        );
                    }else{
                        get_data_pemilik_usaha();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa dihapus',
                            'error'
                        );
                    }
                },
                complete:function(response){
                    loading_stop();
                }
            });
        }
        else if(result.dismiss === swal.DismissReason.cancel) {
            swalInit(
                'Batal',
                'Data masih tersimpan!',
                'error'
            ).then(function(results){
                loading_stop();
                if(result.results){
                    get_data_pemilik_usaha();
                }
            });
        }
    });
}

function show_detail(id_pemilik_usaha){
    $.ajax({
        url: base_url+'pemilik_usaha/request/get_data_pemilik_usaha_by_id',
        data:{id_pemilik_usaha:id_pemilik_usaha},
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $(".label_btn_verified").html((response.is_verified == "0"?"Setuju":"Batalkan"));
            $("#modal_detail").modal("show");
            $(".nama_pemilik_usaha").html(response.nama_pemilik_usaha);
            $(".nik").html(response.nik);
            $(".nomor_kk").html(response.nomor_kk);
            $(".tempat_tanggal_lahir").html(response.tempat_lahir+", "+response.tanggal_lahir);
            $(".jenis_kelamin").html((response.jenis_kelamin=="P"?"Perempuan":"Laki-Laki"));
            $(".status_perkawinan").html(response.status_perkawinan);
            $(".pendidikan_terakhir").html((response.pendidikan_terakhir=="Lainnya"?response.pendidikan_terakhir_lainnya:response.pendidikan_terakhir));
            $(".pekerjaan_inti").html((response.pekerjaan_inti=="Lainnya"?response.pekerjaan_inti_lainnya:response.pekerjaan_inti));
            $(".alamat").html(response.alamat+" RT/RW "+response.rt_rw+", "+(response.klasifikasi == "KEL"?"Kelurahan":"Desa")+" "+response.nama_kelurahan+" Kecamatan "+response.nama_kecamatan);
            $(".no_telepon").html(response.nomor_telepon);
            $(".npwp").html(response.npwp);
            $(".foto_ktp").attr("src",response.foto_ktp);
            $(".foto_kk").attr("src",response.foto_kk);
            $("input[name=id_pemilik_usaha]").val(id_pemilik_usaha);
        },
        complete:function(){
            loading_stop();
        }
    });
}

function change_status(){
    id_pemilik_usaha = $("input[name=id_pemilik_usaha]").val();

    $.ajax({
        url: base_url+'pemilik_usaha/change_status',
        data:{id_pemilik_usaha:id_pemilik_usaha},
        type: 'POST',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $("#modal_detail").modal("toggle");
            $(".label_btn_verified").html("");
            $(".nama_pemilik_usaha").html("");
            $(".nik").html("");
            $(".nomor_kk").html("");
            $(".tempat_tanggal_lahir").html("");
            $(".jenis_kelamin").html("");
            $(".status_perkawinan").html("");
            $(".pendidikan_terakhir").html("");
            $(".pekerjaan_inti").html("");
            $(".alamat").html("");
            $(".no_telepon").html("");
            $(".npwp").html("");
            $("input[name=id_pemilik_usaha]").val("");
            get_data_pemilik_usaha();
        },
        complete:function(){
            loading_stop();
        }
    });
}

$("a[href$='showFotoKtp']").on("click",function(){
    $("#foto_detail").modal("show");
})
</script>