<style>
    .calendars{
        width:350px;
    }

    .place_other_pendidikan,.place_other_pekerjaan{
        margin-top: -.5rem;
        margin-left: .5rem;
    }

    .ktp-image-custom{
        margin-bottom:20px;
    }

    .kk-image-custom{
        margin-bottom:20px;
    }
</style>
<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <?php echo form_open_multipart(current_url(),array('class'=>'form-validate-jquery')); ?>
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Pemilik Usaha</legend>

                    <input type="hidden" name="kel_desa_hidden" value="<?php echo !empty($content)?encrypt_data($content->kelurahan_master_wilayah_id):""; ?>" />
                    <input type="hidden" name="id_pemilik_usaha" value="<?php echo !empty($content)?encrypt_data($content->id_pemilik_usaha):""; ?>" />
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Nama Pemilik Usaha <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" value="<?php echo !empty($content)?$content->nama_pemilik_usaha:""; ?>" name="nama_pemilik_usaha" required placeholder="Nama Pemilik Usaha">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">NIK <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" value="<?php echo !empty($content)?$content->nik:""; ?>" name="nik" required placeholder="NIK Pemilik Usaha">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Nomor KK <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="number" class="form-control" value="<?php echo !empty($content)?$content->nomor_kk:""; ?>" name="nomor_kk" required placeholder="Nomor KK">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Tempat Lahir <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" value="<?php echo !empty($content)?$content->tempat_lahir:""; ?>" name="tempat_lahir" required placeholder="Tempat Lahir">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Tanggal Lahir <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control daterange-single" readonly value="<?php echo !empty($content)?$content->tanggal_lahir:""; ?>" name="tanggal_lahir" required placeholder="Tanggal Lahir">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Jenis Kelamin <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-input-styled" name="jenis_kelamin" value="L" <?php echo !empty($content)?($content->jenis_kelamin == "L"?"checked":""):""; ?> data-fouc>
                                    Laki-Laki
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-input-styled" name="jenis_kelamin" value="P" <?php echo !empty($content)?($content->jenis_kelamin == "P"?"checked":""):""; ?> data-fouc>
                                    Perempuan
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Status Perkawinan <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-input-styled" name="status_perkawinan" value="Kawin" <?php echo !empty($content)?($content->status_perkawinan == "Kawin"?"checked":""):""; ?> data-fouc>
                                    Kawin
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-input-styled" name="status_perkawinan" value="Belum Kawin" <?php echo !empty($content)?($content->status_perkawinan == "Belum Kawin"?"checked":""):""; ?> data-fouc>
                                    Belum Kawin
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Pendidikan Terakhir <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input onclick="delete_input_other_pendidikan()" type="radio" class="form-input-styled" name="pendidikan_terakhir" value="SD" <?php echo !empty($content)?($content->pendidikan_terakhir == "SD"?"checked":""):""; ?> data-fouc>
                                    SD
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input onclick="delete_input_other_pendidikan()" type="radio" class="form-input-styled" name="pendidikan_terakhir" value="SLTP" <?php echo !empty($content)?($content->pendidikan_terakhir == "SLTP"?"checked":""):""; ?> data-fouc>
                                    SLTP
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input onclick="delete_input_other_pendidikan()" type="radio" class="form-input-styled" name="pendidikan_terakhir" value="SLTA" <?php echo !empty($content)?($content->pendidikan_terakhir == "SLTA"?"checked":""):""; ?> data-fouc>
                                    SLTA
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input onclick="delete_input_other_pendidikan()" type="radio" class="form-input-styled" name="pendidikan_terakhir" value="Diploma" <?php echo !empty($content)?($content->pendidikan_terakhir == "Diploma"?"checked":""):""; ?> data-fouc>
                                    Diploma
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input onclick="delete_input_other_pendidikan()" type="radio" class="form-input-styled" name="pendidikan_terakhir" value="S1" <?php echo !empty($content)?($content->pendidikan_terakhir == "S1"?"checked":""):""; ?> data-fouc>
                                    S1
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input onclick="delete_input_other_pendidikan()" type="radio" class="form-input-styled" name="pendidikan_terakhir" value="S2" <?php echo !empty($content)?($content->pendidikan_terakhir == "S2"?"checked":""):""; ?> data-fouc>
                                    S2
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input onclick="show_input_other_pendidikan()" type="radio" class="form-input-styled"  value="Lainnya" <?php echo !empty($content)?($content->pendidikan_terakhir == "Lainnya"?"checked":""):""; ?> name="pendidikan_terakhir" data-fouc>
                                    Lainnya
                                    <span class="place_other_pendidikan">
                                        <?php
                                            if(!empty($content)){
                                                if($content->pendidikan_terakhir == "Lainnya"){
                                                    ?>
                                                    <input value="<?php echo !empty($content)?$content->pendidikan_terakhir_lainnya:""; ?>" type='text' class='form-control' name='pendidikan_lainnya' required />
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Pekerjaan Inti <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input onclick="delete_input_other_pekerjaan()" type="radio" class="form-input-styled" name="pekerjaan_inti" value="Wiraswasta" <?php echo !empty($content)?($content->pekerjaan_inti == "Wiraswasta"?"checked":""):""; ?> data-fouc>
                                    Wiraswasta
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input onclick="delete_input_other_pekerjaan()" type="radio" class="form-input-styled" name="pekerjaan_inti" value="ASN" <?php echo !empty($content)?($content->pekerjaan_inti == "ASN"?"checked":""):""; ?> data-fouc>
                                    ASN
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input onclick="delete_input_other_pekerjaan()" type="radio" class="form-input-styled" name="pekerjaan_inti" value="TNI" <?php echo !empty($content)?($content->pekerjaan_inti == "TNI"?"checked":""):""; ?> data-fouc>
                                    TNI
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input onclick="delete_input_other_pekerjaan()" type="radio" class="form-input-styled" name="pekerjaan_inti"  value="Polri" <?php echo !empty($content)?($content->pekerjaan_inti == "Polri"?"checked":""):""; ?> data-fouc>
                                    Polri
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input onclick="show_input_other_pekerjaan()" type="radio" class="form-input-styled" <?php echo !empty($content)?($content->pekerjaan_inti == "Lainnya"?"checked":""):""; ?> value="Lainnya" name="pekerjaan_inti" data-fouc>
                                    Lainnya
                                    <span class="place_other_pekerjaan">
                                    <?php
                                        if(!empty($content)){
                                            if($content->pekerjaan_inti == "Lainnya"){
                                                ?>
                                                <input value="<?php echo !empty($content)?$content->pekerjaan_inti_lainnya:""; ?>" type='text' class='form-control' name='pekerjaan_lainnya' required />
                                                <?php
                                            }
                                        }
                                    ?>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Alamat <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <textarea class="form-control"name="alamat" required placeholder="Alamat"><?php echo !empty($content)?$content->alamat:""; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">RT/RW <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" value="<?php echo !empty($content)?$content->rt_rw:""; ?>" name="rt_rw" required placeholder="RT/RW">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Kelurahan/Desa <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <select class="form-control select-search" name="kelurahan_desa" required onchange="get_kecamatan_kabupaten_provinsi()">
								<option value="">-- Pilih Kelurahan/Desa --</option>
								<?php
								foreach($master_wilayah as $key=>$row){
									$selected = "";
									if(!empty($content)){
										if($row->id_master_wilayah == $content->kelurahan_master_wilayah_id){
											$selected = 'selected="selected"';
										}
									}
									?>
									<option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_master_wilayah); ?>"><?php echo ucwords(strtolower($row->nama_wilayah)); ?></option>
									<?php
								}
								?>
							</select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Kecamatan <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <span class="nama_kecamatan"></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Kabupaten <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <span class="nama_kabupaten"></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Provinsi <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <span class="nama_provinsi"></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Nomor Telepon <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="number" class="form-control" value="<?php echo !empty($content)?$content->nomor_telepon:""; ?>" name="nomor_telepon" required placeholder="Nomor Telepon">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">NPWP <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="number" class="form-control" value="<?php echo !empty($content)?$content->npwp:""; ?>" name="npwp" required placeholder="NPWP">
                        </div>
                    </div>

                    <div class="form-group row">
						<label class="col-lg-2 col-form-label">Foto KTP <?php echo !empty($content)?'':'<span class="text-danger">*</span>'; ?></label>
						<div class="col-lg-10">
							<?php
							if(!empty($content)){
							?>
							<img width="200" class="ktp-image-custom" src="<?php echo base_url().$this->config->item('pemilik_usaha_path')."/".($content->foto_ktp != ""?$content->foto_ktp:""); ?>">
							<?php
							}
							?>
							<input type="file" class="file-input" name="foto_ktp" <?php echo !empty($content)?'':'required'; ?> data-show-upload="false">
                            <code>*Foto harus berupa jpg/jpeg</code> <br>
                        	<code>*Ukuran file maksimal 1 MB</code>
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-lg-2 col-form-label">Foto KK <?php echo !empty($content)?'':'<span class="text-danger">*</span>'; ?></label>
						<div class="col-lg-10">
							<?php
							if(!empty($content)){
							?>
							<img width="200" class="kk-image-custom" src="<?php echo base_url().$this->config->item('pemilik_usaha_path')."/".($content->foto_kk != ""?$content->foto_kk:""); ?>">
							<?php
							}
							?>
							<input type="file" class="file-input" name="foto_kk" <?php echo !empty($content)?'':'required'; ?> data-show-upload="false">
                        	<code>*Foto harus berupa jpg/jpeg</code> <br>
                        	<code>*Ukuran file maksimal 1 MB</code>
						</div>
					</div>
                </fieldset>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /form inputs -->
</div>

<script>

$("input[name=foto_ktp]").on('change',function(){
	$(".ktp-image-custom").hide();
});

$("input[name=foto_kk]").on('change',function(){
	$(".kk-image-custom").hide();
});

$('.daterange-single').daterangepicker({ 
	singleDatePicker: true,
	locale: {
      format: 'DD/MM/YYYY'
    },
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
});

$('.form-input-styled').uniform({
    fileButtonClass: 'action btn bg-pink-400'
});

function delete_input_other_pendidikan(){
    $(".place_other_pendidikan").html("");
}

function delete_input_other_pekerjaan(){
    $(".place_other_pekerjaan").html("");
}

function show_input_other_pendidikan(){
    $(".place_other_pendidikan").html("<input type='text' class='form-control' name='pendidikan_lainnya' required />");
}

function show_input_other_pekerjaan(){
    $(".place_other_pekerjaan").html("<input type='text' class='form-control' name='pekerjaan_lainnya' required />");
}

get_kecamatan_kabupaten_provinsi();
function get_kecamatan_kabupaten_provinsi(){
    let kel_desa = $("select[name=kelurahan_desa]").val();

    if(!kel_desa){
        kel_desa = $("input[name=kel_desa_hidden]").val();
    }

    if(kel_desa){
        $.ajax({
            url: base_url+'pemilik_usaha/request/get_kecamatan_kabupaten_provinsi',
            data: {kel_desa:kel_desa},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                $(".nama_kecamatan").html(response.kecamatan);
                $(".nama_kabupaten").html(response.kabupaten);
                $(".nama_provinsi").html(response.provinsi);
            },
            complete:function(response){
                loading_stop();
            }
        });
    }else{
        $(".nama_kecamatan").html("");
    }
}

$("form").submit(function(){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    let id_pemilik_usaha = $("input[name=id_pemilik_usaha]").val();
    let nik = $("input[name=nik]").val();
    let status = true;

    $.ajax({
        url: base_url+'pemilik_usaha/request/cek_nik',
        async:false,
        data:{id_pemilik_usaha:id_pemilik_usaha,nik:nik},
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            // alert(response);
            status = response;
        },
        complete:function(response){
            loading_stop();
        }
    });

    if(!status){
        swalInit(
            'Gagal',
            'NIK sudah pernah digunakan',
            'error'
        );
        return false;
    }
})

</script>