<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {
	function __construct(){
		parent::__construct();
        $this->load->model('pemilik_usaha_model');
		$this->load->model('master_wilayah_model');
	}

	public function get_data_pemilik_usaha()
	{
        $data_pemilik_usaha = $this->pemilik_usaha_model->get(
            array(
                'order_by'=>array(
                    'nama_pemilik_usaha'=>"ASC"
                )
            )
        );

        $templist = array();
        foreach($data_pemilik_usaha as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_pemilik_usaha);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function get_kecamatan_kabupaten_provinsi(){
        $kel_desa = decrypt_data($this->iget("kel_desa"));

        $data_master = $this->master_wilayah_model->get_by($kel_desa);

        $data_kecamatan = $this->master_wilayah_model->get(
            array(
                "fields"=>"nama_wilayah,kode_induk",
                "where"=>array(
                    "kode_wilayah"=>$data_master->kode_induk
                )
            ),"row"
        );
        
        $data_kabupaten = $this->master_wilayah_model->get(
            array(
                "fields"=>"nama_wilayah,kode_induk",
                "where"=>array(
                    "kode_wilayah"=>$data_kecamatan->kode_induk
                )
            ),"row"
        );

        $data_provinsi = $this->master_wilayah_model->get(
            array(
                "fields"=>"nama_wilayah,kode_induk",
                "where"=>array(
                    "kode_wilayah"=>$data_kabupaten->kode_induk
                )
            ),"row"
        );

        $data['kecamatan'] = ucwords(strtolower($data_kecamatan->nama_wilayah));
        $data['kabupaten'] = ucwords(strtolower($data_kabupaten->nama_wilayah));
        $data['provinsi'] = ucwords(strtolower($data_provinsi->nama_wilayah));

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function get_data_pemilik_usaha_by_id(){
        $id_pemilik_usaha = decrypt_data($this->iget("id_pemilik_usaha"));
        $data = $this->pemilik_usaha_model->get(
            array(
                "fields"=>"master_pemilik_usaha.*,DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') as tanggal_lahir,nama_wilayah as nama_kelurahan,klasifikasi",
                "join"=>array(
                    "master_wilayah"=>"id_master_wilayah=kelurahan_master_wilayah_id"
                ),
                "where"=>array(
                    "id_pemilik_usaha"=>$id_pemilik_usaha
                )
            ),"row"
        );

        $data_master = $this->master_wilayah_model->get_by($data->kelurahan_master_wilayah_id);

        $data_kecamatan = $this->master_wilayah_model->get(
            array(
                "fields"=>"nama_wilayah",
                "where"=>array(
                    "kode_wilayah"=>$data_master->kode_induk
                )
            ),"row"
        );

        $data->foto_ktp = $this->config->item("pemilik_usaha_path")."/".$data->foto_ktp;
        $data->foto_kk = $this->config->item("pemilik_usaha_path")."/".$data->foto_kk;
        $data->nama_kecamatan = ucwords(strtolower($data_kecamatan->nama_wilayah));
        $data->id_encrypt = encrypt_data($data->id_pemilik_usaha);

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function cek_nik(){
        $id_pemilik_usaha = $this->iget("id_pemilik_usaha");
        $nik = $this->iget("nik");

        $con_str = "";

        if($id_pemilik_usaha){
            $con_str .= " AND id_pemilik_usaha != '".decrypt_data($id_pemilik_usaha)."'";
        }

        $data_pemilik_usaha = $this->pemilik_usaha_model->get(
            array(
                "where"=>array(
                    "nik"=>$nik
                ),
                "where_false"=>$con_str
            )
        );

        if(!$data_pemilik_usaha){
            $data = true;
        }else{
            $data = false;
        }

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
}
