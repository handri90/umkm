<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemilik_usaha extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('pemilik_usaha_model');
		$this->load->model('master_wilayah_model');
	}

	public function index()
	{
        $data['breadcrumb'] = [['link'=>false,'content'=>'Pemilik Usaha','is_active'=>true]];
        
        if($this->session->userdata('level_user_id') == '3'){
            $this->execute('index_admin_biasa',$data);
        }else{
            $this->execute('index',$data);
        }
    }
    
    public function tambah_pemilik_usaha(){
        if(empty($_POST)){
            $data['master_wilayah'] = $this->master_wilayah_model->get(
                array(
                    "fields"=>"master_wilayah.*,concat(klasifikasi,' - ',nama_wilayah) as nama_wilayah",
                    "where_false"=>"klasifikasi NOT IN ('PROV','KAB','KEC')",
                    "order_by_false"=>'field (klasifikasi,"KEC","KEL","DESA"),nama_wilayah'
                )
            );

            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'pemilik_usaha','content'=>'Pemilik Usaha','is_active'=>false],['link'=>false,'content'=>'Tambah Pemilik Usaha','is_active'=>true]];
            $this->execute('form_pemilik_usaha',$data);
        }else{

            $input_name_ktp = 'foto_ktp';
            $upload_foto_ktp = $this->upload_file($input_name_ktp,$this->config->item('pemilik_usaha_path'));

            $input_name_kk = 'foto_kk';
            $upload_foto_kk = $this->upload_file($input_name_kk,$this->config->item('pemilik_usaha_path'));
            
            if(!isset($upload_foto_ktp['error']) && !isset($upload_foto_kk['error'])){
                $date_exp = explode("/",$this->ipost('tanggal_lahir'));
                $tanggal_lahir = $date_exp[2]."-".$date_exp[1]."-".$date_exp[0];

                $data = array(
                    "nama_pemilik_usaha"=>$this->ipost('nama_pemilik_usaha'),
                    "nik"=>$this->ipost('nik'),
                    "nomor_kk"=>$this->ipost('nomor_kk'),
                    "tempat_lahir"=>$this->ipost('tempat_lahir'),
                    "tanggal_lahir"=>$tanggal_lahir,
                    "jenis_kelamin"=>$this->ipost('jenis_kelamin'),
                    "status_perkawinan"=>$this->ipost('status_perkawinan'),
                    "pendidikan_terakhir"=>$this->ipost('pendidikan_terakhir'),
                    "pendidikan_terakhir_lainnya"=>($this->ipost('pendidikan_terakhir')=="Lainnya"?$this->ipost('pendidikan_lainnya'):""),
                    "pekerjaan_inti"=>$this->ipost('pekerjaan_inti'),
                    "pekerjaan_inti_lainnya"=>($this->ipost('pekerjaan_inti')=="Lainnya"?$this->ipost('pekerjaan_lainnya'):""),
                    "alamat"=>$this->ipost('alamat'),
                    "rt_rw"=>$this->ipost('rt_rw'),
                    "kelurahan_master_wilayah_id"=>decrypt_data($this->ipost('kelurahan_desa')),
                    "nomor_telepon"=>$this->ipost('nomor_telepon'),
                    "npwp"=>$this->ipost('npwp'),
                    'foto_ktp'=>$upload_foto_ktp['data']['file_name'],
                    'foto_kk'=>$upload_foto_kk['data']['file_name'],
                    'is_verified'=>"0",
                    'created_at'=>$this->datetime(),
                    'id_user_created'=>$this->session->userdata("id_user")
                );

                $status = $this->pemilik_usaha_model->save($data);
                if($status){
                    $this->session->set_flashdata('message','Data baru berhasil ditambahkan');
                }else{
                    $this->session->set_flashdata('message','Data baru gagal ditambahkan');
                }
            }else{
                $this->session->set_flashdata('message','Gagal upload foto');
            }

            redirect('pemilik_usaha');
        }
    }

    public function edit_pemilik_usaha($id_pemilik_usaha){
        $data_master = $this->pemilik_usaha_model->get(
            array(
                "fields"=>"master_pemilik_usaha.*,DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') as tanggal_lahir",
                "where"=>array(
                    "id_pemilik_usaha"=>decrypt_data($id_pemilik_usaha)
                )
            ),"row"
        );

        if(!$data_master){
            $this->page_error();
        }

        if(empty($_POST)){
            $data['master_wilayah'] = $this->master_wilayah_model->get(
                array(
                    "fields"=>"master_wilayah.*,concat(klasifikasi,' - ',nama_wilayah) as nama_wilayah",
                    "where_false"=>"klasifikasi NOT IN ('PROV','KAB','KEC')",
                    "order_by_false"=>'field (klasifikasi,"KEC","KEL","DESA"),nama_wilayah'
                )
            );

            $data['content'] = $data_master;
            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'pemilik_usaha','content'=>'Pemilik Usaha','is_active'=>false],['link'=>false,'content'=>'Tambah Pemilik Usaha','is_active'=>true]];
            $this->execute('form_pemilik_usaha',$data);
        }else{

            $input_name_ktp = 'foto_ktp';
            $foto_ktp = '';
            if(!empty($_FILES['foto_ktp']['name'])){
                if(file_exists($this->config->item('pemilik_usaha_path')."/".$data_master->foto_ktp)){
                    unlink($this->config->item('pemilik_usaha_path')."/".$data_master->foto_ktp);
                }
                $upload_foto_ktp = $this->upload_file($input_name_ktp,$this->config->item('pemilik_usaha_path'));
                $foto_ktp = $upload_foto_ktp['data']['file_name'];
            }else{
                $foto_ktp = $data_master->foto_ktp;
            }

            $input_name_kk = 'foto_kk';
            $foto_kk = '';
            if(!empty($_FILES['foto_kk']['name'])){
                if(file_exists($this->config->item('pemilik_usaha_path')."/".$data_master->foto_kk)){
                    unlink($this->config->item('pemilik_usaha_path')."/".$data_master->foto_kk);
                }
                $upload_foto_kk = $this->upload_file($input_name_kk,$this->config->item('pemilik_usaha_path'));
                $foto_kk = $upload_foto_kk['data']['file_name'];
            }else{
                $foto_kk = $data_master->foto_kk;
            }

            $date_exp = explode("/",$this->ipost('tanggal_lahir'));
            $tanggal_lahir = $date_exp[2]."-".$date_exp[1]."-".$date_exp[0];

            $data = array(
                "nama_pemilik_usaha"=>$this->ipost('nama_pemilik_usaha'),
                "nik"=>$this->ipost('nik'),
                "nomor_kk"=>$this->ipost('nomor_kk'),
                "tempat_lahir"=>$this->ipost('tempat_lahir'),
                "tanggal_lahir"=>$tanggal_lahir,
                "jenis_kelamin"=>$this->ipost('jenis_kelamin'),
                "status_perkawinan"=>$this->ipost('status_perkawinan'),
                "pendidikan_terakhir"=>$this->ipost('pendidikan_terakhir'),
                "pendidikan_terakhir_lainnya"=>($this->ipost('pendidikan_terakhir')=="Lainnya"?$this->ipost('pendidikan_lainnya'):NULL),
                "pekerjaan_inti"=>$this->ipost('pekerjaan_inti'),
                "pekerjaan_inti_lainnya"=>($this->ipost('pekerjaan_inti')=="Lainnya"?$this->ipost('pekerjaan_lainnya'):NULL),
                "alamat"=>$this->ipost('alamat'),
                "rt_rw"=>$this->ipost('rt_rw'),
                "kelurahan_master_wilayah_id"=>decrypt_data($this->ipost('kelurahan_desa')),
                "nomor_telepon"=>$this->ipost('nomor_telepon'),
                "npwp"=>$this->ipost('npwp'),
                'foto_ktp'=>$foto_ktp,
                'foto_kk'=>$foto_kk,
                'updated_at'=>$this->datetime(),
                'id_user_updated'=>$this->session->userdata("id_user")
            );

            $status = $this->pemilik_usaha_model->edit(decrypt_data($id_pemilik_usaha),$data);
            if($status){
                $this->session->set_flashdata('message','Data berhasil diubah');
            }else{
                $this->session->set_flashdata('message','Data gagal diubah');
            }

            redirect('pemilik_usaha');
        }
    }

    public function delete_pemilik_usaha(){
        $id_pemilik_usaha = decrypt_data($this->iget('id_pemilik_usaha'));
        $data_master = $this->pemilik_usaha_model->get_by($id_pemilik_usaha);

        if(!$data_master){
            $this->page_error();
        }

        $status = $this->pemilik_usaha_model->remove($id_pemilik_usaha);
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }

    public function change_status(){
        $id_pemilik_usaha = decrypt_data($this->ipost("id_pemilik_usaha"));

        $data_master = $this->pemilik_usaha_model->get_by($id_pemilik_usaha);

        if($data_master->is_verified == "0"){
            $data_update = array(
                "is_verified"=>"1"
            );
        }else{
            $data_update = array(
                "is_verified"=>"0"
            );
        }

        $status = $this->pemilik_usaha_model->edit($id_pemilik_usaha,$data_update);

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }
}
