<?php

class Pemilik_usaha_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="master_pemilik_usaha";
        $this->primary_id="id_pemilik_usaha";
    }
}