<style>
    .v_align_top{
        vertical-align:top !important;
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Pemilik Usaha: </label>
                        <select class="form-control select-search" name="pemilik_usaha" onChange="get_data_usaha()">
                            <option value="">-- SEMUA --</option>
                            <?php
                            foreach($master_pemilik_usaha as $key=>$row){
                                ?>
                                <option value="<?php echo encrypt_data($row->id_pemilik_usaha); ?>"><?php echo $row->nama_pemilik_usaha; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Basic datatable -->
    <div class="card">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url(); ?>data_usaha/tambah_data_usaha" class="btn btn-info">Tambah Data Usaha</a>
            </div>
        </div>
        <table id="datatableDataUsaha" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>Nama Pemilik Usaha</th>
                    <th>Nama Toko / Perusahaan</th>
                    <th>Jenis Usaha</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->
</div>

<div id="modal_detail" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title">Detail Data Usaha</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <table class="table datatable-save-state">
                    <tbody>
                        <tr>
                            <td width="200">Nama Toko / Perusahaan</td>
                            <td width="50">:</td>
                            <td>
                                <span class="nama_toko_perusahaan"></span>
                                <input type="hidden" name="id_data_usaha" />
                            </td>
                        </tr>
                        <tr>
                            <td>Jenis Usaha</td>
                            <td>:</td>
                            <td><span class="jenis_usaha"></span></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td><span class="alamat"></span></td>
                        </tr>
                        <tr>
                            <td class="v_align_top">Nomor Izin Usaha</td>
                            <td class="v_align_top">:</td>
                            <td>
                                <span class="no_izin_usaha"></span>
                                <a href="#collapse-izin-usaha" class="text-default" data-toggle="collapse">
									<i class="icon-image2"></i>
                                </a>
                                <div class="collapse" id="collapse-izin-usaha">
                                    <div class="mt-3">
                                        <image class="foto_izin_usaha" width="250" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
let datatableDataUsaha = $("#datatableDataUsaha").DataTable();
get_data_usaha();
function get_data_usaha(){
    let pemilik_usaha = $("select[name=pemilik_usaha]").val();

    datatableDataUsaha.clear().draw();
    $.ajax({
        url: base_url+'data_usaha/request/get_data_usaha',
        data: {pemilik_usaha:pemilik_usaha},
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $.each(response,function(index,value){
                datatableDataUsaha.row.add([
                    value.nama_pemilik_usaha,
                    value.nama_toko_perusahaan,
                    value.nama_jenis_usaha,
                    (value.is_verified == "0"?"<span class='badge bg-warning ml-md-3 mr-md-auto'>Draft</span>":"<span class='badge bg-success ml-md-3 mr-md-auto'>Sudah Diverifikasi</span>"),
                    "<a href='"+base_url+"data_usaha/edit_data_usaha/"+value.id_encrypt+"' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('"+value.id_encrypt+"')\" href='#'><i class='icon-trash'></i></a> <a class='btn btn-success btn-icon' onClick=\"show_detail('"+value.id_encrypt+"')\" href='#'><i class='icon-eye8'></i></a>"
                ]).draw(false);
            });
        },
        complete:function(response){
            loading_stop();
        }
    });
}

function confirm_delete(id_data_usaha){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    swalInit({
        title: 'Apakah anda yakin menghapus data ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: base_url+'data_usaha/delete_data_usaha',
                data : {id_data_usaha:id_data_usaha},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    if(response){
                        get_data_usaha();
                        swalInit(
                            'Berhasil',
                            'Data sudah dihapus',
                            'success'
                        );
                    }else{
                        get_data_usaha();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa dihapus',
                            'error'
                        );
                    }
                },
                complete:function(response){
                    loading_stop();
                }
            });
        }
        else if(result.dismiss === swal.DismissReason.cancel) {
            swalInit(
                'Batal',
                'Data masih tersimpan!',
                'error'
            ).then(function(results){
                loading_stop();
                if(result.results){
                    get_data_usaha();
                }
            });
        }
    });
}

function show_detail(id_data_usaha){
    $.ajax({
        url: base_url+'data_usaha/request/get_data_usaha_by_id',
        data:{id_data_usaha:id_data_usaha},
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            $("#modal_detail").modal("show");
            $(".nama_toko_perusahaan").html(response.nama_toko_perusahaan);
            $(".jenis_usaha").html(response.nama_jenis_usaha);
            $(".alamat").html(response.alamat_usaha+" RT/RW "+ response.rt_rw +" "+(response.klasifikasi == "KEL"?"Kelurahan":"Desa")+" "+response.nama_kelurahan+", Kecamatan "+response.kecamatan+", Kabupaten "+response.kabupaten+", Provinsi "+response.provinsi);
            $(".no_izin_usaha").html(response.no_izin_usaha);
            $(".foto_izin_usaha").attr("src",response.foto_izin_usaha);
        },
        complete:function(){
            loading_stop();
        }
    });
}
</script>