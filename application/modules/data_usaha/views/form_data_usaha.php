<style>
    .calendars{
        width:350px;
    }

    .izin-usaha-image-custom{
        margin-bottom:20px;
    }
</style>
<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <?php echo form_open_multipart(current_url(),array('class'=>'form-validate-jquery')); ?>
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Data Usaha</legend>

                    <input type="hidden" name="kel_desa_hidden" value="<?php echo !empty($content)?encrypt_data($content->kelurahan_master_wilayah_id):""; ?>" />

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Pemilik Usaha <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <select class="form-control select-search" name="pemilik_usaha" required>
								<option value="">-- Pilih Pemilik Usaha --</option>
								<?php
								foreach($master_pemilik_usaha as $key=>$row){
									$selected = "";
									if(!empty($content)){
										if($row->id_pemilik_usaha == $content->master_pemilik_usaha_id){
											$selected = 'selected="selected"';
										}
									}
									?>
									<option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_pemilik_usaha); ?>"><?php echo $row->nik." - ".$row->nama_pemilik_usaha; ?></option>
									<?php
								}
								?>
							</select>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Nama Toko / Perusahaan <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" value="<?php echo !empty($content)?$content->nama_toko_perusahaan:""; ?>" name="nama_toko_perusahaan" required placeholder="Nama Toko / Perusahaan">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Jenis Usaha <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <select class="form-control select-search" name="master_jenis_usaha_id" required>
								<option value="">-- Pilih Jenis Usaha --</option>
								<?php
								foreach($master_data_jenis_usaha as $key=>$row){
									$selected = "";
									if(!empty($content)){
										if($row->id_jenis_usaha == $content->master_jenis_usaha_id){
											$selected = 'selected="selected"';
										}
									}
									?>
									<option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_jenis_usaha); ?>"><?php echo $row->nama_jenis_usaha; ?></option>
									<?php
								}
								?>
							</select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Alamat <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <textarea class="form-control"name="alamat_usaha" required placeholder="Alamat Usaha"><?php echo !empty($content)?$content->alamat_usaha:""; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">RT/RW <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" value="<?php echo !empty($content)?$content->rt_rw:""; ?>" name="rt_rw" required placeholder="RT/RW">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Kelurahan/Desa <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <select class="form-control select-search" name="kelurahan_desa" required onchange="get_kecamatan_kabupaten_provinsi()">
								<option value="">-- Pilih Kelurahan/Desa --</option>
								<?php
								foreach($master_wilayah as $key=>$row){
									$selected = "";
									if(!empty($content)){
										if($row->id_master_wilayah == $content->kelurahan_master_wilayah_id){
											$selected = 'selected="selected"';
										}
									}
									?>
									<option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_master_wilayah); ?>"><?php echo ucwords(strtolower($row->nama_wilayah)); ?></option>
									<?php
								}
								?>
							</select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Kecamatan <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <span class="nama_kecamatan"></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Kabupaten <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <span class="nama_kabupaten"></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Provinsi <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <span class="nama_provinsi"></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Nomor Izin Usaha <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" value="<?php echo !empty($content)?$content->no_izin_usaha:""; ?>" name="no_izin_usaha" required placeholder="Nomor Izin Usaha">
                        </div>
                    </div>

                    <div class="form-group row">
						<label class="col-lg-2 col-form-label">Foto Izin Usaha <?php echo !empty($content)?'':'<span class="text-danger">*</span>'; ?></label>
						<div class="col-lg-10">
							<?php
							if(!empty($content)){
							?>
							<img width="200" class="izin-usaha-image-custom" src="<?php echo base_url().$this->config->item('izin_usaha_path')."/".($content->foto_izin_usaha != ""?$content->foto_izin_usaha:""); ?>">
							<?php
							}
							?>
							<input type="file" class="file-input" name="foto_izin_usaha" <?php echo !empty($content)?'':'required'; ?> data-show-upload="false">
                        	<code>*Foto harus berupa jpg/jpeg</code> <br>
                        	<code>*Ukuran file maksimal 1 MB</code>
						</div>
					</div>
                </fieldset>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /form inputs -->
</div>

<script>
$("input[name=foto_izin_usaha]").on('change',function(){
	$(".izin-usaha-image-custom").hide();
});

get_kecamatan_kabupaten_provinsi();

function get_kecamatan_kabupaten_provinsi(){
    let kel_desa = $("select[name=kelurahan_desa]").val();

    if(!kel_desa){
        kel_desa = $("input[name=kel_desa_hidden]").val();
    }

    if(kel_desa){
        $.ajax({
            url: base_url+'data_usaha/request/get_kecamatan_kabupaten_provinsi',
            data: {kel_desa:kel_desa},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                $(".nama_kecamatan").html(response.kecamatan);
                $(".nama_kabupaten").html(response.kabupaten);
                $(".nama_provinsi").html(response.provinsi);
            },
            complete:function(response){
                loading_stop();
            }
        });
    }else{
        $(".nama_kecamatan").html("");
        $(".nama_kabupaten").html("");
        $(".nama_provinsi").html("");
    }
}

</script>