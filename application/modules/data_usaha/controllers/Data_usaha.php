<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_usaha extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('data_usaha_model');
		$this->load->model('jenis_usaha/jenis_usaha_model','jenis_usaha_model');
		$this->load->model('pemilik_usaha/pemilik_usaha_model','pemilik_usaha_model');
		$this->load->model('master_wilayah_model');
	}

	public function index()
	{
        $data['master_pemilik_usaha'] = $this->pemilik_usaha_model->get(
            array(
                "order_by"=>array(
                    "nama_pemilik_usaha"=>"ASC"
                ),
                "where"=>array(
                    "is_verified"=>"1"
                )
            )
        );
        
        $data['breadcrumb'] = [['link'=>false,'content'=>'Data Usaha','is_active'=>true]];
        if($this->session->userdata('level_user_id') == '3'){
            $this->execute('index_admin_biasa',$data);
        }else{
            $this->execute('index',$data);
        }
    }
    
    public function tambah_data_usaha(){
        if(empty($_POST)){
            $data['master_wilayah'] = $this->master_wilayah_model->get(
                array(
                    "fields"=>"master_wilayah.*,concat(klasifikasi,' - ',nama_wilayah) as nama_wilayah",
                    "where_false"=>"klasifikasi NOT IN ('PROV','KAB','KEC')",
                    "order_by_false"=>'field (klasifikasi,"KEC","KEL","DESA"),nama_wilayah'
                )
            );

            $data['master_pemilik_usaha'] = $this->pemilik_usaha_model->get(
                array(
                    "order_by"=>array(
                        "nama_pemilik_usaha"=>"ASC"
                    ),
                    "where"=>array(
                        "is_verified"=>"1"
                    )
                )
            );

            $data['master_data_jenis_usaha'] = $this->jenis_usaha_model->get(
                array(
                    "order_by"=>array(
                        "nama_jenis_usaha"=>"ASC"
                    )
                )
            );

            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'data_usaha','content'=>'Data Usaha','is_active'=>false],['link'=>false,'content'=>'Tambah Data Usaha','is_active'=>true]];
            $this->execute('form_data_usaha',$data);
        }else{

            $input_name_izin_usaha = 'foto_izin_usaha';
            $upload_foto_izin_usaha = $this->upload_file($input_name_izin_usaha,$this->config->item('izin_usaha_path'));
            
            if(!isset($upload_foto_izin_usaha['error'])){
                $data = array(
                    "master_pemilik_usaha_id"=>decrypt_data($this->ipost('pemilik_usaha')),
                    "nama_toko_perusahaan"=>$this->ipost('nama_toko_perusahaan'),
                    "master_jenis_usaha_id"=>decrypt_data($this->ipost('master_jenis_usaha_id')),
                    "alamat_usaha"=>$this->ipost('alamat_usaha'),
                    "rt_rw"=>$this->ipost('rt_rw'),
                    "kelurahan_master_wilayah_id"=>decrypt_data($this->ipost('kelurahan_desa')),
                    "no_izin_usaha"=>$this->ipost('no_izin_usaha'),
                    'foto_izin_usaha'=>$upload_foto_izin_usaha['data']['file_name'],
                    'is_verified'=>"0",
                    'created_at'=>$this->datetime(),
                    'id_user_created'=>$this->session->userdata("id_user")
                );
    
                $status = $this->data_usaha_model->save($data);
                if($status){
                    $this->session->set_flashdata('message','Data baru berhasil ditambahkan');
                }else{
                    $this->session->set_flashdata('message','Data baru gagal ditambahkan');
                }

            }else{
                $this->session->set_flashdata('message','Gagal upload foto');
            }

            redirect('data_usaha');
        }
    }

    public function edit_data_usaha($id_data_usaha){
        $data_master = $this->data_usaha_model->get_by(decrypt_data($id_data_usaha));

        if(!$data_master){
            $this->page_error();
        }

        if(empty($_POST)){
            $data['master_wilayah'] = $this->master_wilayah_model->get(
                array(
                    "fields"=>"master_wilayah.*,concat(klasifikasi,' - ',nama_wilayah) as nama_wilayah",
                    "where_false"=>"klasifikasi NOT IN ('PROV','KAB','KEC')",
                    "order_by_false"=>'field (klasifikasi,"KEC","KEL","DESA"),nama_wilayah'
                )
            );

            $data['master_pemilik_usaha'] = $this->pemilik_usaha_model->get(
                array(
                    "order_by"=>array(
                        "nama_pemilik_usaha"=>"ASC"
                    ),
                    "where"=>array(
                        "is_verified"=>"1"
                    )
                )
            );

            $data['master_data_jenis_usaha'] = $this->jenis_usaha_model->get(
                array(
                    "order_by"=>array(
                        "nama_jenis_usaha"=>"ASC"
                    )
                )
            );

            $data['content'] = $data_master;
            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'data_usaha','content'=>'Data Usaha','is_active'=>false],['link'=>false,'content'=>'Tambah Data Usaha','is_active'=>true]];
            $this->execute('form_data_usaha',$data);
        }else{

            $input_name_izin_usaha = 'foto_izin_usaha';
            $foto_izin_usaha = '';
            if(!empty($_FILES['foto_izin_usaha']['name'])){
                if(file_exists($this->config->item('izin_usaha_path')."/".$data_master->foto_izin_usaha)){
                    unlink($this->config->item('izin_usaha_path')."/".$data_master->foto_izin_usaha);
                }
                $upload_foto_izin_usaha = $this->upload_file($input_name_izin_usaha,$this->config->item('izin_usaha_path'));
                $foto_izin_usaha = $upload_foto_izin_usaha['data']['file_name'];
            }else{
                $foto_izin_usaha = $data_master->foto_izin_usaha;
            }
            
            $data = array(
                "master_pemilik_usaha_id"=>decrypt_data($this->ipost('pemilik_usaha')),
                "nama_toko_perusahaan"=>$this->ipost('nama_toko_perusahaan'),
                "master_jenis_usaha_id"=>decrypt_data($this->ipost('master_jenis_usaha_id')),
                "alamat_usaha"=>$this->ipost('alamat_usaha'),
                "rt_rw"=>$this->ipost('rt_rw'),
                "kelurahan_master_wilayah_id"=>decrypt_data($this->ipost('kelurahan_desa')),
                "no_izin_usaha"=>$this->ipost('no_izin_usaha'),
                'foto_izin_usaha'=>$foto_izin_usaha,
                'updated_at'=>$this->datetime(),
                'id_user_updated'=>$this->session->userdata("id_user")
            );

            $status = $this->data_usaha_model->edit(decrypt_data($id_data_usaha),$data);
            if($status){
                $this->session->set_flashdata('message','Data berhasil diubah');
            }else{
                $this->session->set_flashdata('message','Data gagal diubah');
            }

            redirect('data_usaha');
        }
    }

    public function delete_data_usaha(){
        $id_data_usaha = decrypt_data($this->iget('id_data_usaha'));
        $data_master = $this->data_usaha_model->get_by($id_data_usaha);

        if(!$data_master){
            $this->page_error();
        }

        $status = $this->data_usaha_model->remove($id_data_usaha);
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }

    public function change_status(){
        $id_data_usaha = decrypt_data($this->ipost("id_data_usaha"));

        $data_master = $this->data_usaha_model->get_by($id_data_usaha);

        if($data_master->is_verified == "0"){
            $data_update = array(
                "is_verified"=>"1"
            );
        }else{
            $data_update = array(
                "is_verified"=>"0"
            );
        }

        $status = $this->data_usaha_model->edit($id_data_usaha,$data_update);

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }
}
