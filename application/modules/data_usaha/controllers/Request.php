<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {
	function __construct(){
		parent::__construct();
        $this->load->model('data_usaha_model');
		$this->load->model('master_wilayah_model');
	}

	public function get_data_usaha()
	{
        $pemilik_usaha = $this->iget("pemilik_usaha");

        $con_where = array();

        if($pemilik_usaha){
            $con_where = array(
                "master_pemilik_usaha_id"=>decrypt_data($pemilik_usaha)
            );
        }


        $data_usaha = $this->data_usaha_model->get(
            array(
                "fields"=>"master_data_usaha.*,nama_pemilik_usaha,nama_jenis_usaha",
                "join"=>array(
                    "master_pemilik_usaha"=>"id_pemilik_usaha=master_pemilik_usaha_id AND master_pemilik_usaha.deleted_at IS NULL and master_pemilik_usaha.is_verified = 1",
                    "master_jenis_usaha"=>"id_jenis_usaha=master_jenis_usaha_id",
                ),
                "where"=>$con_where,
                'order_by'=>array(
                    'nama_toko_perusahaan'=>"ASC"
                )
            )
        );

        $templist = array();
        foreach($data_usaha as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_data_usaha);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function get_kecamatan_kabupaten_provinsi(){
        $kel_desa = decrypt_data($this->iget("kel_desa"));

        $data_master = $this->master_wilayah_model->get_by($kel_desa);

        $data_kecamatan = $this->master_wilayah_model->get(
            array(
                "fields"=>"nama_wilayah,kode_induk",
                "where"=>array(
                    "kode_wilayah"=>$data_master->kode_induk
                )
            ),"row"
        );
        
        $data_kabupaten = $this->master_wilayah_model->get(
            array(
                "fields"=>"nama_wilayah,kode_induk",
                "where"=>array(
                    "kode_wilayah"=>$data_kecamatan->kode_induk
                )
            ),"row"
        );

        $data_provinsi = $this->master_wilayah_model->get(
            array(
                "fields"=>"nama_wilayah,kode_induk",
                "where"=>array(
                    "kode_wilayah"=>$data_kabupaten->kode_induk
                )
            ),"row"
        );

        $data['kecamatan'] = ucwords(strtolower($data_kecamatan->nama_wilayah));
        $data['kabupaten'] = ucwords(strtolower($data_kabupaten->nama_wilayah));
        $data['provinsi'] = ucwords(strtolower($data_provinsi->nama_wilayah));

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function get_data_usaha_by_id(){
        $id_data_usaha = decrypt_data($this->iget("id_data_usaha"));

        $data = $this->data_usaha_model->get(
            array(
                "fields"=>"master_data_usaha.*,nama_pemilik_usaha,nama_jenis_usaha,nama_wilayah AS nama_kelurahan,klasifikasi",
                "join"=>array(
                    "master_pemilik_usaha"=>"id_pemilik_usaha=master_pemilik_usaha_id AND master_pemilik_usaha.deleted_at IS NULL and master_pemilik_usaha.is_verified = 1",
                    "master_jenis_usaha"=>"id_jenis_usaha=master_jenis_usaha_id",
                    "master_wilayah"=>"master_data_usaha.kelurahan_master_wilayah_id=id_master_wilayah"
                ),
                "where"=>array(
                    "id_data_usaha"=>$id_data_usaha
                )
            ),"row"
        );

        $data_master = $this->master_wilayah_model->get_by($data->kelurahan_master_wilayah_id);

        $data_kecamatan = $this->master_wilayah_model->get(
            array(
                "fields"=>"nama_wilayah,kode_induk",
                "where"=>array(
                    "kode_wilayah"=>$data_master->kode_induk
                )
            ),"row"
        );
        
        $data_kabupaten = $this->master_wilayah_model->get(
            array(
                "fields"=>"nama_wilayah,kode_induk",
                "where"=>array(
                    "kode_wilayah"=>$data_kecamatan->kode_induk
                )
            ),"row"
        );

        $data_provinsi = $this->master_wilayah_model->get(
            array(
                "fields"=>"nama_wilayah,kode_induk",
                "where"=>array(
                    "kode_wilayah"=>$data_kabupaten->kode_induk
                )
            ),"row"
        );

        $data->foto_izin_usaha = $this->config->item("izin_usaha_path")."/".$data->foto_izin_usaha;
        $data->kecamatan = ucwords(strtolower($data_kecamatan->nama_wilayah));
        $data->kabupaten = ucwords(strtolower($data_kabupaten->nama_wilayah));
        $data->provinsi = ucwords(strtolower($data_provinsi->nama_wilayah));

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
}
