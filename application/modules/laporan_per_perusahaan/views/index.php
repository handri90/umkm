<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Pemilik Usaha: </label>
                        <select class="form-control select-search" name="pemilik_usaha" onChange="get_data_usaha()">
                            <option value="">-- Pilih Pemilik Usaha --</option>
                            <?php
                            foreach($master_pemilik_usaha as $key=>$row){
                                ?>
                                <option value="<?php echo encrypt_data($row->id_pemilik_usaha); ?>"><?php echo $row->nama_pemilik_usaha; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Toko / Perusahaan: </label>
                        <select class="form-control select-search" name="data_usaha" onChange="get_laporan()">
                            <option value="">-- Pilih Toko / Perusahaan --</option>
                            <?php
                            foreach($master_data_usaha as $key=>$row){
                                ?>
                                <option value="<?php echo encrypt_data($row->id_data_usaha); ?>"><?php echo $row->nama_toko_perusahaan; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Basic datatable -->
    <div class="card">
        <div class="card-body">
            <div class="box-top text-right">
                <a href="#cetakLaporan" id="cetakLaporan" class="btn btn-info">Cetak Laporan</a>
            </div>
        </div>
        <table id="datatableLaporan" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>Pemilik Usaha</th>
                    <th>Toko / Perusahaan</th>
                    <th>Jenis Usaha</th>
                    <th>Jumlah Aset</th>
                    <th>Jumlah Omset</th>
                    <th>Modal Sendiri</th>
                    <th>Modal Luar</th>
                    <th>Pinjaman Luar</th>
                    <th>Tahun</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->
</div>

<script>

$(".box-top").hide();

const IDR = value => currency(
    value, { 
        symbol: "Rp. ",
        precision: 0,
        separator: "." 
});

let datatableLaporan = $("#datatableLaporan").DataTable({
    "columns": [
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    ]
});

function get_laporan(){
    let pemilik_usaha = $("select[name=pemilik_usaha]").val();
    let data_usaha = $("select[name=data_usaha]").val();

    datatableLaporan.clear().draw();

    if(pemilik_usaha && data_usaha){
        $(".box-top").show();
        $("#cetakLaporan").attr("href",base_url+"laporan_per_perusahaan/cetak_laporan/"+pemilik_usaha+"/"+data_usaha);

        $.ajax({
            url: base_url+'laporan_per_perusahaan/request/get_laporan',
            data:{pemilik_usaha:pemilik_usaha,data_usaha:data_usaha},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                $.each(response,function(index,value){
                    datatableLaporan.row.add([
                        value.nama_pemilik_usaha,
                        value.nama_toko_perusahaan,
                        value.nama_jenis_usaha,
                        IDR(value.jumlah_aset).format(true),
                        IDR(value.jumlah_omset_per_tahun).format(true),
                        IDR(value.modal_sendiri).format(true),
                        IDR(value.modal_luar).format(true),
                        IDR(value.pinjaman_luar).format(true),
                        value.tahun_n
                    ]).draw(false);
                });
            },
            complete:function(response){
                loading_stop();
            }
        });
    }
}

function get_data_usaha(){
    let pemilik_usaha = $("select[name=pemilik_usaha]").val();

    let html = "";
    $("select[name=data_usaha]").html("<option value=''>-- SEMUA --</option>");
    $.ajax({
        url: base_url+'aset_omset_usaha/request/get_data_usaha',
        data: {pemilik_usaha:pemilik_usaha},
        type: 'GET',
        beforeSend: function(){
            loading_start();
        },
        success: function(response){
            html += "<option value=''>-- SEMUA --</option>";
            $.each(response,function(index,value){
                html += "<option value='"+value.id_encrypt+"'>"+value.nama_toko_perusahaan+"</option>";
            });

            $("select[name=data_usaha]").html(html);
            get_laporan();
        },
        complete:function(response){
            loading_stop();
        }
    });
}
</script>