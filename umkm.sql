-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 03, 2020 at 07:34 PM
-- Server version: 5.5.68-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `umkm`
--

-- --------------------------------------------------------

--
-- Table structure for table `aset_omset_usaha`
--

CREATE TABLE `aset_omset_usaha` (
  `id_aset_omset_usaha` int(11) NOT NULL,
  `master_data_usaha_id` int(11) DEFAULT NULL,
  `tahun_berkenaan` char(4) DEFAULT NULL,
  `jumlah_aset` double(15,0) DEFAULT NULL,
  `jumlah_omset_per_tahun` double(15,0) DEFAULT NULL,
  `modal_sendiri` double(15,0) DEFAULT NULL,
  `modal_luar` double(15,0) DEFAULT NULL,
  `jumlah_pekerja` int(5) DEFAULT NULL,
  `pinjaman_luar` double(15,0) DEFAULT NULL,
  `agunan_modal` text,
  `is_verified` enum('1','0') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_user_created` int(11) DEFAULT NULL,
  `id_user_updated` int(11) DEFAULT NULL,
  `id_user_deleted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aset_omset_usaha`
--

INSERT INTO `aset_omset_usaha` (`id_aset_omset_usaha`, `master_data_usaha_id`, `tahun_berkenaan`, `jumlah_aset`, `jumlah_omset_per_tahun`, `modal_sendiri`, `modal_luar`, `jumlah_pekerja`, `pinjaman_luar`, `agunan_modal`, `is_verified`, `created_at`, `updated_at`, `deleted_at`, `id_user_created`, `id_user_updated`, `id_user_deleted`) VALUES
(1, 1, '2020', 120000000, 200000000, 35000000, 25000000, 2, 0, 'Tidak aja', '1', '2020-09-23 04:46:24', NULL, NULL, 3, NULL, NULL),
(2, 2, '2019', 100000000, 125000000, 25000000, 15000000, 2, 0, 'Tidak ada', '1', '2020-09-23 06:25:45', '2020-09-24 12:08:33', NULL, 3, 3, NULL),
(3, 1, '2019', 120000000, 180000000, 12000000, 21000000, 2, 0, 'Tidak ada', '1', '2020-09-23 06:27:55', '2020-09-24 12:24:34', '2020-10-27 08:52:28', 3, 3, NULL),
(4, 1, '2021', 210000000, 250000000, 23000000, 12000000, 2, 0, 'Tidak ada', '1', '2020-09-23 06:30:19', '2020-09-24 12:25:19', '2020-10-25 12:54:22', 3, 3, NULL),
(5, 6, '2020', 2000000, 1000000, 1000000, 3000000, 2, 15000000, 'Sepeda motor', '1', '2020-10-23 03:10:27', NULL, NULL, 10, NULL, NULL),
(6, 4, '2020', 2400000, 10000000, 5000000, 10000000, 4, 10000000, 'Kebun Sawit 1H', '1', '2020-10-23 03:10:31', NULL, NULL, 12, NULL, NULL),
(7, 3, '2020', 2500000, 15000000, 20000000, 0, 1, 0, '0', '1', '2020-10-23 03:12:48', '2020-10-23 03:38:19', '2020-10-27 08:48:52', 11, 11, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `level_user`
--

CREATE TABLE `level_user` (
  `id_level_user` int(11) NOT NULL,
  `nama_level_user` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `level_user`
--

INSERT INTO `level_user` (`id_level_user`, `nama_level_user`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1, 'superadmin', '2020-01-15 07:42:14', NULL, NULL),
(2, 'admin utama', '2020-09-04 01:49:18', NULL, '2020-09-19 01:44:31'),
(3, 'admin biasa', '2020-09-19 01:44:27', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_data_usaha`
--

CREATE TABLE `master_data_usaha` (
  `id_data_usaha` int(11) NOT NULL,
  `master_pemilik_usaha_id` int(11) DEFAULT NULL,
  `nama_toko_perusahaan` varchar(100) DEFAULT NULL,
  `master_jenis_usaha_id` int(11) DEFAULT NULL,
  `alamat_usaha` text,
  `rt_rw` varchar(10) DEFAULT NULL,
  `kelurahan_master_wilayah_id` int(11) DEFAULT NULL,
  `no_izin_usaha` varchar(100) DEFAULT NULL,
  `foto_izin_usaha` varchar(100) DEFAULT NULL,
  `is_verified` enum('1','0') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_user_created` int(11) DEFAULT NULL,
  `id_user_updated` int(11) DEFAULT NULL,
  `id_user_deleted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_data_usaha`
--

INSERT INTO `master_data_usaha` (`id_data_usaha`, `master_pemilik_usaha_id`, `nama_toko_perusahaan`, `master_jenis_usaha_id`, `alamat_usaha`, `rt_rw`, `kelurahan_master_wilayah_id`, `no_izin_usaha`, `foto_izin_usaha`, `is_verified`, `created_at`, `updated_at`, `deleted_at`, `id_user_created`, `id_user_updated`, `id_user_deleted`) VALUES
(1, 1, 'Arya Rental', 48, 'Jalan Kawitan II No. 09', '05/00', 16, '503.5/408/AS-IUMK/IX/2020', 'fea174e6ee7deae36d74764ba2bac55f.jpg', '1', '2020-09-23 04:45:26', '2020-09-27 04:24:03', NULL, 3, 3, NULL),
(2, 2, 'Berkah', 4, 'Jalan Sudirman No. 6', '03/00', 16, '503.5/448/AS-IUMK/IX/2020', '5efd66df2a6f4feaf1f81f66e4966d26.jpg', '1', '2020-09-23 06:24:00', '2020-09-27 04:23:50', NULL, 3, 3, NULL),
(3, 4, 'MARLITA', 33, 'Jl. PANGERAN ANTASARI', '004/000', 18, '503.5/2681/AS-IUMK/X/2020', '676724b9345d7a2f8819137aaf762bcc.jpg', '1', '2020-10-23 02:52:00', NULL, '2020-10-27 08:51:14', 11, NULL, NULL),
(4, 5, 'Miranti', 55, 'Jl. P. Antasari ', '004', 18, '503.5/2625/AS-IUMK/X/2020', '943b87faf275f1b6091408a29975bb2b.jpg', '1', '2020-10-23 03:00:05', NULL, NULL, 12, NULL, NULL),
(5, 4, 'MARLITA', 3, 'Jl.Paku Negara', '11/', 18, '503.5/2681/AS-IUMK/X/2020', '0b0b067825659f12f4218baa4aec4bc1.jpg', '1', '2020-10-23 03:00:07', NULL, '2020-10-27 08:51:07', 11, NULL, NULL),
(6, 6, 'My Hair Cut', 2, 'Jl.malijo', '09', 17, '503.5/2764/AS_IUMK/X/2020', 'aaa2952108285267e0461812d44a369b.jpg', '1', '2020-10-23 03:01:13', '2020-10-23 11:07:40', NULL, 10, 13, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_jenis_usaha`
--

CREATE TABLE `master_jenis_usaha` (
  `id_jenis_usaha` int(11) NOT NULL,
  `nama_jenis_usaha` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_jenis_usaha`
--

INSERT INTO `master_jenis_usaha` (`id_jenis_usaha`, `nama_jenis_usaha`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1, 'Jasa Servis Kendaraan', '2020-09-23 04:34:19', NULL, '2020-09-24 11:39:39'),
(2, 'Barbershop (Pangkas Rambut)', '2020-09-23 06:47:24', NULL, '2020-10-23 11:05:24'),
(3, 'Bidang Jasa (Advertising, Fotography & Videography, Event Organizer, dan Jasa Lainnya)', '2020-09-24 11:40:37', NULL, NULL),
(4, 'Kuliner (Makanan & Minuman)', '2020-09-24 11:40:48', NULL, '2020-09-26 06:26:04'),
(5, 'Daur Ulang Plastik dan Karton', '2020-09-24 11:41:08', NULL, NULL),
(6, 'Driver Online', '2020-09-24 11:41:19', NULL, NULL),
(7, 'Fashion', '2020-09-24 11:41:29', NULL, NULL),
(8, 'Fashion (Muslim dan Aksesoris Sepatu dan Tas)', '2020-09-24 11:41:55', NULL, NULL),
(9, 'Fashion (Aksesoris)', '2020-09-24 11:42:14', NULL, '2020-09-26 06:27:14'),
(10, 'Fashion Batik', '2020-09-24 11:42:26', NULL, NULL),
(11, 'Furniture', '2020-09-24 11:42:41', NULL, NULL),
(12, 'Handycraft', '2020-09-24 11:42:53', NULL, NULL),
(13, 'Herbal Alternatif', '2020-09-24 11:43:05', NULL, NULL),
(14, 'Home Decor (Furniture and Craft)', '2020-09-24 11:43:25', NULL, NULL),
(15, 'Jasa Angkutan', '2020-09-24 11:43:37', NULL, NULL),
(16, 'Jasa Keuangan', '2020-09-24 11:43:48', NULL, NULL),
(17, 'Jasa Konsultan', '2020-09-24 11:43:58', NULL, NULL),
(18, 'Jasa Laundry', '2020-09-24 11:44:10', NULL, NULL),
(19, 'Jasa Layanan Antar', '2020-09-24 11:44:26', NULL, NULL),
(20, 'Jasa Parkir', '2020-09-24 11:44:39', NULL, NULL),
(21, 'Jasa Pendidikan', '2020-09-24 11:44:56', NULL, NULL),
(22, 'Jasa Penginapan', '2020-09-24 11:45:08', NULL, NULL),
(23, 'Jasa Percetakan', '2020-09-24 11:45:18', NULL, NULL),
(24, 'Jasa Rental Mobil', '2020-09-24 11:45:33', '2020-09-26 06:26:25', NULL),
(25, 'Jasa Transportasi', '2020-09-24 11:45:47', NULL, NULL),
(26, 'Jasa Wedding', '2020-09-24 11:45:57', NULL, NULL),
(27, 'Kerajinan Tangan', '2020-09-24 11:46:09', NULL, NULL),
(28, 'Kesehatan', '2020-09-24 11:46:18', NULL, NULL),
(29, 'Konstruksi', '2020-09-24 11:46:28', NULL, NULL),
(30, 'Konter Hanphone', '2020-09-24 11:46:45', NULL, NULL),
(31, 'Konveksi', '2020-09-24 11:46:56', NULL, NULL),
(32, 'Kuliner (Makanan & Minuman)', '2020-09-24 11:47:10', '2020-09-26 06:25:56', '2020-09-26 06:25:47'),
(33, 'Usaha Lainnya', '2020-09-24 11:47:21', NULL, NULL),
(34, 'Manufaktur', '2020-09-24 11:47:33', NULL, NULL),
(35, 'Olahan Makanan', '2020-09-24 11:47:44', NULL, NULL),
(36, 'Pariwisata (Tour & Travel, Oleh-oleh)', '2020-09-24 11:48:09', NULL, NULL),
(37, 'Penjahit Pakaian', '2020-09-24 11:48:19', NULL, NULL),
(38, 'Penjualan ATK', '2020-09-24 11:48:30', NULL, NULL),
(39, 'Penjualan Beras', '2020-09-24 11:48:41', NULL, NULL),
(40, 'Perdagangan', '2020-09-24 11:48:50', NULL, NULL),
(41, 'Perhutanan', '2020-09-24 11:49:00', NULL, NULL),
(42, 'Perikanan', '2020-09-24 11:49:10', NULL, NULL),
(43, 'Perkebunan', '2020-09-24 11:49:20', NULL, NULL),
(44, 'Pertambangan', '2020-09-24 11:49:32', NULL, NULL),
(45, 'Pertanian', '2020-09-24 11:49:42', NULL, NULL),
(46, 'Peternakan', '2020-09-24 11:49:53', NULL, NULL),
(47, 'Salon', '2020-09-24 11:50:17', NULL, NULL),
(48, 'Jasa Rental Mobil', '2020-09-24 11:50:28', NULL, '2020-09-26 06:26:35'),
(49, 'Steam Mobil dan Motor', '2020-09-24 11:50:44', NULL, NULL),
(50, 'Tempat Senam', '2020-09-24 11:50:56', NULL, NULL),
(51, 'Toko Bahan Bangunan', '2020-09-24 11:51:08', NULL, NULL),
(52, 'Toko Kelontong', '2020-09-24 11:51:20', NULL, NULL),
(53, 'Toko Sembako', '2020-09-24 11:51:31', NULL, NULL),
(54, 'UMKM Sektor Kreatif', '2020-09-24 11:51:45', NULL, NULL),
(55, 'Dagang Buah', '2020-10-23 02:53:30', NULL, NULL),
(56, 'Dagang Buah', '2020-10-23 02:55:33', '2020-10-23 02:55:48', NULL),
(57, 'Dagang Sayuran', '2020-10-23 02:57:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_pemilik_usaha`
--

CREATE TABLE `master_pemilik_usaha` (
  `id_pemilik_usaha` int(11) NOT NULL,
  `nama_pemilik_usaha` varchar(100) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nomor_kk` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` enum('L','P') DEFAULT NULL,
  `status_perkawinan` enum('Kawin','Belum Kawin') DEFAULT NULL,
  `pendidikan_terakhir` enum('SD','SLTP','SLTA','Diploma','S1','S2','Lainnya') DEFAULT NULL,
  `pendidikan_terakhir_lainnya` varchar(100) DEFAULT NULL,
  `pekerjaan_inti` enum('Wiraswasta','ASN','TNI','Polri','Lainnya') DEFAULT NULL,
  `pekerjaan_inti_lainnya` varchar(100) DEFAULT NULL,
  `alamat` text,
  `rt_rw` varchar(10) DEFAULT NULL,
  `kelurahan_master_wilayah_id` int(11) DEFAULT NULL,
  `nomor_telepon` varchar(15) DEFAULT NULL,
  `npwp` varchar(100) DEFAULT NULL,
  `foto_ktp` varchar(100) DEFAULT NULL,
  `foto_kk` varchar(100) DEFAULT NULL,
  `is_verified` enum('1','0') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_user_created` int(11) DEFAULT NULL,
  `id_user_updated` int(11) DEFAULT NULL,
  `id_user_deleted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_pemilik_usaha`
--

INSERT INTO `master_pemilik_usaha` (`id_pemilik_usaha`, `nama_pemilik_usaha`, `nik`, `nomor_kk`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `status_perkawinan`, `pendidikan_terakhir`, `pendidikan_terakhir_lainnya`, `pekerjaan_inti`, `pekerjaan_inti_lainnya`, `alamat`, `rt_rw`, `kelurahan_master_wilayah_id`, `nomor_telepon`, `npwp`, `foto_ktp`, `foto_kk`, `is_verified`, `created_at`, `updated_at`, `deleted_at`, `id_user_created`, `id_user_updated`, `id_user_deleted`) VALUES
(1, 'Wahyudi Purnomo', '6201020805070063', '6201020707730003', 'Pangkalan Bun', '1978-09-21', 'L', 'Kawin', 'SLTA', NULL, 'Wiraswasta', NULL, 'BTN Pinang Merah IX No. 15', '07/00', 12, '085750032000', '0', '1e24ceb41cdc75a2b156b973a8c9a6bc.jpg', '1e24ceb41cdc75a2b156b973a8c9a6bc1.jpg', '1', '2020-09-23 04:36:41', '2020-09-27 04:23:30', NULL, 3, 3, NULL),
(2, 'Maslinah, S.E.', '6208014805700001', '6201022008140003', 'Pangkalan Bun', '1979-09-21', 'P', 'Kawin', 'S1', NULL, 'Wiraswasta', NULL, 'Jalan Sudirman No. 6', '03/00', 16, '082154977707', '0', '822f2b60513059ec1df9b14a5af9c850.jpg', '822f2b60513059ec1df9b14a5af9c8501.jpg', '1', '2020-09-23 06:22:50', '2020-09-27 04:23:13', NULL, 3, 3, NULL),
(3, 'andi', 'andi', '2345', '2344', '2020-10-23', 'L', 'Kawin', 'SD', '', 'Wiraswasta', '', 'andi', '2', 51, '4', '0', 'daebed8cd38c2a17ef514542b217ad45.jpg', 'daebed8cd38c2a17ef514542b217ad451.jpg', '0', '2020-10-23 02:24:44', NULL, '2020-10-23 02:28:04', 6, NULL, NULL),
(4, 'MARLITA', '6201026511780001', '62010211170021', 'PANGKALAN BUN', '1978-11-25', 'P', 'Kawin', 'SD', NULL, 'Wiraswasta', NULL, 'Jl. PAKUNEGARA', '011/000', 18, '082156468979', '0', '9798f8ffbf08c4387bb61d7deea4fd72.jpg', '9798f8ffbf08c4387bb61d7deea4fd721.jpg', '1', '2020-10-23 02:25:39', '2020-10-23 02:42:20', '2020-10-27 08:51:40', 11, 6, NULL),
(5, 'Miranti', '6201024310000004', '62010211100700221', 'Pangkalan Bun', '2020-10-23', 'P', 'Belum Kawin', 'SLTA', '', 'Wiraswasta', '', 'Jl. Pakunegara', '011', 18, '081352550570', '0', '91551173171635a34cdb1f2831a09c83.jpg', '91551173171635a34cdb1f2831a09c831.jpg', '1', '2020-10-23 02:34:04', NULL, NULL, 12, NULL, NULL),
(6, 'Muhammad Yusuf', '6201020812880003', '6201021006110006', 'Pangkalan Bun', '1988-12-12', 'L', 'Belum Kawin', 'Diploma', '', 'Wiraswasta', '', 'Jl.malijo', '09', 17, '082251144171', '0', 'cb78f0f78ac45c49ac78bce0b72b0197.jpg', 'cb78f0f78ac45c49ac78bce0b72b01971.jpg', '1', '2020-10-23 02:35:17', NULL, NULL, 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_wilayah`
--

CREATE TABLE `master_wilayah` (
  `id_master_wilayah` int(11) NOT NULL,
  `kode_wilayah` varchar(100) NOT NULL,
  `nama_wilayah` varchar(100) NOT NULL,
  `klasifikasi` varchar(30) NOT NULL,
  `kode_induk` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_wilayah`
--

INSERT INTO `master_wilayah` (`id_master_wilayah`, `kode_wilayah`, `nama_wilayah`, `klasifikasi`, `kode_induk`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, '62', 'KALIMANTAN TENGAH', 'PROV', 0, '0000-00-00 00:00:00', NULL, NULL),
(7, '6201', 'KOTAWARINGIN BARAT', 'KAB', 62, '0000-00-00 00:00:00', NULL, NULL),
(8, '620101', 'ARUT SELATAN', 'KEC', 6201, '0000-00-00 00:00:00', NULL, NULL),
(9, '6201012001', 'Tanjung Putri', 'DESA', 620101, '0000-00-00 00:00:00', NULL, NULL),
(10, '6201012002', 'Kumpai Batu Bawah', 'DESA', 620101, '0000-00-00 00:00:00', NULL, NULL),
(11, '6201012003', 'Kumpai Batu Atas', 'DESA', 620101, '0000-00-00 00:00:00', NULL, NULL),
(12, '6201012004', 'Pasir Panjang', 'DESA', 620101, '0000-00-00 00:00:00', NULL, NULL),
(13, '6201012005', 'Mendawai', 'KEL', 620101, '0000-00-00 00:00:00', NULL, NULL),
(14, '6201012006', 'Mendawai Seberang', 'KEL', 620101, '0000-00-00 00:00:00', NULL, NULL),
(15, '6201012007', 'Raja ', 'KEL', 620101, '0000-00-00 00:00:00', NULL, NULL),
(16, '6201012008', 'Sidorejo', 'KEL', 620101, '0000-00-00 00:00:00', NULL, NULL),
(17, '6201012009', 'Madurejo', 'KEL', 620101, '0000-00-00 00:00:00', NULL, NULL),
(18, '6201012010', 'Baru', 'KEL', 620101, '0000-00-00 00:00:00', NULL, NULL),
(19, '6201012011', 'Raja Seberang', 'KEL', 620101, '0000-00-00 00:00:00', NULL, NULL),
(20, '6201012012', 'Rangda', 'DESA', 620101, '0000-00-00 00:00:00', NULL, NULL),
(21, '6201012013', 'Kenambui', 'DESA', 620101, '0000-00-00 00:00:00', NULL, NULL),
(22, '6201012014', 'Runtu', 'DESA', 620101, '0000-00-00 00:00:00', NULL, NULL),
(23, '6201012015', 'Umpang', 'DESA', 620101, '0000-00-00 00:00:00', NULL, NULL),
(24, '6201012016', 'Natai Raya', 'DESA', 620101, '0000-00-00 00:00:00', NULL, NULL),
(25, '6201012017', 'Medang Sari', 'DESA', 620101, '0000-00-00 00:00:00', NULL, NULL),
(26, '6201012018', 'Natai Baru', 'DESA', 620101, '0000-00-00 00:00:00', NULL, NULL),
(27, '6201012019', 'Tanjung Terantang', 'DESA', 620101, '0000-00-00 00:00:00', NULL, NULL),
(28, '6201012020', 'Sulung', 'DESA', 620101, '0000-00-00 00:00:00', NULL, NULL),
(29, '620102', 'ARUT UTARA', 'KEC', 6201, '0000-00-00 00:00:00', NULL, NULL),
(30, '6201022001', 'Nanga Mua', 'DESA', 620102, '0000-00-00 00:00:00', NULL, NULL),
(31, '6201022002', 'Pangkut', 'KEL', 620102, '0000-00-00 00:00:00', NULL, NULL),
(32, '6201022003', 'Sukarami', 'DESA', 620102, '0000-00-00 00:00:00', NULL, NULL),
(33, '6201022004', 'Gandis', 'DESA', 620102, '0000-00-00 00:00:00', NULL, NULL),
(34, '6201022005', 'Kerabu', 'DESA', 620102, '0000-00-00 00:00:00', NULL, NULL),
(35, '6201022006', 'Sambi', 'DESA', 620102, '0000-00-00 00:00:00', NULL, NULL),
(36, '6201022007', 'Penyombaan', 'DESA', 620102, '0000-00-00 00:00:00', NULL, NULL),
(37, '6201022008', 'Pandau', 'DESA', 620102, '0000-00-00 00:00:00', NULL, NULL),
(38, '6201022009', 'Riam', 'DESA', 620102, '0000-00-00 00:00:00', NULL, NULL),
(39, '6201022010', 'Panahan', 'DESA', 620102, '0000-00-00 00:00:00', NULL, NULL),
(40, '6201022011', 'Sungai Dau', 'DESA', 620102, '0000-00-00 00:00:00', NULL, NULL),
(41, '620103', 'KUMAI', 'KEC', 6201, '0000-00-00 00:00:00', NULL, NULL),
(42, '6201032001', 'Sungai Cabang', 'DESA', 620103, '0000-00-00 00:00:00', NULL, NULL),
(43, '6201032002', 'Teluk Pulai', 'DESA', 620103, '0000-00-00 00:00:00', NULL, NULL),
(44, '6201032003', 'Sungai Sekonyer', 'DESA', 620103, '0000-00-00 00:00:00', NULL, NULL),
(45, '6201032004', 'Kubu', 'DESA', 620103, '0000-00-00 00:00:00', NULL, NULL),
(46, '6201032005', 'Sungai Bakau', 'DESA', 620103, '0000-00-00 00:00:00', NULL, NULL),
(47, '6201032006', 'Teluk Bogam', 'DESA', 620103, '0000-00-00 00:00:00', NULL, NULL),
(48, '6201032007', 'Keraya', 'DESA', 620103, '0000-00-00 00:00:00', NULL, NULL),
(49, '6201032008', 'Sebuai', 'DESA', 620103, '0000-00-00 00:00:00', NULL, NULL),
(50, '6201032009', 'Sungai Kapitan', 'DESA', 620103, '0000-00-00 00:00:00', NULL, NULL),
(51, '6201032010', 'Kumai Hilir', 'KEL', 620103, '0000-00-00 00:00:00', NULL, NULL),
(52, '6201032011', 'Batu Belaman', 'DESA', 620103, '0000-00-00 00:00:00', NULL, NULL),
(53, '6201032012', 'Sungai Tendang', 'DESA', 620103, '0000-00-00 00:00:00', NULL, NULL),
(54, '6201032013', 'Candi', 'KEL', 620103, '0000-00-00 00:00:00', NULL, NULL),
(55, '6201032014', 'Kumai Hulu', 'KEL', 620103, '0000-00-00 00:00:00', NULL, NULL),
(56, '6201032015', 'Sungai Bedaun', 'DESA', 620103, '0000-00-00 00:00:00', NULL, NULL),
(57, '6201032016', 'Sebuai Timur', 'DESA', 620103, '0000-00-00 00:00:00', NULL, NULL),
(58, '6201032017', 'Bumi Harjo', 'DESA', 620103, '0000-00-00 00:00:00', NULL, NULL),
(59, '6201032018', 'Pangkalan Satu', 'DESA', 620103, '0000-00-00 00:00:00', NULL, NULL),
(60, '620104', 'KOTAWARINGIN LAMA', 'KEC', 6201, '0000-00-00 00:00:00', NULL, NULL),
(61, '6201042001', 'Babual Baboti', 'DESA', 620104, '0000-00-00 00:00:00', NULL, NULL),
(62, '6201042002', 'Tempayung', 'DESA', 620104, '0000-00-00 00:00:00', NULL, NULL),
(63, '6201042003', 'Sakabulin', 'DESA', 620104, '0000-00-00 00:00:00', NULL, NULL),
(64, '6201042004', 'Kinjil', 'DESA', 620104, '0000-00-00 00:00:00', NULL, NULL),
(65, '6201042005', 'Kotawaringin Hilir', 'KEL', 620104, '0000-00-00 00:00:00', NULL, NULL),
(66, '6201042006', 'Riam Durian', 'DESA', 620104, '0000-00-00 00:00:00', NULL, NULL),
(67, '6201042007', 'Dawak', 'DESA', 620104, '0000-00-00 00:00:00', NULL, NULL),
(68, '6201042008', 'Kotawaringin Hulu', 'KEL', 620104, '0000-00-00 00:00:00', NULL, NULL),
(69, '6201042009', 'Lalang', 'DESA', 620104, '0000-00-00 00:00:00', NULL, NULL),
(70, '6201042010', 'Rungun', 'DESA', 620104, '0000-00-00 00:00:00', NULL, NULL),
(71, '6201042011', 'Kondang', 'DESA', 620104, '0000-00-00 00:00:00', NULL, NULL),
(72, '6201042012', 'Sagu Suka Mulya', 'DESA', 620104, '0000-00-00 00:00:00', NULL, NULL),
(73, '6201042013', 'Suka Jaya', 'DESA', 620104, '0000-00-00 00:00:00', NULL, NULL),
(74, '6201042014', 'Suka Makmur', 'DESA', 620104, '0000-00-00 00:00:00', NULL, NULL),
(75, '6201042015', 'Ipuh Bangun Jaya', 'DESA', 620104, '0000-00-00 00:00:00', NULL, NULL),
(76, '6201042016', 'Sumber Mukti', 'DESA', 620104, '0000-00-00 00:00:00', NULL, NULL),
(77, '6201042017', 'Palih Baru', 'DESA', 620104, '0000-00-00 00:00:00', NULL, NULL),
(78, '620105', 'PANGKALAN LADA', 'KEC', 6201, '0000-00-00 00:00:00', NULL, NULL),
(79, '6201052001', 'Pangkalan Tiga', 'DESA', 620105, '0000-00-00 00:00:00', NULL, NULL),
(80, '6201052002', 'Pandu Sanjaya', 'DESA', 620105, '0000-00-00 00:00:00', NULL, NULL),
(81, '6201052003', 'Lada Mandala Jaya', 'DESA', 620105, '0000-00-00 00:00:00', NULL, NULL),
(82, '6201052004', 'Makarti Jaya', 'DESA', 620105, '0000-00-00 00:00:00', NULL, NULL),
(83, '6201052005', 'Sumber Agung', 'DESA', 620105, '0000-00-00 00:00:00', NULL, NULL),
(84, '6201052006', 'Purbasari', 'DESA', 620105, '0000-00-00 00:00:00', NULL, NULL),
(85, '6201052007', 'Sungai Rangit Jaya', 'DESA', 620105, '0000-00-00 00:00:00', NULL, NULL),
(86, '6201052008', 'Pangkalan Dewa', 'DESA', 620105, '0000-00-00 00:00:00', NULL, NULL),
(87, '6201052009', 'Kadipi Atas ', 'DESA', 620105, '0000-00-00 00:00:00', NULL, NULL),
(88, '6201052010', 'Sungai Melawen', 'DESA', 620105, '0000-00-00 00:00:00', NULL, NULL),
(89, '6201052011', 'Pangkalan Durin', 'DESA', 620105, '0000-00-00 00:00:00', NULL, NULL),
(90, '620106', 'PANGKALAN BANTENG', 'KEC', 6201, '0000-00-00 00:00:00', NULL, NULL),
(91, '6201062001', 'Pangkalan Banteng', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(92, '6201062002', 'Mulya Jadi', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(93, '6201062003', 'Karang Mulya', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(94, '6201062004', 'Kebun Agung', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(95, '6201062005', 'Sidomulyo', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(96, '6201062006', 'Marga Mulya', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(97, '6201062007', 'Amin Jaya', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(98, '6201062008', 'Arga Mulya', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(99, '6201062009', 'Natai Kerbau', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(100, '6201062010', 'Simpang Berambai', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(101, '6201062011', 'Sungai Hijau', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(102, '6201062012', 'Sungai Pakit', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(103, '6201062013', 'Berambai Makmur', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(104, '6201062014', 'Karang Sari', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(105, '6201062015', 'Sungai Pulau', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(106, '6201062016', 'Sungai Bengkuang', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL),
(107, '6201062017', 'Sungai Kuning', 'DESA', 620106, '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(50) DEFAULT NULL,
  `id_parent_menu` int(11) DEFAULT NULL,
  `nama_module` varchar(100) DEFAULT NULL,
  `nama_class` varchar(100) DEFAULT NULL,
  `class_icon` varchar(20) DEFAULT NULL,
  `order_menu` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `nama_menu`, `id_parent_menu`, `nama_module`, `nama_class`, `class_icon`, `order_menu`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1, 'User', 0, 'user', 'User', 'icon-user', 2, '2020-01-14 10:16:58', NULL, '2020-01-26 11:44:32'),
(5, 'Dashboard', 0, 'dashboard', 'Dashboard', 'icon-home4', 1, '2020-01-14 14:06:31', NULL, NULL),
(6, 'Level User', 0, 'level_user', 'Level_user', 'icon-accessibility', 3, '2020-01-14 14:21:49', NULL, '2020-01-26 11:45:08'),
(7, 'Menu', 0, 'menu', 'Menu', 'icon-menu4', 4, '2020-01-19 13:40:22', NULL, '2020-01-26 11:47:37'),
(9, 'Privilage Menu', 0, 'privilage_level', 'Privilage_level', 'icon-list', 5, '2020-01-19 11:22:27', NULL, '2020-01-26 11:47:04'),
(10, 'Jenis Usaha', 0, 'jenis_usaha', 'Jenis_usaha', 'icon-price-tag2', 6, '2020-09-05 06:51:26', NULL, NULL),
(11, 'Data Pemilik Usaha', 0, 'pemilik_usaha', 'Pemilik_usaha', 'icon-user-tie', 7, '2020-09-05 07:11:58', NULL, '2020-09-26 01:21:33'),
(12, 'Data Usaha', 0, 'data_usaha', 'Data_usaha', 'icon-clipboard3', 8, '2020-09-06 08:37:19', NULL, NULL),
(13, 'Data Aset & Omset', 0, 'aset_omset_usaha', 'Aset_omset_usaha', 'icon-coin-dollar', 9, '2020-09-06 09:38:10', NULL, '2020-09-26 01:22:03'),
(14, 'Laporan Per Tahun', 15, 'laporan_per_tahun', 'Laporan_per_tahun', 'icon-stack-text', 1, '2020-09-08 07:57:40', NULL, '2020-09-08 08:21:04'),
(15, 'Laporan', 0, 'laporan', 'laporan', 'icon-stack-text', 10, '2020-09-08 08:09:58', NULL, NULL),
(16, 'Laporan Per Perusahaan', 15, 'laporan_per_perusahaan', 'Laporan_per_perusahaan', 'icon-stack-text', 2, '2020-09-08 08:20:58', NULL, NULL),
(17, 'Laporan Per Jenis Usaha', 15, 'laporan_per_jenis_usaha', 'Laporan_per_jenis_usaha', 'icon-stack-text', 3, '2020-09-08 01:08:50', NULL, NULL),
(18, 'User', 9999, 'user', 'User', 'icon-user', 1, '2020-09-20 14:59:52', NULL, NULL),
(19, 'Laporan', 0, 'laporan_per_jenis_usaha', 'Laporan_per_jenis_usaha', 'icon-stack-text', 10, '2020-09-20 03:05:13', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `privilage_level_menu`
--

CREATE TABLE `privilage_level_menu` (
  `id_privilage` int(11) NOT NULL,
  `level_user_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `create_content` enum('1','0') DEFAULT NULL,
  `update_content` enum('1','0') DEFAULT NULL,
  `delete_content` enum('1','0') DEFAULT NULL,
  `view_content` enum('1','0') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `privilage_level_menu`
--

INSERT INTO `privilage_level_menu` (`id_privilage`, `level_user_id`, `menu_id`, `create_content`, `update_content`, `delete_content`, `view_content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '1', '1', '1', '1', '2020-01-17 14:20:33', '2020-09-20 03:05:31', NULL),
(2, 1, 5, '1', '1', '1', '1', '2020-01-17 14:20:54', '2020-09-20 03:05:31', NULL),
(4, 1, 6, '1', '1', '1', '1', '2020-01-17 14:33:15', '2020-09-20 03:05:31', '2020-09-04 02:02:23'),
(5, 1, 7, '1', '1', '1', '1', '2020-01-19 13:41:58', '2020-09-20 03:05:31', NULL),
(6, 1, 9, '1', '1', '1', '1', '2020-01-19 23:23:38', '2020-09-20 03:05:31', NULL),
(8, 1, 6, '1', '1', '1', '1', NULL, '2020-09-20 03:05:31', '2020-09-04 02:05:51'),
(9, 1, 6, '1', '1', '1', '1', NULL, '2020-09-20 03:05:31', '2020-09-04 02:09:02'),
(10, 1, 6, '1', '1', '1', '1', '2020-09-04 02:09:14', '2020-09-20 03:05:31', NULL),
(11, 1, 10, '1', '1', '1', '1', '2020-09-05 06:51:36', '2020-09-20 03:05:31', NULL),
(12, 1, 11, '1', '1', '1', '1', '2020-09-05 07:12:08', '2020-09-20 03:05:31', NULL),
(13, 1, 12, '1', '1', '1', '1', '2020-09-06 08:37:29', '2020-09-20 03:05:31', NULL),
(14, 1, 13, '1', '1', '1', '1', '2020-09-06 09:38:25', '2020-09-20 03:05:31', NULL),
(15, 1, 14, '1', '1', '1', '1', '2020-09-08 07:57:53', '2020-09-20 03:05:31', NULL),
(16, 1, 15, '1', '1', '1', '1', '2020-09-08 08:10:16', '2020-09-08 01:08:59', '2020-09-20 03:05:31'),
(17, 1, 16, '1', '1', '1', '1', '2020-09-08 08:21:13', '2020-09-20 03:05:31', NULL),
(18, 1, 17, '1', '1', '1', '1', '2020-09-08 01:08:59', '2020-09-20 03:05:31', NULL),
(19, 2, 1, '1', '1', '1', '1', '2020-09-08 05:21:01', NULL, '2020-09-08 05:21:34'),
(20, 2, 5, '1', '1', '1', '1', '2020-09-08 05:21:01', '2020-09-23 04:37:10', NULL),
(21, 2, 10, '1', '1', '1', '1', '2020-09-08 05:21:01', '2020-09-23 04:37:10', NULL),
(22, 2, 11, '1', '1', '1', '1', '2020-09-08 05:21:01', '2020-09-23 04:37:10', NULL),
(23, 2, 12, '1', '1', '1', '1', '2020-09-08 05:21:01', '2020-09-23 04:37:10', NULL),
(24, 2, 13, '1', '1', '1', '1', '2020-09-08 05:21:01', '2020-09-23 04:37:10', NULL),
(25, 2, 14, '1', '1', '1', '1', '2020-09-08 05:21:01', '2020-09-23 04:37:10', NULL),
(26, 2, 15, '1', '1', '1', '1', '2020-09-08 05:21:01', '2020-09-20 03:01:56', '2020-09-20 03:05:51'),
(27, 2, 16, '1', '1', '1', '1', '2020-09-08 05:21:01', '2020-09-23 04:37:10', NULL),
(28, 2, 17, '1', '1', '1', '1', '2020-09-08 05:21:01', '2020-09-23 04:37:10', NULL),
(29, 3, 5, '1', '1', '1', '1', '2020-09-19 01:47:36', '2020-09-20 03:00:30', NULL),
(30, 3, 11, '1', '1', '1', '1', '2020-09-19 01:47:36', '2020-09-20 03:00:30', NULL),
(31, 3, 12, '1', '1', '1', '1', '2020-09-19 01:47:36', '2020-09-20 03:00:30', NULL),
(32, 3, 13, '1', '1', '1', '1', '2020-09-19 01:47:36', '2020-09-20 03:00:30', NULL),
(33, 3, 18, '1', '1', '1', '1', '2020-09-20 03:00:30', NULL, NULL),
(34, 2, 18, '1', '1', '1', '1', '2020-09-20 03:01:56', '2020-09-23 04:37:10', NULL),
(35, 1, 19, '1', '1', '1', '1', '2020-09-20 03:05:31', NULL, NULL),
(36, 2, 19, '1', '1', '1', '1', '2020-09-20 03:05:51', '2020-09-23 04:37:10', NULL),
(37, 2, 1, '1', '1', '1', '1', '2020-09-23 04:37:10', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `level_user_id` int(11) DEFAULT NULL,
  `foto_user` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama_lengkap`, `username`, `password`, `level_user_id`, `foto_user`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Superadmin', 'superadmin', '$2y$12$2d2GEFgIkv/WhniVgh4aFuQFRLryQ1BOn6CnZBmft2XP2WR.MP7ua', 1, 'd428e7d67751869b72a9db2adc8cb0d3.jpg', NULL, '2020-09-08 05:17:46', NULL),
(2, 'test', 'test', '$2y$12$fNkicv8gjZLWl/gdbfsqP.TrpcbuS6b/yswOH5eFO8HK6hIsOVwaK', 1, 'e61185f74670bdecabc95da9d29f6f31.JPG', '2020-09-04 01:48:15', '2020-09-04 01:48:25', '2020-09-04 01:48:32'),
(3, 'admin', 'admin_utama', '$2y$12$LzVVFHL.JmR9juXyoKmq5OMduBJjAlmnZai7095NEuxkTo1akh1n2', 2, 'e150b95a688bd5e1d5df8b8ad68c8852.jpg', '2020-09-04 02:13:50', '2020-09-27 04:11:13', NULL),
(4, 'admin', 'admins', '$2y$12$4M7OWmdRcvsV3uy/qNuvXO4KtnyK.uXPFdcWJ3EljHql86Bnw3QXS', 2, '0a6f16341524e5e24216a2bda5ba1360.jpg', '2020-09-08 05:46:29', NULL, '2020-09-08 05:46:37'),
(5, 'admin', 'admins', '$2y$12$CorYWfwS.ElhazuWkazokO70h6/BRmT64E2ChuLs9hf3Ii2aiDhlm', 2, '0cd7e44227616ec2eab667328cf7dbf8.jpg', '2020-09-08 05:48:32', NULL, '2020-09-08 05:48:39'),
(6, 'admin_biasa', 'admin_biasa', '$2y$12$2k8s/meUChbkcQP55e2NBeOeIW8fU.9f4c9mWqFUFZvcyqB41bDPS', 3, '8d973757bf28a1bfa2e9203c41e8d89c.jpg', '2020-09-19 01:46:59', '2020-09-27 04:08:50', NULL),
(7, 'noor aida w.', 'noor aida', '$2y$12$U3DMqZaekreRJLEwC5uSoeJyGwa0dEwU5xmxyy27sGjFc7iDoZ.xi', 2, '73442bab3839c788ce55fbd4faec8421.jpg', '2020-09-23 04:39:45', '2020-09-27 04:22:26', NULL),
(8, 'andi wijaya', 'andi', '$2y$12$DYW/vnbjIszL45s79ZeL9.lh22KyAuyXzALtiifQPAy9Jl1ZY4YvK', 2, '043afec5edc1cf1cbc54b953b48c8af8.jpg', '2020-09-23 04:40:24', '2020-09-27 04:22:03', NULL),
(9, 'Sopiani', 'sopiani', '$2y$12$5chCQ/KoZ7WgfEYtz73XkeyJLeJcWM60FrajYXDKZzTcah.r5J/dK', 3, '7bb36716729e95e26de44cbb3437b826.jpg', '2020-10-23 01:47:13', NULL, NULL),
(10, 'hanafiah', 'hanafiah', '$2y$12$o/oA.RtOFdWVZb4x3db8bOqIKgIaahb2EOFAc43ctu5kRrLGFbHma', 3, '8509465ea983466b1fed737057620a1f.jpg', '2020-10-23 01:47:51', NULL, NULL),
(11, 'dwi', 'dwi', '$2y$12$JQKSrPcFBAPI8tvW3mPvjuOgkuj2bTbPi.x20zZL.vPqqDO2Oaiyi', 3, '90b2b6b46fe220d9af310aeac03415a3.jpg', '2020-10-23 01:48:19', '2020-10-23 03:46:40', NULL),
(12, 'lince', 'lince', '$2y$12$Eq/PAPNP3MKCjgnzslLWB.vgm9ZEQzG0DhcHMmPkPvqA4GJXAKp4y', 3, '10748b0f695377d970c2738ecb5ffd18.jpg', '2020-10-23 01:48:46', NULL, NULL),
(13, 'ninja', 'ninja', '$2y$12$7Qh15eyJVPf/NRFiN9gc8ebDbIrUKSpLLaXdu7KeFguFuXEAN4Izq', 2, 'af38cc5eb370210ad4bd73561cac814f.png', '2020-10-23 10:59:28', NULL, NULL),
(14, 'kecamatan arsel', 'arsel', '$2y$12$z.cBhGrVH5ISipg4e4z8fez2qYFhSry9mvbWbCw3utriaNFuUvAQ2', 3, '7e758f6960bcf65d92a94df38d8a6e06.jpg', '2020-11-13 08:37:47', NULL, NULL),
(15, 'kecamatan kumai', 'kumai', '$2y$12$GUZNlyEoKF15hsvHjzvKL.IM/ZBb5tY3eKI4L1wMTYaGCDRWe48ru', 3, 'a87c34e58f659349c8e3a5c127b8a8eb.jpg', '2020-11-13 08:38:23', NULL, NULL),
(16, 'kecamatan pangkalan lada', 'pangkalanlada', '$2y$12$yLDiJndEa3LTZJRGukwEpuGBY2W.QPXfnDHc0/k55OmiPBz647jkO', 3, 'f5e9434baf2e9e260aafffde4c446397.jpg', '2020-11-13 08:38:56', NULL, NULL),
(17, 'kecamatan pangkalan banteng', 'pangkalanbanteng', '$2y$12$9FISTOIsJT3f6tx7QlwSDeBMtgJjEJvvB1/8SFnkOdpRNFbm4uFnO', 3, '69599b716f9adbe3f8d6f11cee36cd41.jpg', '2020-11-13 08:39:30', NULL, NULL),
(18, 'kecamatan arut utara', 'arututara', '$2y$12$4.zmTK5P7kuXWmemzWZ0a.T7pkbj8eThamy81J/nvQn51hLG3TDQm', 3, '1652a25e2e01cfea7afd023c6ca5d8ac.jpg', '2020-11-13 08:39:59', NULL, NULL),
(19, 'kecamatan kotawaringin lama', 'kotawaringinlama', '$2y$12$F3cR2h39aovCz1r5FMuVyeL06jgmnfdlRkbW/qz348.yRzjEaqa9y', 3, '8221b440d8e9ff7028d92d7c06a678d1.jpg', '2020-11-13 08:40:37', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aset_omset_usaha`
--
ALTER TABLE `aset_omset_usaha`
  ADD PRIMARY KEY (`id_aset_omset_usaha`);

--
-- Indexes for table `level_user`
--
ALTER TABLE `level_user`
  ADD PRIMARY KEY (`id_level_user`);

--
-- Indexes for table `master_data_usaha`
--
ALTER TABLE `master_data_usaha`
  ADD PRIMARY KEY (`id_data_usaha`);

--
-- Indexes for table `master_jenis_usaha`
--
ALTER TABLE `master_jenis_usaha`
  ADD PRIMARY KEY (`id_jenis_usaha`);

--
-- Indexes for table `master_pemilik_usaha`
--
ALTER TABLE `master_pemilik_usaha`
  ADD PRIMARY KEY (`id_pemilik_usaha`);

--
-- Indexes for table `master_wilayah`
--
ALTER TABLE `master_wilayah`
  ADD PRIMARY KEY (`id_master_wilayah`),
  ADD UNIQUE KEY `kode_wilayah` (`kode_wilayah`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `privilage_level_menu`
--
ALTER TABLE `privilage_level_menu`
  ADD PRIMARY KEY (`id_privilage`),
  ADD KEY `fk_privilage_level_menu` (`level_user_id`),
  ADD KEY `fk_privilage_menu_id` (`menu_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `fk_level_user` (`level_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aset_omset_usaha`
--
ALTER TABLE `aset_omset_usaha`
  MODIFY `id_aset_omset_usaha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `level_user`
--
ALTER TABLE `level_user`
  MODIFY `id_level_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `master_data_usaha`
--
ALTER TABLE `master_data_usaha`
  MODIFY `id_data_usaha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `master_jenis_usaha`
--
ALTER TABLE `master_jenis_usaha`
  MODIFY `id_jenis_usaha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `master_pemilik_usaha`
--
ALTER TABLE `master_pemilik_usaha`
  MODIFY `id_pemilik_usaha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `master_wilayah`
--
ALTER TABLE `master_wilayah`
  MODIFY `id_master_wilayah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `privilage_level_menu`
--
ALTER TABLE `privilage_level_menu`
  MODIFY `id_privilage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `privilage_level_menu`
--
ALTER TABLE `privilage_level_menu`
  ADD CONSTRAINT `fk_privilage_level_menu` FOREIGN KEY (`level_user_id`) REFERENCES `level_user` (`id_level_user`),
  ADD CONSTRAINT `fk_privilage_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id_menu`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_level_user` FOREIGN KEY (`level_user_id`) REFERENCES `level_user` (`id_level_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
